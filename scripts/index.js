const cd = __dirname;
const fs = require('fs');
const figureLoader = new (require(cd+'/figures.js').FigureLoader)();

let inputFiles = {
	tex1_svg: cd+'/../resources/tex1.svg'
};

//skip if no input files modified since last run
let doBypass = checkIfUnnecessary(inputFiles);

if(!doBypass){
	figureLoader.importSvg(inputFiles.tex1_svg);
	figureLoader.export({
		header: cd+'/../src/generated/figures.h'
	});
	console.log('[ASSETS] Resources generated.');
}

function checkIfUnnecessary(inputFiles){
	//get last execution time from 'last.log'
	let logPath = cd+'/last.log';
	let touch = () => fs.closeSync(fs.openSync(logPath, 'w'));;
	if(!fs.existsSync(logPath)){
		touch();
		return false;
	}
	let lastExecuted = fs.statSync(logPath).mtime;
	touch();
	//check input files
	for(var id in inputFiles){
		let modified = fs.statSync(inputFiles[id]).mtime;
		if(modified > lastExecuted) return false;
	}
	//check scripts
	let scriptsDir = ['index.js', 'figures.js'];
	for(var i=0; i < scriptsDir.length; i++){
		let modified = fs.statSync(cd+'/'+scriptsDir[i]).mtime;
		if(modified > lastExecuted) return false;
	}
	return true;
}
