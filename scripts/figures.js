const fs = require('fs');
const Path = require('path');
const parseSVG = require('svg-parser').parse;

class Figure{

	constructor(name){
		this.name = name;
		this.cname = CStyleName(name);
		this.frames = [];
		this.vertCount = 0;
	}
	
	addFrame(id, verts, el){
		let frame = triangulate(verts);
		frame.bb = getBoundingBox(verts);
		if(el.properties['inkscape:transform-center-x']){
			frame.center = [frame.bb[0]+parseFloat(el.properties['inkscape:transform-center-x']),
							frame.bb[1]-parseFloat(el.properties['inkscape:transform-center-y'])];
		}else{
			frame.center = [frame.bb[0]+frame.bb[2]/2,
							frame.bb[1]+frame.bb[3]/2];
		}
		frame.baseVert = this.vertCount;
		this.vertCount += frame.verts.length;
		//frame.cname = 'FRAME_'+fig.name.toUpperCase()+'_'+(id || 0);
		this.frames[id || 0] = frame;
	}

}

class FigureLoader{

	constructor(){
		this.figures = [];
	}

	importSvg(path){
		let srcSVG = fs.readFileSync(path, 'utf-8');
		let root = parseSVG(srcSVG);
		let layers = root.children[0];
		let imgLayer = layers.children.find(el => el.properties['inkscape:label'] == 'img');
		let figLayer = layers.children.find(el => el.properties['inkscape:label'] == 'figures');
		let tex;
		if(imgLayer){
			let props = imgLayer.children[0].properties;
			tex = {
				path: props['xlink:href'],
				name: Path.basename(props['xlink:href']),
				width: props.width,
				height: props.height
			};
		}
		if(!figLayer) throw('No figures layer found.');
		
		figLayer.children.forEach(el => {
			if(el.tagName != 'path') return;
			let verts = pathToVerts(el.properties.d);
			const regex = /(\w+)(?:\#(\d+))?/;
			const [ᝰ, name, frameId] = [...el.properties['inkscape:label'].match(regex)];
			let fig = this.figures.find(s => s.name == name);
			if(!fig){
				fig = new Figure(name);
				this.figures.push(fig);
			}
			fig.addFrame(frameId || 0, verts, el);
			fig.tex = tex;
		});
	}

	export(paths){
		let figs_h = fs.createWriteStream(paths.header);
		let cname = CStyleName(Path.basename(paths.header));
//----------------------- header
		figs_h.write(`
#pragma once
#include "../graphics.h"

#ifndef ${cname}_IMP

${this.figures.map(fig =>
					fig.frames.map((frame, frameId) =>
									`extern const FigureFrame FRAME_${frameId};`
					).join('\n') +`\nextern const Figure FIG_${fig.cname};`).join('\n')}

#else
		`);
//----------------------- frames
		this.figures.forEach(fig => {
			fig.frames.forEach((frame, frameId) => {
				figs_h.write(`
const FigureFrame FRAME_${fig.cname}_${frameId} = {
	.center = {${frame.center[0]}, ${frame.center[1]}},
	.triCount = ${frame.triCount},
	.indices = { ${frame.indices.map(i => i + frame.baseVert).join(', ')} }
};
				`);
			});
//----------------------- figure
			let reducer = (acc, val) => acc.concat(val);
			figs_h.write(`
const Figure FIG_${fig.cname} = {
	.frameCount = ${fig.frames.length},
	.frames = { ${fig.frames.map((frame, frameId) => `&FRAME_${fig.cname}_${frameId}`).join(', ')} },
	.verts = { ${fig.frames.map(frame => frame.verts).reduce(reducer).map(v => `{${v[0]}, ${v[1]}}`).join(', ')} }
};
			`);
		});
//----------------------- fin
		figs_h.write(`
#endif`
		);

		figs_h.close();
	}

}

exports.FigureLoader = FigureLoader;

// utils

function CStyleName(str){
	return str.replace(/\./g,'_').toUpperCase();
}

function getBoundingBox(verts){
	let bb = [Infinity,Infinity,0,0];
	verts.forEach(vert => {
		if(vert[0] < bb[0]){
			bb[0] = vert[0];
		}else if(vert[0] > (bb[0] + bb[2])){
			bb[2] = vert[0] - bb[0];
		}
		if(vert[1] < bb[1]){
			bb[1] = vert[1];
		}else if(vert[1] > (bb[1] + bb[3])){
			bb[3] = vert[1] - bb[1];
		}
	});
	return bb;
}

function pathToVerts(str){
	const regex = /[a-zA-Z]|[\-\d\.]+/g;
	let tokens = Array.from(str.matchAll(regex), m => m[0]);
	let res = [];
	let lastOp = 'L', lastPos = [0,0], end;
	for(var i = 0; i < tokens.length; i++){
		let op = tokens[i];
		const allCommands = ['M','m','L','l','H','h','V','v','C','c','S','s','Q','q','T','t','A','a','Z','z'];
		if(allCommands.includes(op)){
			lastOp = op;
		}else{
			op = lastOp;
			i--;
		}
		let pos;
		let getNextNum = () => parseFloat(tokens[++i]);
		let getPos = (rel) => {
			pos = [getNextNum(), getNextNum()];
			if(rel){
				pos[0] += lastPos[0];
				pos[1] += lastPos[1];
			}
		};
		switch(op){
			case 'M':
			case 'L':
				getPos();
				res.push(pos);
				break;
			case 'm':
			case 'l':
				getPos(true);
				res.push(pos);
				break;
			case 'H':
				pos = [
					getNextNum(),
					lastPos[1]
				];
				res.push(pos);
				break;
			case 'h':
				pos = [
					lastPos[0] + getNextNum(),
					lastPos[1]
				];
				res.push(pos);
				break;
			case 'V':
				pos = [
					lastPos[0],
					getNextNum()
				];
				res.push(pos);
				break;
			case 'v':
				pos = [
					lastPos[0],
					lastPos[1] + getNextNum()
				];
				res.push(pos);
				break;
			case 'Z':
			case 'z':
				end = true;
				break;
			//todo:
			case 'C':
			case 'c':
				getPos();
				getPos();
				getPos();
				break;
			case 'S':
			case 's':
			case 'Q':
			case 'q':
				getPos();
				getPos();
				break;
			case 'T':
			case 't':
				getPos();
				break;
			case 'A':
			case 'a':
				getPos();
				getNextNum();
				getNextNum();
				getPos();
				break;
		}
		if(end) break;
		if(pos) lastPos = pos;
	}
	return res;
}

// triangulation

let isTriangleCW = (t) => {
	let a = t[1][0] * t[0][1] + t[2][0] * t[1][1] + t[0][0] * t[2][1];
	let b = t[0][0] * t[1][1] + t[1][0] * t[2][1] + t[2][0] * t[0][1];
	return (a > b);
};

let isPointInTriangle = (p, t) => {
	for(var i=0; i < 3; i++){
		if(isTriangleCW([t[i], t[(i+1)%3], p])){
			return false;
		}
	}
	return true;
};

let earQ = (i, j, k, verts) => {
	let t = [verts[i], verts[j], verts[k]];
	if(isTriangleCW(t)) return false;
	for(var m=0; m < verts.length; m++){
		if((m != i) && (m != j) && (m != k)){
			if(isPointInTriangle(verts[m], t)) return false;
		}
	}
	return true;
};

function triangulate(verts){
	if(verts.length > 2){
		if(isTriangleCW([verts[0], verts[1], verts[2]])){
			verts.reverse();
		}
	}
	let out = {
		verts,
		indices: [],
		triCount: 0
	};
	let l = [], r = [];
	for(let i=0; i < verts.length; i++){
		l[i] = ((i-1) + verts.length) % verts.length;
		r[i] = ((i+1) + verts.length) % verts.length;
	}
	let i = verts.length - 1;
	let iterationCounter = 0;
	while(out.triCount < (verts.length - 2)){
		i = r[i];
		if(earQ(l[i], i, r[i], verts)){
			out.triCount++;
			out.indices.push(l[i]);
			out.indices.push(i);
			out.indices.push(r[i]);
			l[r[i]] = l[i];
			r[l[i]] = r[i];
		}
		if((iterationCounter++) > verts.length) break;
	}
	return out;
}
