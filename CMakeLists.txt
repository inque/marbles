cmake_minimum_required(VERSION 3.11)
project(testgame)

set(INSTALL_BIN_DIR ${PROJECT_SOURCE_DIR}/bin)

add_compile_definitions(CP_USE_DOUBLES=0)
if(MSVC)
    add_compile_definitions(_CRT_SECURE_NO_DEPRECATE=1)
    add_compile_definitions(_CRT_NONSTDC_NO_DEPRECATE=1)
endif()

add_subdirectory(extern)
add_subdirectory(src)
