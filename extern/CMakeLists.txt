cmake_minimum_required(VERSION 3.11)
project(testgame)

include(FetchContent)

#=== raylib ===

find_package(raylib 3.0 QUIET)

if (NOT raylib_FOUND)
  FetchContent_Declare(
    raylib
    URL https://github.com/raysan5/raylib/archive/master.tar.gz
  )
  FetchContent_GetProperties(raylib)
  if (NOT raylib_POPULATED) # Have we downloaded raylib yet?
    set(FETCHCONTENT_QUIET NO)
    FetchContent_Populate(raylib)
    set(BUILD_EXAMPLES OFF CACHE BOOL "" FORCE) # don't build the supplied examples
    add_subdirectory(${raylib_SOURCE_DIR} ${raylib_BINARY_DIR})
  endif()
endif()

#=== chipmunk ===

#find_package(chipmunk 3.7 QUIET)

if (NOT chipmunk_FOUND)
  FetchContent_Declare(
    chipmunk
    URL https://github.com/slembcke/Chipmunk2D/archive/master.tar.gz
  )
  FetchContent_GetProperties(chipmunk)
  if (NOT chipmunk_POPULATED)
    set(FETCHCONTENT_QUIET NO)
    FetchContent_Populate(chipmunk)
    set(BUILD_DEMOS OFF CACHE BOOL "" FORCE)
    set(BUILD_SHARED OFF CACHE BOOL "" FORCE)
    add_subdirectory(${chipmunk_SOURCE_DIR} ${chipmunk_BINARY_DIR})
  endif()
endif()
