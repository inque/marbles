
#pragma once
#include "../graphics.h"

#ifndef FIGURES_H_IMP

extern const FigureFrame FRAME_0;
extern const FigureFrame FRAME_1;
extern const FigureFrame FRAME_2;
extern const Figure FIG_CIRCLE;
extern const FigureFrame FRAME_0;
extern const FigureFrame FRAME_1;
extern const Figure FIG_HAIR;
extern const FigureFrame FRAME_0;
extern const Figure FIG_TEST;

#else
		
const FigureFrame FRAME_CIRCLE_0 = {
	.center = {4.2556732, -0.9582171000000002},
	.triCount = 5,
	.indices = { 6, 0, 1, 6, 1, 2, 6, 2, 3, 6, 3, 4, 6, 4, 5 }
};
				
const FigureFrame FRAME_CIRCLE_1 = {
	.center = {114.9405502, -2.385756720000005},
	.triCount = 4,
	.indices = { 12, 7, 8, 12, 8, 9, 12, 9, 10, 12, 10, 11 }
};
				
const FigureFrame FRAME_CIRCLE_2 = {
	.center = {221.3074582, 3.0924354999999957},
	.triCount = 5,
	.indices = { 19, 13, 14, 19, 14, 15, 19, 15, 16, 19, 16, 17, 19, 17, 18 }
};
				
const Figure FIG_CIRCLE = {
	.frameCount = 3,
	.frames = { &FRAME_CIRCLE_0, &FRAME_CIRCLE_1, &FRAME_CIRCLE_2 },
	.verts = { {32.651282, 4.6708856}, {94.285324, 3.0994635}, {114.71381, 48.670703}, {104.23766, 103.14667}, {46.095047, 115.71804}, {3.1428441, 78.527723}, {6.8094956, 29.813639}, {112.59787, 78.214924}, {120.56121, 16.545365999999994}, {174.08223, 0.9890807799999948}, {219.26954, 22.656762999999994}, {221.67705, 98.771442}, {151.48858, 118.77238}, {225.544, 90.437292}, {222.61813, 41.670562}, {245.14184, 4.147078199999996}, {321.61772, 8.861344399999997}, {329.75534000000005, 61.088661}, {320.45391000000006, 107.61765}, {258.87251000000003, 111.52759} }
};
			
const FigureFrame FRAME_HAIR_0 = {
	.center = {31.492603999999993, 141.34360099999998},
	.triCount = 2,
	.indices = { 3, 0, 1, 3, 1, 2 }
};
				
const FigureFrame FRAME_HAIR_1 = {
	.center = {16.177214100000004, 173.25920469999997},
	.triCount = 3,
	.indices = { 8, 4, 5, 8, 5, 6, 8, 6, 7 }
};
				
const Figure FIG_HAIR = {
	.frameCount = 2,
	.frames = { &FRAME_HAIR_0, &FRAME_HAIR_1 },
	.verts = { {34.816447, 161.73736}, {17.037835999999995, 126.18012999999999}, {86.67072999999999, 130.62479}, {78.52219899999999, 164.70046}, {34.047479, 210.00337}, {18.333257000000003, 193.76533999999998}, {58.142616000000004, 167.57497999999998}, {99.523396, 202.67006999999998}, {73.85683700000001, 216.81286999999998} }
};
			
const FigureFrame FRAME_TEST_0 = {
	.center = {71.23779885, 281.241165},
	.triCount = 4,
	.indices = { 5, 0, 1, 5, 1, 2, 5, 2, 3, 5, 3, 4 }
};
				
const Figure FIG_TEST = {
	.frameCount = 1,
	.frames = { &FRAME_TEST_0 },
	.verts = { {8.3809177, 292.76493}, {14.666606, 236.19373}, {98.475783, 217.33667}, {134.09468, 286.47924}, {94.28532500000001, 345.14566}, {10.476147000000012, 322.09814} }
};
			
#endif