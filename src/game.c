#include "game.h"
#include "gameplay.h"
#include "particles.h"
#include "gameinterface.h"
#include "items.h"
#include "physics.h"
#include "menus.h"
#include "utils.h"
#include "networking.h"
#include "networkingpackets.h"
#include "leveleditor.h"

#include "graphics.h"
#include "generated/figures.h"

#include "raymath.h"
#include "GLFW/glfw3.h"
#include "string.h"
#include "stdio.h"
#include "time.h"

AppState_t app;
GameState_t game;

static int GetDisplayRefreshRate(){
	//GLFWmonitor* monitor = glfwGetWindowMonitor(glfwGetCurrentContext());
	//printf("current window: %p\n", glfwGetCurrentContext());
	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	if(monitor){
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);
		return mode->refreshRate;
	}else{
		return 60;
	}
}

static void DefaultSettings(){
	app.settings = (AppSettings_t){
		.fullScreen = 0,
		.windowSize = {1024, 576},
		.windowPos = {-1, -1},
		.windowMaximized = 0,
		.frameSmoothing = -1,
		.hostPort = 54460,
		.hostGameType = GAME_DEATHMATCH,
		.hostTitle = "Epic Marbles Server"
	};
	sprintf(app.settings.playerName, "Juicy%02i", (rand() % 100) );
	RandomizeCustomizations(&app.settings.customizations);

	int* kb = (int*)&app.settings.keybinds[0].kb;
	kb[KB_GO_LEFT] = KEY_A;
	kb[KB_GO_RIGHT] = KEY_D;
	kb[KB_TURN_LEFT] = KEY_Q;
	kb[KB_TURN_RIGHT] = KEY_E;
	kb[KB_ACTION_PRIMARY] = KEY_W;
	kb[KB_ACTION_SECONDARY] = KEY_S;
	kb[KB_RELOAD] = KEY_R;
	kb[KB_SWAP] = KEY_F;
	kb[KB_DROP_PRIMARY] = KEY_T;
	kb[KB_DROP_SECONDARY] = KEY_G;
	kb[KB_CYCLE] = KEY_TAB;
	kb[KB_SELECT_1] = KEY_ONE;
	kb[KB_SELECT_2] = KEY_TWO;
	kb[KB_SELECT_3] = KEY_THREE;
	kb[KB_SELECT_4] = KEY_FOUR;
}

void LoadSettings(){
	DefaultSettings();
}

void SaveSettings(){
}

static void CreateLocalPlayer(){
	app.localPlayerId = CreatePlayer(0);
	Player_t* localPlayer = &game.players[app.localPlayerId];
	localPlayer->customizations = app.settings.customizations;
	strncpy(localPlayer->name, app.settings.playerName, sizeof(localPlayer->name));
}

void StartGame(AppRole_e role, GameType_e type, int levelId){
	if(app.screen == SCREEN_MAINMENU){
		app.backgroundTransitionOut = 40;
	}
	app.screen = SCREEN_GAME;
	app.inGameSubMenu = SUBMENU_NONE;
	app.isPaused = 0;
	app.role = role;

	if(app.settings.frameSmoothing == -1){
		if(role == ROLE_REMOTE){
			app.frameSmoothing = 1;
		}else{
			app.frameSmoothing = (GetDisplayRefreshRate() == GAME_MAX_TICK_RATE) ? 0 : 1;
		}
	}else{
		app.frameSmoothing = app.settings.frameSmoothing;
	}

	memset(&game, 0, sizeof(game));
	game.type = type;
	
	if( (role == ROLE_LOCAL) || (role == ROLE_HOST) ){
		CreateLocalPlayer();
	}else{
		app.localPlayerId = -1;
	}

	app.cam.target = game.level.spawnPos;
	app.cam.target.x -= 128;

	game.levelId = levelId;
	ResetGame();
}

void ResetGame(){
	game.state = STATE_UNINITIALIZED;
	game.frame = 0;
	game.timer = 0;

	if(game.initialized){
		if(app.role != ROLE_REMOTE){
			for(int playerId = 0; playerId < MAX_PLAYERS; playerId++){
				Player_t* player = &game.players[playerId];
				if(!player->active) continue;
				if(player->isNPC){
					RemovePlayer(playerId);
				}else{
					ResetPlayerStats(playerId);
				}
			}
		}
		CloseGame();
	}else{
		DefineItems();
		DefineCharacters();
		game.initialized = 1;
	}
	if(app.role != ROLE_REMOTE){
		InitializePhysics();
	}

	if(app.role == ROLE_HOST){
		ResponseMessage_t* msg = GetResponseBuffer();
		msg->code = RESPONSE_NEWGAME;
		msg->newGame.gameType = game.type;
		msg->newGame.levelId = game.levelId;
		BroadcastResponse(CHAN_STATUS, 1+sizeof(struct ResponseNewGame_st));

		ResetNetworkingCache();
	}

	if(game.levelId != -1){
		LoadLevel(game.levelId);
	}

}

void CloseGame(){
	CleanUpLevel();
	for(int characterId = 0; characterId < MAX_CHARACTERS; characterId++){
		Character_t* character = &game.characters[characterId];
		if(character->active) RemoveCharacter(characterId);
	}
	for(int itemId = 0; itemId < MAX_ITEMS; itemId++){
		Item_t* item = &game.items[itemId];
		if(item->active) RemoveItem(itemId);
	}
	game.particleSystemState.simpleMaxActiveId = 0;
	game.particleSystemState.simpleNextMinFreeId = 0;
	game.projectileSystemState.projectileMaxActiveId = 0;
	game.projectileSystemState.projectileNextMinFreeId = 0;
	if(app.role != ROLE_REMOTE){
		//cpSpaceDestroy(&game.space);
		memset(&game.space, 0, sizeof(game.space));
	}
}


void GameLoop(){
	app.gameplayJob.refreshRate = (1.0 / GAME_MAX_TICK_RATE);
	//app.graphicsJob.refreshRate = (1.0 / 60);
	app.graphicsJob.refreshRate = (1.0 / GetDisplayRefreshRate());

	double timeNow = GetTime();

	app.gameplayJob.elapsed = (timeNow - app.gameplayJob.lastEnded);
	app.graphicsJob.elapsed = (timeNow - app.graphicsJob.lastEnded);

	app.gameplayJob.shouldUpdate = (!app.gameplayJob.lastStarted) || (app.gameplayJob.elapsed >= app.gameplayJob.refreshRate);
	app.graphicsJob.shouldUpdate = (!app.graphicsJob.lastStarted) || (app.graphicsJob.elapsed >= app.graphicsJob.refreshRate);
	if(!app.frameSmoothing) app.graphicsJob.shouldUpdate = app.gameplayJob.shouldUpdate;

	if(app.gameplayJob.shouldUpdate){
		app.gameplayJob.lastStarted = timeNow;
	}

	if(app.graphicsJob.shouldUpdate){
		app.graphicsJob.lastStarted = timeNow;
		app.uiFrame++; //todo
		app.fineUIFrame = (float)app.uiFrame;
	}

	switch(app.screen){
		case SCREEN_MAINMENU: {
			if(app.gameplayJob.shouldUpdate){
				UpdateNetworking();
			}
			if(app.graphicsJob.shouldUpdate){
				UpdateAppState();
				BeginDrawing();
				DrawMenuBackground(1);
				MainMenuScreen();
				//DrawFigureEx(&FIG_TEST, 0, (Vector2){60, 60}, app.uiFrame*0.01f, WHITE);
				//DrawFigureCustom(&FIG_TEST, 0, (Vector2){60, 60}, app.uiFrame*0.01f, GREEN);
				EndDrawing();
			}
		} break;
		case SCREEN_GAME: {
			if(app.gameplayJob.shouldUpdate){
				if(app.shouldRestartRound){
					ResetGame();
					app.shouldRestartRound = 0;
				}
				if(!app.shouldRestartRound){
					UpdateNetworking();
				}
				if(!app.isPaused){
					UpdateGame();
				}
				//UpdateNetworking();
				if(app.shouldRestartRound){
					ResetGame();
					app.shouldRestartRound = 0;
				}
			}
			if(app.graphicsJob.shouldUpdate){
				DrawGraphics();
			}
		} break;
		case SCREEN_EDITOR: {
			if(app.graphicsJob.shouldUpdate){
				UpdateAppState();
				BeginDrawing();
					DrawGameScene();
					UpdateLevelEditor();
				EndDrawing();
			}
		} break;
	}

	timeNow = GetTime();
	if(app.gameplayJob.shouldUpdate){
		app.gameplayJob.lastEnded = timeNow;
		app.gameplayJob.timeTaken = (app.gameplayJob.lastEnded - app.gameplayJob.lastStarted);
	}
	if(app.graphicsJob.shouldUpdate){
		app.graphicsJob.lastEnded = timeNow;
		app.graphicsJob.timeTaken = (app.graphicsJob.lastEnded - app.graphicsJob.lastStarted);
	}

	if( !(app.gameplayJob.shouldUpdate || app.graphicsJob.shouldUpdate) ){
		float waitTime = 1000 * fminf( (float)(app.gameplayJob.refreshRate - app.gameplayJob.elapsed),
									   (float)(app.graphicsJob.refreshRate - app.graphicsJob.elapsed) );
//		float waitTime = 1000 * fminf( (float)(app.gameplayJob.refreshRate - app.gameplayJob.timeTaken),
//									   (float)(app.graphicsJob.refreshRate - app.graphicsJob.timeTaken) );
		if(waitTime > 0){
			//printf("waiting for: %f\n", waitTime);
			//printf("finished in: %i; %i\n", (int)(app.gameplayJob.timeTaken * 1000), (int)(app.graphicsJob.timeTaken * 1000) );
			//Wait(waitTime);
			Wait(1);
		}
	}
}


/*
void GameLoop(){
	static double timeLastGameplay = 0, timeLastGraphics = 0;
	double timeNow = GetTime();
	double elapsedGameplay = (timeNow - timeLastGameplay);
	double elapsedGraphics = (timeNow - timeLastGraphics);
	float gameplayRefreshRate = (1.0f / GAME_MAX_TICK_RATE);
	float displayRefreshRate = (1.0f / 60);
	bool shouldUpdateGameplay = (!timeLastGameplay) || (elapsedGameplay >= gameplayRefreshRate);
	bool shouldUpdateGraphics = (!timeLastGameplay) || (elapsedGraphics >= displayRefreshRate);

	if(shouldUpdateGraphics){
		//app.fineUIFrame = (float)timeNow * (1000.0f / 60.0f);
		//app.uiFrame = (int)app.fineUIFrame;
		app.uiFrame++;
		app.fineUIFrame = (float)app.uiFrame;
	}

	if(shouldUpdateGameplay) timeLastGameplay = timeNow;
	if(shouldUpdateGraphics) timeLastGraphics = timeNow;

	switch(app.screen){
		case SCREEN_MAINMENU: {
			if(shouldUpdateGraphics){
				UpdateAppState();
				BeginDrawing();
				DrawMenuBackground(1.0f);
				MainMenuScreen();
				EndDrawing();
			}
		} break;
		case SCREEN_GAME: {
			if(shouldUpdateGameplay){
				if(!app.isPaused) UpdateGame();
			}
			if(shouldUpdateGraphics){
				DrawGraphics();
			}
		} break;
	}

	//if(shouldUpdateGameplay) timeLastGameplay = timeNow;
	//if(shouldUpdateGraphics) timeLastGraphics = timeNow;
	if( !(shouldUpdateGameplay || shouldUpdateGraphics) ){
		float waitTime = 1000 * fminf( (gameplayRefreshRate - elapsedGameplay), (displayRefreshRate - elapsedGraphics) );
		Wait(waitTime);
		//printf("sleeping for %f\n", waitTime);
	}

}
*/

void StartGameLoop(){
	while( (!WindowShouldClose()) && (!app.shouldClose) ){
		GameLoop();
	}
}

void UpdatePlayerControls(){
	if(IsKeyPressed(KEY_F8)) app.debugMode ^= 1;

	if(app.gameInterfaceState.chatBoxEnabled) return;

	if(app.localPlayerId == -1) return;
	Player_t* localPlayer = &game.players[app.localPlayerId];
	if(localPlayer->characterId == -1) return;
	Character_t* localCharacter = &game.characters[localPlayer->characterId];

	AppPlayerKeybinds_t* keybinds = &app.settings.keybinds[0];
	int gamepadNum = keybinds->gamepadNum;
	bool useGamepad = IsGamepadAvailable(gamepadNum);
	/*
	if(useGamepad && (!keybinds->useGamepad)){
		const char* gamepadName = GetGamepadName(gamepadNum);
		strncpy(&keybinds->gamepadName, gamepadName, sizeof(keybinds->gamepadName));
	}
	*/
	keybinds->useGamepad = useGamepad;

	localPlayer->controls.axis_h = (float)(IsKeyDown(keybinds->kb[KB_GO_RIGHT]) - IsKeyDown(keybinds->kb[KB_GO_LEFT]));
	localPlayer->controls.axis_r = (float)(IsKeyDown(keybinds->kb[KB_TURN_RIGHT]) - IsKeyDown(keybinds->kb[KB_TURN_LEFT]));
	if(useGamepad){
		localPlayer->controls.axis_h += GetGamepadAxisMovement(gamepadNum, GAMEPAD_AXIS_LEFT_X);
		localPlayer->controls.axis_r -= 0.5f * GetGamepadAxisMovement(gamepadNum, GAMEPAD_AXIS_LEFT_TRIGGER);
		localPlayer->controls.axis_r += 0.5f * GetGamepadAxisMovement(gamepadNum, GAMEPAD_AXIS_RIGHT_TRIGGER);
	}

	localPlayer->controls.buttonsLast = localPlayer->controls.buttons;
	localPlayer->controls.buttons = 0;
	if(IsKeyDown(keybinds->kb[KB_ACTION_PRIMARY]) || (useGamepad && IsGamepadButtonDown(gamepadNum, GAMEPAD_BUTTON_RIGHT_TRIGGER_1))){
		localPlayer->controls.buttons |= BUTTON_PRIMARY;
	}
	if(IsKeyDown(keybinds->kb[KB_ACTION_SECONDARY]) || (useGamepad && IsGamepadButtonDown(gamepadNum, GAMEPAD_BUTTON_LEFT_TRIGGER_1))){
		localPlayer->controls.buttons |= BUTTON_SECONDARY;
	}
	if(IsKeyDown(keybinds->kb[KB_CYCLE]) || (useGamepad && IsGamepadButtonPressed(gamepadNum, GAMEPAD_BUTTON_LEFT_THUMB))){
		localPlayer->controls.buttons |= BUTTON_CYCLE;
	}

	if(IsKeyPressed(keybinds->kb[KB_DROP_PRIMARY]) || (useGamepad && IsGamepadButtonPressed(gamepadNum, GAMEPAD_BUTTON_RIGHT_FACE_UP))){
		if(localCharacter->inventoryItemIds[0] != -1){
			AddPlayerAction(app.localPlayerId, (PlayerAction_t){.type = ACTION_THROW, .parameter = 0});
		}
	}
	if(IsKeyPressed(keybinds->kb[KB_DROP_SECONDARY]) || (useGamepad && IsGamepadButtonPressed(gamepadNum, GAMEPAD_BUTTON_RIGHT_FACE_RIGHT))){
		if(localCharacter->inventoryItemIds[localCharacter->inventorySecondarySelectionSlot] != -1){
			AddPlayerAction(app.localPlayerId, (PlayerAction_t){.type = ACTION_THROW, .parameter = 1});
		}
	}

	if(IsKeyPressed(keybinds->kb[KB_SELECT_1]) || (useGamepad && IsGamepadButtonPressed(gamepadNum, GAMEPAD_BUTTON_LEFT_FACE_UP))){
		if( (localCharacter->lookingAtItemId != -1) && (localCharacter->inventoryItemIds[0] == -1) ){
			AddPlayerAction(app.localPlayerId, (PlayerAction_t){.type = ACTION_PICKUP, .parameter = 0});
		}
	}
	for(int i = 1; i < 4; i++){
		if(IsKeyPressed(keybinds->kb[KB_SELECT_1 + i]) || (useGamepad && IsGamepadButtonPressed(gamepadNum, GAMEPAD_BUTTON_LEFT_FACE_UP + i))){
			if( (localCharacter->lookingAtItemId != -1) && (localCharacter->inventoryItemIds[i] == -1) ){
				AddPlayerAction(app.localPlayerId, (PlayerAction_t){.type = ACTION_PICKUP, .parameter = i});
			}else{
				AddPlayerAction(app.localPlayerId, (PlayerAction_t){.type = ACTION_SELECT_SECONDARY, .parameter = i});
			}
		}
	}

	if(IsKeyPressed(keybinds->kb[KB_SWAP]) || (useGamepad && IsGamepadButtonPressed(gamepadNum, GAMEPAD_BUTTON_RIGHT_THUMB))){
		AddPlayerAction(app.localPlayerId, (PlayerAction_t){.type = ACTION_SWAP});
	}


	if(app.role == ROLE_REMOTE){
		RequestMessage_t* msg = GetRequestBuffer();
		msg->code = REQUEST_INPUTS;
		PackControls(&msg->inputs.controls, &localPlayer->controls);
		SendRequest(CHAN_GAME, 1+sizeof(struct RequestInputs_st));
	}else{
		if(IsKeyPressed(KEY_F5)){
			//ResetGameState(false);
			app.shouldRestartRound = 1;
		}
	}

}

void UpdateGameLogic(){
	switch(game.type){
		case GAME_REGULAR:
			Scenario_Regular_GameTick();
			break;
		case GAME_DEATHMATCH:
			Scenario_Deathmatch_GameTick();
			break;
	}
	game.frame++;
}

void UpdateGame(){
	if(app.role != ROLE_REMOTE){
		UpdatePhysics();
		UpdateProjectiles();
		UpdatePlayerControls();
		UpdatePlayers();
		UpdateCharacters();
		UpdateItems();
		UpdateLevel();
		UpdateParticles();
		UpdateGameLogic();
	}else{
		UpdatePlayerControls();
		UpdateParticles();
	}
}

void UpdateAppState(){
//	if( (IsKeyDown(KEY_LEFT_ALT) || IsKeyDown(KEY_RIGHT_ALT)) && (IsKeyPressed(KEY_ENTER)) ){
//		ToggleFullscreen();
//	}

	app.width = GetScreenWidth();
	app.height = GetScreenHeight();
	app.halfWidth = app.width / 2;
	app.halfHeight = app.height / 2;

	app.cam.offset.x = (float)app.halfWidth;
	app.cam.offset.y = (float)app.halfHeight;

	app.deltaTimeUI = GetFrameTime();
	double timeNow = GetTime();
	app.deltaTimeGame = (((float)timeNow - app.gameplayJob.lastEnded) / (float)app.gameplayJob.refreshRate);
	//app.fineGameFrame = (float)game.frame + fminf(1.0f, app.deltaTimeUI / (1000.0f / GAME_MAX_TICK_RATE));
	//app.fineGameFrame = (float)game.frame + fminf(1.0f, ((float)timeNow - app.gameplayJob.lastEnded) / (float)app.gameplayJob.refreshRate );
	//app.fineGameFrame = (float)game.frame + (((float)timeNow - app.gameplayJob.lastEnded) / (float)app.gameplayJob.refreshRate);
	app.fineGameFrame = (float)game.frame + fminf(1.0f, app.deltaTimeGame);
	//printf("frame time: %f\n", app.deltaTimeUI);
}

void DrawGameScene(){
	DrawLevelBackground();
	BeginMode2D(app.cam);
		DrawLevel();
		DrawCharacters();
		DrawItems();
		DrawProjectiles();
		DrawParticles();
	EndMode2D();
}

void DrawGraphics(){
	UpdateAppState();
	BeginDrawing();
		DrawGameScene();
		DrawHUD();
		if(app.backgroundTransitionOut){
			DrawMenuBackground((float)app.backgroundTransitionOut / 40.1f);
			app.backgroundTransitionOut--;
		}
	EndDrawing();
}

//utils

int mod2(int a, int b){
	int r = a % b;
	return r < 0 ? r + b : r;
}
