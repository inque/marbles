#include "gameinterface.h"
#include "menus.h"
#include "game.h"
#include "networkingpackets.h"
#include "raymath.h"
#include "raygui.h"
#include "stdio.h"
#include "string.h"
#include "stdbool.h"

#define mainColor DARKBLUE
static const int fontSizeBig = 32;

void UpdateViewBounds(){
	Vector2 topLeft = GetScreenToWorld2D(Vector2Zero(), app.cam);
	Vector2 bottomRight = GetScreenToWorld2D((Vector2){(float)app.width, (float)app.height}, app.cam);
	//app.viewBounds = (Rectangle){topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y};
	app.viewBounds = (Rectangle){topLeft.x - 100, topLeft.y - 100, bottomRight.x - topLeft.x + 200, bottomRight.y - topLeft.y + 200};
}

static void UpdateGameCamera(){
	if(!IsKeyDown(KEY_F10)){
		if(app.localPlayerId != -1){
			Player_t* localPlayer = &game.players[app.localPlayerId];
			if(localPlayer->characterId != -1){
				Character_t* character = &game.characters[localPlayer->characterId];
				//follow player smoothly
				float minSpeed = 30.0f;
				float minEffectLength = 6.0f;
				//float fractionSpeed = 0.98f;
				float fractionSpeed = 1.5f;
				Vector2 diff = Vector2Subtract(GetInterpolatedPos(&character->tform), app.cam.target);
				float length = Vector2Length(diff);
				if(length > minEffectLength){
					float speed = fmaxf(fractionSpeed * length, minSpeed);
					app.cam.target = Vector2Add(app.cam.target, Vector2Scale(diff, speed * app.deltaTimeUI / length));
				}
			}
			app.cam.zoom = 1.0f;
		}
	}
	UpdateViewBounds();
}

static void DrawDebugOverlay(){
	char text[4096];
	//sprintf(text, "gt: %i. gs: %i. t: %i. f: %i.", game.type, game.state, game.timer, game.frame);
	sprintf(text, "lv polys: %i", app.debugInfo.levelPolysDrawn);
	DrawText(text, 10, app.height - 20, 18, BLACK);
	sprintf(text, "interpolation: %i. fps: %i", app.frameSmoothing, GetFPS());
	DrawText(text, 10, app.height - 40, 18, BLACK);
	if(app.localPlayerId != -1){
		Player_t* localPlayer = &game.players[app.localPlayerId];
		if(localPlayer->characterId != -1){
			Character_t* localCharacter = &game.characters[localPlayer->characterId];
			sprintf(text, "char %i. a: %f. acc: %f", localPlayer->characterId, GetAngle(&localCharacter->tform), localCharacter->primaryItemAngleAcceleration);

		}else{
			strcpy(text, "Dead.");
		}
		DrawText(text, 10, app.height - 60, 18, BLACK);
	}
}

static void DrawInventorySlot(Character_t* character, Vector2 pos, int id){
	float invBoxSize = 40;
	float invBoxRoundness = 0.3f;
	int invBoxSegments = 16;
	int invBoxLineThick = 3;
	char text[64];
	int itemId = character->inventoryItemIds[id];
	bool isEmpty = (itemId == -1);
	//Color invBoxColor = (!id) ? (Color){180, 100, 100, 255} : (Color){100, 180, 100, 255};
	Color invBoxColor;
	if(!id){
		//primary
		invBoxColor = (Color){180, 100, 100, 255};
	}else{
		//secondary
		if(id == character->inventorySecondarySelectionSlot){
			invBoxColor = (Color){100, 180, 100, 255};
		}else{
			invBoxColor = (Color){120, 200, 120, 128};
		}
	}
	if( (character->lookingAtItemId != -1) && isEmpty && (((app.uiFrame + id*2) >> 3) & 1) ){
		//flashing
		//invBoxColor = (Color){160, 240, 175, 255};
		invBoxColor = (Color){160, 240, 175, 64};
	}
	DrawRectangleRoundedLines((Rectangle){pos.x, pos.y, invBoxSize, invBoxSize}, invBoxRoundness, invBoxSegments, invBoxLineThick, invBoxColor);
	if(!isEmpty){
		Item_t* item = &game.items[itemId];
		DrawItemIcon(item->type, (Vector2){pos.x + 20, pos.y + 20}, -2.2f, 0.65f);
	}
	snprintf(text, sizeof(text), "%i", (id+1));
	DrawText(text, pos.x + 6, pos.y + 4, 14, (Color){80, 80, 80, (id ? 255 : 100)});
}

static void DrawCharacterStats(){

	//all characters on screen
	for(int characterId = 0; characterId < MAX_CHARACTERS; characterId++){
		Character_t *character = &game.characters[characterId];
		if(!character->active) continue;
		if(!IsInView(&character->tform)) continue;
		Vector2 pos = GetWorldToScreen2D(GetInterpolatedPos(&character->tform), app.cam);
		if(character->healthLastDamageFrame != -1){
			int elapsed = (game.frame - character->healthLastDamageFrame);
			if( (elapsed < 60) || ((elapsed < 80) && ((game.frame>>2)&1)) ){
				if(character->health <= 0){
					character->healthAnimatedValue = 0;
				}else{
					int diff = (character->health - character->healthAnimatedValue);
					if(diff){
						//diff = 1 + (diff / 5);
						diff = (int)ceilf((float)diff / 5.0f);
						character->healthAnimatedValue += diff;
					}
					if(character->healthAnimatedValue > 100) character->healthAnimatedValue = 100;
				}
				DrawRectangle(pos.x-35, pos.y-80, 70, 16, DARKGRAY);
				DrawRectangle(pos.x-32, pos.y-78, (int)(((float)character->healthAnimatedValue / 100) * 66), 12, GREEN);
			}
		}
		if(character->playerId != -1){
			Player_t* player = &game.players[character->playerId];
			//if( (!player->isNPC) && (character->playerId != app.localPlayerId) ){
			if(player->name[0] && (character->playerId != app.localPlayerId)){
				int tagWidth = MeasureText(player->name, 14);
				Vector2 tagPos = (Vector2){(pos.x - tagWidth/2), (pos.y - character->tform.size.y)};
				DrawRectangle(tagPos.x - 4, tagPos.y - 2, tagWidth + 8, 16, (Color){0, 0, 0, 40});
				DrawText(player->name, tagPos.x, tagPos.y, 14, (Color){255, 255, 255, 130});
			}
		}
	}

	//now specifically ours
	if(app.localPlayerId == -1) return;
	Player_t* player = &game.players[app.localPlayerId];
	if(player->characterId == -1) return;
	Character_t* character = &game.characters[player->characterId];

	//grab item icon
	if(character->lookingAtItemId != -1){
		Item_t* lookingAtItem = &game.items[character->lookingAtItemId];
		Vector2 drawIconAt = GetWorldToScreen2D(Vector2Lerp(GetPos(&character->tform), GetPos(&lookingAtItem->tform), 0.4f + 0.2f * sinf(0.375 * app.fineGameFrame) ), app.cam);
		DrawText("<#>", drawIconAt.x - 12, drawIconAt.y - 9, 18, SKYBLUE);
	}

	//inventory
	Vector2 invPos = (Vector2){20, 20};
	DrawInventorySlot(character, invPos, 0);
	invPos.x += 55;
	for(int i=1; i < CHAR_MAX_INVENTORY_SLOTS; i++){
		DrawInventorySlot(character, invPos, i);
		invPos.x += 50;
	}

}

static void DrawChat(){
	Vector2 chatPos = (Vector2){(float)app.width - 280, (float)app.height - (float)app.height / 4};
	Rectangle chatBox = (Rectangle){chatPos.x, chatPos.y + 22, 190, 22};
	static char tempBuffer[8192];
	static bool chatBoxEnabled;
	static char chatBoxText[CHAT_MESSAGE_LEN];

	for(int i = 1; i <= app.gameInterfaceState.chatMessagesShowing; i++){
		int messageId = mod2(app.gameInterfaceState.chatMessageNext - i, CHAT_HISTORY_MAX);
		ChatMessage_t* message = &app.gameInterfaceState.chatHistory[messageId];
		int elapsed = (app.uiFrame - message->sentFrame);
		float offsetX = (elapsed < 20) ? 40*powf(1-(float)elapsed/20, 2) : 0;
		char* text = message->text;
		Color borderColor = GRAY;
		switch(message->type){
			case CHATMSG_SPEC_PLAYER: {
				Player_t* sender = &game.players[message->senderId];
				sprintf(tempBuffer, "%s: %s", sender->name, message->text);
				text = (char*)&tempBuffer;
			} break;
		}
		if(elapsed > CHAT_STARTS_FADING_AT){
			borderColor.a = (char)(255-254*((float)(elapsed - CHAT_STARTS_FADING_AT - 1) / (CHAT_SHOWING_DURATION - CHAT_STARTS_FADING_AT)));
		}
		DrawText(text, chatPos.x + offsetX, chatPos.y, 16, borderColor);
		chatPos.y -= 16;
		if(elapsed > CHAT_SHOWING_DURATION){
			app.gameInterfaceState.chatMessagesShowing--;
		}
	}

	if(app.gameInterfaceState.chatBoxEnabled){
		GuiTextBox(chatBox, chatBoxText, CHAT_MESSAGE_LEN, true);
		if(IsKeyPressed(KEY_ENTER)){
			PlayerSay(app.localPlayerId, chatBoxText);
			app.gameInterfaceState.chatBoxEnabled = false;
		}else if(IsKeyPressed(KEY_ESCAPE)){
			app.gameInterfaceState.chatBoxEnabled = false;
		}
	}else{
		if(IsKeyPressed(KEY_ENTER)){
			app.gameInterfaceState.chatBoxEnabled = true;
			chatBoxText[0] = 0;
		}
	}

}

void DrawHUD(){
	bool localGame = (app.role == ROLE_LOCAL);
	if((!app.gameInterfaceState.chatBoxEnabled) && (IsKeyPressed(KEY_ESCAPE) || (app.settings.keybinds[0].useGamepad && IsGamepadButtonPressed(app.settings.keybinds[0].gamepadNum, GAMEPAD_BUTTON_MIDDLE)))){
		if(app.inGameSubMenu != SUBMENU_NONE){
			app.inGameSubMenu = SUBMENU_NONE;
			if(localGame) app.isPaused = 0;
		}else{
			app.inGameSubMenu = SUBMENU_ESC;
			if(localGame) app.isPaused = 1;
		}
	}

	switch(app.inGameSubMenu){
		case SUBMENU_NONE: {
			UpdateGameCamera();
			DrawCharacterStats();
			switch(game.type){
				case GAME_REGULAR:
					Scenario_Regular_UI();
					break;
				case GAME_DEATHMATCH:
					Scenario_Deathmatch_UI();
					break;
			}
			DrawChat();
			if(app.gameInterfaceState.hintTimer){
				float y = app.height - app.height / 4;
				float opacity = 0.8f;
				if(app.gameInterfaceState.hintFrames < 40) opacity *= app.gameInterfaceState.hintFrames / 40.0f;
				else if(app.gameInterfaceState.hintTimer < 60) opacity *= app.gameInterfaceState.hintTimer / 60.0f;
				DrawText(app.gameInterfaceState.hintText, 20, y, fontSizeBig, (Color){30, 100, 45, (char)(opacity * 255)});
				app.gameInterfaceState.hintFrames++;
				app.gameInterfaceState.hintTimer--;
			}

			if(app.debugMode) DrawDebugOverlay();

		} break;
		case SUBMENU_ESC: {
			InGameEscScreen();
		}
	}

}

static void DrawTimer(bool isCountDown){
	char text[2048];
	int fontSize = 24;
	int textlen = 0;
	if(isCountDown){

	}else{
		int minutes = (game.timer / 3600);
		int seconds = (game.timer - (minutes * 3600)) / 60;
		int micro = (int)((float)(game.timer % 60) / 60.0f * 10.0f);
		textlen = sprintf(text, "%02i:%02i.%i", minutes, seconds, micro);
	}
	DrawText(text, (app.width - 20 - textlen*16), 16, fontSize, mainColor);
}

static void DrawPlayersList(){
	char text[4196];
	static bool playerAddedToList[MAX_PLAYERS];
	static int playerList[MAX_PLAYERS];
	static int playerListSize;

	Vector2 pos = (Vector2){80, app.height / 6};
	Vector2 boxSize = (Vector2){(app.width - pos.x - 80), (app.height - pos.y - (app.height / 8))};
	DrawRectangle(pos.x, pos.y, boxSize.x, boxSize.y, (Color){0, 0, 0, 80});

	pos.x += 10;
	pos.y += 20;

	for(int playerId = 0; playerId < MAX_PLAYERS; playerId++){
		if(playerAddedToList[playerId]) continue;
		Player_t* player = &game.players[playerId];
		if(!player->active) continue;
		playerList[playerListSize++] = playerId;
		playerAddedToList[playerId] = true;
	}
	for(int i=0; i < playerListSize-1; i++){
		Player_t* playerA = &game.players[playerList[i]];
		Player_t* playerB = &game.players[playerList[i+1]];
		if(playerB->stats.killCount > playerA->stats.killCount){
			int tmp = playerList[i+1];
			playerList[i+1] = playerList[i];
			playerList[i] = tmp;
		}
	}


	int colSize = (int)boxSize.x / 7;
	int columns[] = {pos.x + colSize, pos.x + colSize*3, pos.x + colSize*4, pos.x + colSize*6};
	int fontSize = 20;
	Color textColor = WHITE;
	Color headerColor = (Color){240, 240, 240, 100};

	DrawText("Player", columns[0], pos.y, fontSize, headerColor);
	DrawText("Kills",  columns[1], pos.y, fontSize, headerColor);
	DrawText("Deaths", columns[2], pos.y, fontSize, headerColor);
	DrawText("Ping",   columns[3], pos.y, fontSize, headerColor);
	pos.y += 24*2;

	for(int i=0; i < playerListSize; i++){
		Player_t* player = &game.players[playerList[i]];
		if(!player->active) continue;

		DrawText(player->name, columns[0], pos.y, fontSize, textColor);
		sprintf(text, "%i", player->stats.killCount);
		DrawText(text, columns[1], pos.y, fontSize, textColor);
		sprintf(text, "%i", player->stats.deathCount);
		DrawText(text, columns[2], pos.y, fontSize, textColor);

		pos.y += 24;
	}

	/*
	for(int playerId = 0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = &game.players[playerId];
		if(!player->active) continue;

		DrawText(player->name, columns[0], pos.y, fontSize, textColor);
		sprintf(text, "%i", player->stats.killCount);
		DrawText(text, columns[1], pos.y, fontSize, textColor);
		sprintf(text, "%i", player->stats.deathCount);
		DrawText(text, columns[2], pos.y, fontSize, textColor);

		pos.y += 24;
	}
	 */



}

void Scenario_Regular_UI(){
	static int tmpEndTimer;
	switch(game.state){
		case STATE_STARTING: {

			if(((game.frame >> 3)+1) & 1){
				DrawText("Get ready!", app.halfWidth - 70, app.halfHeight, fontSizeBig, mainColor);
			}
		} break;
		case STATE_PLAYING: {
			DrawTimer(false);

		} break;
		case STATE_ENDED: {

			DrawText("Level complete!", app.halfWidth - 70, app.halfHeight - 20, fontSizeBig, mainColor);
			DrawTimer(false);
			tmpEndTimer++;
			if(tmpEndTimer > 500){
				DrawRectangle(0, 0, app.width, app.height, (Color){0,0,0, (char)(255*((tmpEndTimer-500) / 60.0f)) });
			}
			if(tmpEndTimer > 560){
				app.screen = SCREEN_MAINMENU;
				app.mainMenuScreen = MAINMENU_CREDITS;
			}
		} break;
		default: break;
	}
}

void Scenario_Deathmatch_UI(){
	switch(game.state){
	case STATE_STARTING: {
		DrawTimer(false);
		if(game.timer < 60){
			if(((game.frame >> 3)+1) & 1){
				DrawText("Get ready!", app.halfWidth - 70, app.halfHeight, fontSizeBig, mainColor);
			}
		}else if(game.timer > 120){
			DrawPlayersList();
		}
	} break;
	case STATE_PLAYING: {
		DrawTimer(false);

		if(IsKeyDown(KEY_H) && (!app.gameInterfaceState.chatBoxEnabled)){
			DrawPlayersList();
		}

	} break;
	case STATE_ENDED: {
		
		DrawTimer(false);
		DrawPlayersList();

	} break;
	default: break;
	}
}

void PlayerSay(int playerId, const char* text){
	if(app.role == ROLE_REMOTE){
		RequestMessage_t* msg = GetRequestBuffer();
		msg->code = REQUEST_CHATMSG;
		strncpy((char*)&msg->chat.text, text, sizeof(msg->chat.text));
		SendRequest(CHAN_STATUS, 1+sizeof(struct RequestChat_st));
	}else{
		//Player_t* player = &game.players[playerId];
		//if(player->characterId != -1){
			SendChatMessage(CHATMSG_SPEC_PLAYER, playerId, text, 0);
		//}else{
		//	...
		//}
	}
}

void SendChatMessage(ChatMessageType_e type, int senderId, const char* text, int variation){
	ChatMessage_t* message = &app.gameInterfaceState.chatHistory[app.gameInterfaceState.chatMessageNext];
	message->type = type;
	message->senderId = senderId;
	message->variation = variation;
	strncpy(message->text, text, CHAT_MESSAGE_LEN);
	message->sentFrame = app.uiFrame;
	app.gameInterfaceState.chatMessageNext = (app.gameInterfaceState.chatMessageNext + 1) % CHAT_HISTORY_MAX;
	if(app.gameInterfaceState.chatMessagesTotal < CHAT_HISTORY_MAX){
		app.gameInterfaceState.chatMessagesTotal++;
	}
	if(app.gameInterfaceState.chatMessagesShowing < CHAT_SHOWING_MAX){
		app.gameInterfaceState.chatMessagesShowing++;
	}

	if(app.role == ROLE_HOST){
		ResponseMessage_t* msg = GetResponseBuffer();
		msg->code = RESPONSE_CHATMSG;
		msg->chat.type = (char)type;
		msg->chat.senderId = (char)senderId;
		msg->chat.variation = (char)variation;
		strncpy((char*)&msg->chat.text, text, sizeof(msg->chat.text));
		BroadcastResponse(CHAN_STATUS, 1+sizeof(struct ResponseChat_st));
	}

}
