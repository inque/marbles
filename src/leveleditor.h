#pragma once
#include "raymath.h"
#include "level.h"

typedef enum{
	LVE_TOOL_SELECT,
	LVE_TOOL_MOVE,
	LVE_TOOL_VERTEX,
	LVE_TOOL_RECT,
	LVE_TOOL_TRIGGER
} LevelEditorTool_e;

static char* toolNames[] = {
	"Select",
	"Move",
	"Vertex Edit",
	"Rectangle",
	"Trigger"
};

typedef enum{
	LVE_SEL_NONE,
	LVE_SEL_LAYER,
	LVE_SEL_POLY
} LevelEditorSelectionTarget_e;

typedef struct{
	Color fillColor, borderColor;
} LevelEditorSwatch_t;

typedef struct{
	LevelEditorTool_e tool;
	Vector2 mousePosLast;
	bool isDragging;
	Vector2 dragOrigin;
	bool isHoveringOverWindow;
	char* bottomTooltip_p;
	int showTriggers;
	LevelEditorSelectionTarget_e selectionTarget;
	int layerSelectedId;
	int areWeRenamingSelectedLayer; //text
	int areWeModifyingLayerTexturePath; //text
	int polysSelected;
	int polySelection[LV_MAX_POLYGONS];
	bool vertToolDragging;
	int vertToolPolyId;
	int vertToolVertexId;
	//Level_t savedState;
} LevelEditorState_t;

extern LevelEditorState_t* editor;

void StartLevelEditor();
void CloseLevelEditor();
void UpdateLevelEditor();
