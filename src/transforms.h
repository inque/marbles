#pragma once
#include "raymath.h"
#include "chipmunk/chipmunk.h"
#include "stdbool.h"

#define toCpVect(v) cpv(v.x, v.y)
#define toVector2(v) (Vector2){v.x, v.y}

typedef struct{
	Vector2 pos[2];
	float angle[2];
	int frameReceived[2];
	Vector2 size;
} Transform_t;

Transform_t NewTransform(Vector2 pos, float angle);
void UpdateTransform(Transform_t* tform, cpBody* body);
void SetTransformData(Transform_t* tform, Vector2 pos, float angle);
void SetTransformStatic(Transform_t* tform, Vector2 pos, float angle);
bool DoesIntersectRect(Transform_t* tform, Rectangle rec);
bool IsInView(Transform_t* tform);
bool HasChanged(Transform_t* tform);
Vector2 GetPos(Transform_t* tform);
float GetAngle(Transform_t* tform);
Rectangle GetBounds(Transform_t* tform);
Vector2 GetInterpolatedPos(Transform_t* tform);
float GetInterpolatedAngle(Transform_t* tform);
