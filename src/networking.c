#include "networking.h"
#include "game.h"
#include "players.h"
#include "masterserver.h"
#include "networkingpackets.h"
#include "masterserverpackets.h"
#define ENET_IMPLEMENTATION
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include "enetheaders.h"
#include "stdio.h"

typedef struct{
	ENetHost* host;
	ENetPeer* peer;
	RequestMessage_t request;
	ResponseMessage_t response;
	int lastSentFrame;
	int lastReceivedFrame;

	int lastSentProjectileId;
} NetworkingState_t;

static NetworkingState_t networking;


static int InterceptCallback(ENetHost* host, ENetEvent* event){
	//printf("intercept received\n");
	InfoRequestMessage_t* msg;
	enet_host_get_received_data(host, (enet_uint8**)&msg);
	if(msg->magic != NET_MAGIC_ID) return 0;
	return ProcessInfoRequest(msg) ? 1 : 0;
}

void InitNetworking(){
	enet_initialize();
	InitializeMasterServerConnection();
}

bool NetworkHostCreate(){
	CleanUpNetworking();

	ENetAddress address = (ENetAddress){
		.host = ENET_HOST_ANY,
		.port = app.settings.hostPort
	};
	networking.host = enet_host_create(&address, MAX_PLAYERS, CHAN__COUNT, 0, 0);
	if(networking.host == NULL){
		printf("Error creating a server.\n");
		return false;
	}

	enet_host_set_intercept(networking.host, (ENetInterceptCallback)InterceptCallback);

	return true;
}

bool NetworkHostJoin(uint32_t* ip, uint16_t port){
	CleanUpNetworking();
	app.role = ROLE_REMOTE;

	ENetAddress address;
	memcpy(&address.host, ip, sizeof(address.host));
	address.port = port;
	address.sin6_scope_id = 0;

	app.screen = SCREEN_MAINMENU;
	app.mainMenuScreen = MAINMENU_LOADING;
	app.gameInterfaceState.loadingAbortable = true;
	app.gameInterfaceState.loadingFailed = false;
	strcpy((char*)&app.gameInterfaceState.loadingTextHeader, "Connecting...");
	//strcpy((char*)&app.gameInterfaceState.loadingTextSub, "ass dsfjfdljkfdljk jksdjklfdsljk");
	app.gameInterfaceState.loadingTextSub[0] = 0;
	//enet_address_get_host_ip(&address, (char*)&app.gameInterfaceState.loadingTextSub, sizeof(app.gameInterfaceState.loadingTextSub));

	networking.host = enet_host_create(NULL, 1, CHAN__COUNT, 0, 0);
	if(!networking.host){
		printf("something went wrong while creating host\n");
	}
	networking.peer = enet_host_connect(networking.host, &address, 3, 0);
	if(!networking.peer){
		printf("something went wrong while creating peer\n");
	}

	return true;
}

void CleanUpNetworking(){
	if(app.role == ROLE_HOST){
		for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
			Player_t* player = &game.players[playerId];
			if(!player->active) continue;
			if(!player->session.connected) continue;
			enet_peer_disconnect_now(player->session.peer, 0);
			player->session.connected = 0;
		}
	}

	if(networking.peer){
		enet_peer_disconnect_now(networking.peer, 0);
		networking.peer = NULL;
	}

	if(networking.host){
		enet_host_destroy(networking.host);
		networking.host = NULL;
	}
}

void ResetNetworkingCache(){
	networking.lastReceivedFrame = -1;
	networking.lastSentFrame = -1;
	networking.lastSentProjectileId = 0;
}

static void SendResponse(ENetPeer* peer, int channel, int bytes){
	bool reliable = (channel == CHAN_STATUS);
	ENetPacketFlag flag = reliable ? ENET_PACKET_FLAG_RELIABLE : ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT;
	ENetPacket* packet = enet_packet_create((void*)&networking.response, bytes, flag);
	enet_peer_send(peer, channel, packet);
	//enet_host_flush(networking.host);
}

static void ProcessRequest(RequestMessage_t* msg, ENetPeer* peer, int bytes){
	//printf("[REQ] 0x%x\n", msg->code);
	if(!peer->data){
		switch(msg->code){
			case REQUEST_AUTH: {
				bool failed = false;
				int playerId = CreatePlayer(0);
				if(playerId == -1) failed = true;

				if(failed){
					networking.response.code = RESPONSE_AUTH;
					networking.response.auth.succeded = false;
					strcpy((char*)&networking.response.auth.failure.errorMessage, "You are banned! Fool!");
					SendResponse(peer, CHAN_STATUS, 1+1+sizeof(struct ResponseAuthFailure_st));
					//enet_host_flush(networking.host);
					enet_peer_disconnect_now(peer, 0);
				}else{
					Player_t* player = &game.players[playerId];
					player->session.connected = 1;
					player->session.peer = peer;
					peer->data = (void*)(size_t)playerId;
					player->session.localAccountId = 42;
					strncpy(player->name, msg->auth.username, sizeof(msg->auth.username));
					player->name[sizeof(player->name)-1] = 0;
					UnpackCustomizations(&player->customizations, &msg->auth.customizations);

					networking.response.code = RESPONSE_AUTH;
					networking.response.auth.succeded = true;
					networking.response.auth.success.playerId = (char)playerId;
					SendResponse(peer, CHAN_STATUS, 1+1+sizeof(struct ResponseAuthSuccess_st));

					networking.response.code = RESPONSE_NEWGAME;
					networking.response.newGame.gameType = (char)game.type;
					networking.response.newGame.levelId = (char)game.levelId;
					SendResponse(peer, CHAN_STATUS, 1+sizeof(struct ResponseNewGame_st));

					for(int anotherPlayerId = 0; anotherPlayerId < MAX_PLAYERS; anotherPlayerId++){
						if(anotherPlayerId == playerId) continue;
						if(game.players[anotherPlayerId].active){
							int length = PrepareResponseForPlayer(anotherPlayerId);
							SendResponse(peer, CHAN_STATUS, length);
						}
					}

					for(int characterId = 0; characterId < MAX_CHARACTERS; characterId++){
						if(game.characters[characterId].active){
							int length = PrepareResponseForCharacter(characterId);
							SendResponse(peer, CHAN_STATUS, length);
						}
					}

					for(int itemId = 0; itemId < MAX_ITEMS; itemId++){
						if(game.items[itemId].active){
							int length = PrepareResponseForItem(itemId);
							SendResponse(peer, CHAN_STATUS, length);
						}
					}

					SynchronizePlayer(playerId);

					char text[2048];
					sprintf(text, "> %s joined.", player->name);
					SendChatMessage(CHATMSG_GLOBAL, -1, text, 0);

				}
			} break;
		}
	}else{
		int playerId = (int)peer->data;
		Player_t* player = &game.players[playerId];
		switch(msg->code){
			case REQUEST_INPUTS: {
				UnpackControls(&player->controls, &msg->inputs.controls);
			} break;
			case REQUEST_CHATMSG: {
				PlayerSay(playerId, (const char*)msg->chat.text);
			} break;
		}
	}
}

static bool ProcessResponse(ResponseMessage_t* msg, int bytes){
	//printf("[RES] 0x%x\n", msg->code);
	switch(msg->code){
		case RESPONSE_AUTH: {
			if(msg->auth.succeded){
				if(app.screen == SCREEN_GAME) return true;
				StartGame(ROLE_REMOTE, GAME_PRACTICE, -1);
				app.localPlayerId = msg->auth.success.playerId;
			}else{
				app.screen = SCREEN_MAINMENU;
				app.mainMenuScreen = MAINMENU_LOADING;
				app.role = ROLE_LOCAL;
				strcpy(app.gameInterfaceState.loadingTextHeader, "Connection refused.");
				strncpy(app.gameInterfaceState.loadingTextSub, msg->auth.failure.errorMessage, sizeof(msg->auth.failure.errorMessage));
				app.gameInterfaceState.loadingFailed = true;
				app.gameInterfaceState.loadingAbortable = true;
				return true;
			}
		} break;
		case RESPONSE_NEWGAME: {
			game.type = msg->newGame.gameType;
			game.levelId = msg->newGame.levelId;
			//ResetGame();
			app.shouldRestartRound = 1;
			ResetNetworkingCache();

			for(int playerId = 0; playerId < MAX_PLAYERS; playerId++){
				Player_t* player = &game.players[playerId];
				if(!player->active) continue;
				player->characterId = -1;
			}

			//printf("got newgame\n");
			return true;
		} break;
		case RESPONSE_CHATMSG: {
			SendChatMessage(msg->chat.type, msg->chat.senderId, (const char*)&msg->chat.text, msg->chat.variation);
		} break;
		case RESPONSE_PLAYER: {
			Player_t* player = &game.players[msg->player.playerId];
			if(msg->player.active){
				player->active = 1;
				player->characterId = (msg->player.characterId == 0xFF) ? -1 : msg->player.characterId;
				strncpy((char*)player->name, (char*)&msg->player.name, sizeof(player->name));
				UnpackCustomizations(&player->customizations, &msg->player.customizations);
				UnpackPlayerStats(&player->stats, &msg->player.stats);
			}else{
				player->active = 0;
			}
			//printf("got player info. id: %i char: %i\n", msg->player.playerId, msg->player.characterId);
		} break;
		case RESPONSE_CHARACTER: {
			Character_t* character = &game.characters[msg->character.characterId];
			if(msg->character.active){
				int newPlayerId = (msg->character.playerId == 0xFF) ? -1 : msg->character.playerId;
				//if(character->playerId != newPlayerId){
					if(character->active && (character->playerId != -1)){
						Player_t* oldPlayer = &game.players[character->playerId];
						oldPlayer->characterId = -1;
					}
					if(newPlayerId != -1){
						Player_t* newPlayer = &game.players[newPlayerId];
						newPlayer->characterId = msg->character.characterId;
					}
					character->playerId = newPlayerId;
				//}
				character->active = 1;
				character->type = msg->character.type;
				character->health = msg->character.health;
				InitializeCharacterData(msg->character.characterId);
				UnpackCustomizations(&character->customizations, &msg->character.customizations);
				UnpackTform(&character->tform, &msg->character.tform);
				character->livingFrames = 1000; //todo
			}else{
				if(character->active && (character->playerId != -1)){
					Player_t* player = &game.players[character->playerId];
					if(player->characterId == msg->character.characterId){
						player->characterId = -1;
					}
				}
				character->active = 0;
			}
			//printf("got char info. id: %i . active = %i. player: %i\n", msg->character.characterId, msg->character.active, msg->character.playerId);
		} break;
		case RESPONSE_ITEM: {
			Item_t* item = &game.items[msg->item.itemId];
			if(msg->item.active){
				if(item->active){
					if(item->ownerCharacterId != -1){
						Character_t* owner = &game.characters[item->ownerCharacterId];
						for(int i=0; i < CHAR_MAX_INVENTORY_SLOTS; i++){
							if(owner->inventoryItemIds[i] == msg->item.itemId){
								owner->inventoryItemIds[i] = -1;
								break;
							}
						}
					}
				}
				item->active = 1;
				item->type = msg->item.type;
				InitializeItemData(msg->item.itemId);
				UnpackTform(&item->tform, &msg->item.tform);
				if(msg->item.ownerId == 0xFF){
					item->ownerCharacterId = -1;
				}else{
					if(msg->item.inventorySlot > CHAR_MAX_INVENTORY_SLOTS){
						//printf("bigger than max inv slot. owner = %i. slot = %i.\n", msg->item.ownerId, msg->item.inventorySlot);
						return false;
					}
					item->ownerCharacterId = msg->item.ownerId;
					Character_t* owner = &game.characters[item->ownerCharacterId];
					int existingItemId = owner->inventoryItemIds[msg->item.inventorySlot];
					if(existingItemId != -1){
						Item_t* existingItem = &game.items[existingItemId];
						existingItem->ownerCharacterId = -1;
					}
					owner->inventoryItemIds[msg->item.inventorySlot] = msg->item.itemId;
				}
				//item->ownerCharacterId = (msg->item.ownerId == 0xFF) ? -1 : msg->item.ownerId;
				//item->ownerCharacterId = -1;
			}else{
				item->active = 0;
			}
			//printf("got item info for %i. active = %i. owner = %i. slot = %i\n", msg->item.itemId, item->active, msg->item.ownerId, msg->item.inventorySlot);
		} break;
		case RESPONSE_PARTICLES: {
			Vector2 pos = (Vector2){msg->particles.pos[0], msg->particles.pos[1]};
			SpawnParticles(msg->particles.type, msg->particles.amount, pos);
		} break;
		case RESPONSE_PLAYER_STATS: {
			Player_t* player = &game.players[msg->playerStats.playerId];
			UnpackPlayerStats(&player->stats, &msg->playerStats.stats);
		} break;
		case RESPONSE_FRAME: {
			if( (msg->frame.frame < networking.lastReceivedFrame) && (networking.lastReceivedFrame != -1) ){
				//printf("frame out of order\n");
				return false;
			}
			networking.lastReceivedFrame = msg->frame.frame;
			game.frame = msg->frame.frame;
			//printf("frame packet: %i\n", game.frame);
			game.type = msg->frame.type;
			game.state = msg->frame.state;
			game.timer = msg->frame.timer;
			Player_t* localPlayer = &game.players[app.localPlayerId];
			if(localPlayer->characterId != -1){
				Character_t* localCharacter = &game.characters[localPlayer->characterId];
				localCharacter->lookingAtItemId = (msg->frame.yourCharacterLookingAtItemId == 0xFF) ? -1 : msg->frame.yourCharacterLookingAtItemId;
			}
			void* payload = &msg->frame._rest;
			FrameResponseCharacterInfo_t* characterInfo = payload;
			for(int i=0; i < msg->frame.characterCount; i++){
				Character_t* character = &game.characters[characterInfo->characterId];
				if(characterInfo->health != character->health){
					character->healthLastDamageFrame = game.frame;
					character->health = characterInfo->health;
				}
				character->inventorySecondarySelectionSlot = characterInfo->inventorySecondarySelectionSlot;
				UnpackTform(&character->tform, &characterInfo->tform);
				characterInfo++;
				//printf("unpacked char id: %i (%i, %p)\n", characterInfo->characterId, i, &characterInfo);
			}
			FrameResponseItemInfo_t* itemInfo = (FrameResponseItemInfo_t*)characterInfo;
			for(int i=0; i < msg->frame.itemCount; i++){
				Item_t* item = &game.items[itemInfo->itemId];
				UnpackTform(&item->tform, &itemInfo->tform);
				item->posOffset.x = itemInfo->posOffset[0];
				item->posOffset.y = itemInfo->posOffset[1];
				//printf("unpacked item %i (%i, %p)\n", itemInfo->itemId, i, &itemInfo);
				itemInfo++;
			}
			if(!msg->frame.projectileCount){
				game.projectileSystemState.projectileMaxActiveId = 0;
				game.projectileSystemState.projectileNextMinFreeId = 0;
				game.projectileSystemState.projectiles[0].type = PROJ_INACTIVE;
			}else{
				FrameResponseProjectileInfo_t* projectileInfo = (FrameResponseProjectileInfo_t*)itemInfo;
				for(int i=0; i < msg->frame.projectileCount; i++){
					int projectileId = msg->frame.projectileBaseId + i;
					if(!projectileInfo->type){
						RemoveProjectile(projectileId);
						projectileInfo = (FrameResponseProjectileInfo_t*)((char*)projectileInfo+1);
					}else{
						Projectile_t* projectile = &game.projectileSystemState.projectiles[projectileId];
						if(projectile->type == PROJ_INACTIVE){
							AddedProjectileAt(projectileId);
						}
						UnpackProjectile(projectile, projectileInfo);
						projectileInfo++;
					}
				}

			}
		} break;
	}
	return false;
}

static void BroadcastFrameData(){
	networking.lastSentFrame = game.frame;
	networking.response.code = RESPONSE_FRAME;
	networking.response.frame.frame = game.frame;
	networking.response.frame.type = game.type;
	networking.response.frame.state = game.state;
	networking.response.frame.timer = game.timer;
	int length = 1+13;
	networking.response.frame.characterCount = 0;
	networking.response.frame.itemCount = 0;
	void* payload = &networking.response.frame._rest;
	FrameResponseCharacterInfo_t* characterInfo = payload;
	for(int characterId = 0; characterId < MAX_CHARACTERS; characterId++){
		Character_t* character = &game.characters[characterId];
		if(!character->active) continue;
		//if(!HasChanged(&character->tform)) continue;
		int charInfoNum = networking.response.frame.characterCount++;
		characterInfo[charInfoNum].characterId = (char)characterId;
		characterInfo[charInfoNum].health = character->health;
		characterInfo[charInfoNum].inventorySecondarySelectionSlot = character->inventorySecondarySelectionSlot;
		PackTform(&characterInfo[charInfoNum].tform, &character->tform);
		length += sizeof(FrameResponseCharacterInfo_t);
	}
	FrameResponseItemInfo_t* itemInfo = (FrameResponseItemInfo_t*)&characterInfo[networking.response.frame.characterCount];
	for(int itemId = 0; itemId < MAX_ITEMS; itemId++){
		Item_t* item = &game.items[itemId];
		if(!item->active) continue;
		//if(!HasChanged(&item->tform)) continue;
		int itemInfoNum = networking.response.frame.itemCount++;
		itemInfo[itemInfoNum].itemId = (char)itemId;
		PackTform(&itemInfo[itemInfoNum].tform, &item->tform);
		itemInfo[itemInfoNum].posOffset[0] = item->posOffset.x;
		itemInfo[itemInfoNum].posOffset[1] = item->posOffset.y;
		length += sizeof(FrameResponseItemInfo_t);
		//printf("packed item %i (%i)\n", itemId, itemInfoNum);
	}

	if((!game.projectileSystemState.projectileNextMinFreeId) && (!game.projectileSystemState.projectileMaxActiveId)){
		networking.response.frame.projectileCount = 0;
	}else{
		FrameResponseProjectileInfo_t* projectileInfo = (FrameResponseProjectileInfo_t*)&itemInfo[networking.response.frame.itemCount];
		int baseId = networking.lastSentProjectileId;
		if(baseId >= game.projectileSystemState.projectileMaxActiveId) baseId = 0;
		networking.response.frame.projectileBaseId = baseId;
		networking.response.frame.projectileCount = 0;
		for(int projectileId = baseId; projectileId < (baseId + MAX_PROJECTILES_PER_PACKET); projectileId++){
			if(projectileId > game.projectileSystemState.projectileMaxActiveId) break;
			Projectile_t* projectile = &game.projectileSystemState.projectiles[projectileId];
			PackProjectile(projectileInfo, projectile);
			if(projectile->type == PROJ_INACTIVE){
				length += 1;
				projectileInfo = (FrameResponseProjectileInfo_t*)((char*)projectileInfo + 1);
			}else{
				length += sizeof(FrameResponseProjectileInfo_t);
				projectileInfo++;
			}
			networking.response.frame.projectileCount++;
			networking.lastSentProjectileId = projectileId;
		}
		//printf("sent projectiles %i-%i (%i)\n", baseId, networking.lastSentProjectileId, baseId+networking.response.frame.projectileCount-1);
	}

	for(int playerId = 0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = &game.players[playerId];
		if(player->active && player->session.connected){
			if(player->characterId != -1){
				Character_t* character = &game.characters[player->characterId];
				networking.response.frame.yourCharacterLookingAtItemId = character->lookingAtItemId;
			}else{
				networking.response.frame.yourCharacterLookingAtItemId = -1;
			}
			SendResponse(player->session.peer, CHAN_GAME, length);
		}
	}

}

void UpdateNetworking(){
	if(networking.host){

		if(app.role == ROLE_HOST){
			if(networking.lastSentFrame < game.frame){
				BroadcastFrameData();
			}
		}

		ENetEvent event;
		bool shouldStop = false;
		while((!shouldStop) && enet_host_service(networking.host, &event, 0)){
			bool timedOut = false;
			switch(event.type){
				case ENET_EVENT_TYPE_CONNECT: {
					if(app.role == ROLE_HOST){
						event.peer->data = NULL;
					}else if(app.role == ROLE_REMOTE){
						networking.request.code = REQUEST_AUTH;
						networking.request.auth.version = 2;
						strncpy((char*)&networking.request.auth.username, app.settings.playerName, sizeof(networking.request.auth.username));
						PackCustomizations(&networking.request.auth.customizations, &app.settings.customizations);
						SendRequest(CHAN_STATUS, 1+sizeof(struct RequestAuth_st));
					}
					//printf("[E] received connect\n");
				} break;
				case ENET_EVENT_TYPE_RECEIVE: {
					if(app.role == ROLE_HOST){
						ProcessRequest((RequestMessage_t*)event.packet->data,event.peer, event.packet->dataLength);
					}else if(app.role == ROLE_REMOTE){
						if(ProcessResponse((ResponseMessage_t*)event.packet->data, event.packet->dataLength)){
							shouldStop = true;
						}
					}
					enet_packet_destroy(event.packet);
					//printf("[E] received receive\n");
				} break;
				case ENET_EVENT_TYPE_DISCONNECT_TIMEOUT:
					timedOut = true;
				case ENET_EVENT_TYPE_DISCONNECT: {
					if(app.role == ROLE_HOST){
						if(event.peer->data){
							int playerId = (int)event.peer->data;
							Player_t* player = &game.players[playerId];
							char text[2048];
							if(!timedOut){
								sprintf(text, "> %s left.", player->name);
							}else{
								sprintf(text, "> %s timed out.", player->name);
							}
							SendChatMessage(CHATMSG_GLOBAL, -1, text, 0);
							if(player->characterId != -1){
								RemoveCharacter(player->characterId);
							}
							RemovePlayer(playerId);
						}
					}else if(app.role == ROLE_REMOTE){
						app.screen = SCREEN_MAINMENU;
						app.mainMenuScreen = MAINMENU_LOADING;
						app.role = ROLE_LOCAL;
						app.isPaused = 1;
						strcpy(app.gameInterfaceState.loadingTextHeader, "Disconnected.");
						if(timedOut){
							strcpy(app.gameInterfaceState.loadingTextSub, "Connection timed out.");
						}else{
							app.gameInterfaceState.loadingTextSub[0] = 0;
						}
						app.gameInterfaceState.loadingFailed = true;
						app.gameInterfaceState.loadingAbortable = true;
					}
					//printf("[E] received disconnect\n");
				} break;
				case ENET_EVENT_TYPE_NONE:
					break;
			}
		}
	}

	UpdateMasterServerConnection();
}

void GetIPv6(const char* ip, uint32_t* out){
	ENetAddress tmpAddr;
	enet_address_set_host(&tmpAddr, ip);
	memcpy(out, &tmpAddr.host, sizeof(tmpAddr.host));
}

void* GetHost(){
	return networking.host;
}

RequestMessage_t* GetRequestBuffer(){
	return &networking.request;
}

ResponseMessage_t* GetResponseBuffer(){
	return &networking.response;
}

void SendRequest(int channel, int bytes){
	bool reliable = (channel == CHAN_STATUS);
	ENetPacketFlag flag = reliable ? ENET_PACKET_FLAG_RELIABLE : 0;
	ENetPacket* packet = enet_packet_create((void*)&networking.request, bytes, flag);
	enet_peer_send(networking.peer, channel, packet);
	//enet_host_flush(networking.host);
}

void BroadcastResponse(int channel, int bytes){
	for(int playerId = 0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = &game.players[playerId];
		if(player->active && player->session.connected){
			SendResponse(player->session.peer, channel, bytes);
		}
	}
}


int PrepareResponseForPlayer(int playerId){
	Player_t* player = &game.players[playerId];
	networking.response.code = RESPONSE_PLAYER;
	networking.response.player.playerId = (char)playerId;
	int length;
	if(player->active){
		networking.response.player.active = 1;
		networking.response.player.characterId = (char)player->characterId;
		strncpy((char*)&networking.response.player.name, (char*)&player->name, sizeof(player->name));
		PackCustomizations(&networking.response.player.customizations, &player->customizations);
		PackPlayerStats(&networking.response.player.stats, &player->stats);
		length = 1+sizeof(struct ResponsePlayer_st);
	}else{
		networking.response.player.active = 0;
		length = 3;
	}
	return length;
}

int PrepareResponseForCharacter(int characterId){
	Character_t* character = &game.characters[characterId];
	networking.response.code = RESPONSE_CHARACTER;
	networking.response.character.characterId = (char)characterId;
	int length;
	if(character->active){
		networking.response.character.active = 1;
		networking.response.character.playerId = (char)character->playerId;
		networking.response.character.type = character->type;
		networking.response.character.health = character->health;
		PackCustomizations(&networking.response.character.customizations, &character->customizations);
		PackTform(&networking.response.character.tform, &character->tform);
		length = 1+sizeof(struct ResponseCharacter_st);
	}else{
		networking.response.character.active = 0;
		length = 3;
	}
	return length;
}

int PrepareResponseForItem(int itemId){
	Item_t* item = &game.items[itemId];
	networking.response.code = RESPONSE_ITEM;
	networking.response.item.itemId = (char)itemId;
	int length;
	if(item->active){
		networking.response.item.active = 1;
		networking.response.item.type = item->type;
		networking.response.item.ownerId = item->ownerCharacterId;
		if(item->ownerCharacterId != -1){
			networking.response.item.inventorySlot = FindSlot(item->ownerCharacterId, itemId);
		}
		length = 1+sizeof(struct ResponseItem_st);
	}else{
		networking.response.item.active = 0;
		length = 3;
	}
	return length;
}
