#pragma once
#include "stdbool.h"

#define CHAT_MESSAGE_LEN 64
#define CHAT_HISTORY_MAX 64
#define CHAT_SHOWING_MAX 10
#define CHAT_SHOWING_DURATION 6*60
#define CHAT_STARTS_FADING_AT 5*60

typedef enum{
	CHATMSG_GLOBAL,
	CHATMSG_SPEC_PLAYER
} ChatMessageType_e;

typedef struct{
	ChatMessageType_e type;
	int sentFrame;
	int senderId;
	int variation;
	char text[CHAT_MESSAGE_LEN];
} ChatMessage_t;

typedef struct{
	bool loadingAbortable;
	bool loadingFailed;
	char loadingTextHeader[1024];
	char loadingTextSub[1024];

	char hintText[48];
	int hintTimer;
	int hintFrames;

	ChatMessage_t chatHistory[CHAT_HISTORY_MAX];
	int chatMessageNext;
	int chatMessagesTotal;
	int chatMessagesShowing;

	bool chatBoxEnabled;
} GameInterfaceState_t;

void UpdateViewBounds();
void DrawHUD();
void Scenario_Regular_UI();
void Scenario_Deathmatch_UI();
void PlayerSay(int playerId, const char* text);
void SendChatMessage(ChatMessageType_e type, int senderId, const char* text, int variation);
