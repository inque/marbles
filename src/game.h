#pragma once

#include "raylib.h"
#include "chipmunk/chipmunk.h"
#include "chipmunk/chipmunk_private.h"

#include "level.h"
#include "players.h"
#include "characters.h"
#include "particles.h"
#include "items.h"
#include "physics.h"
#include "gameinterface.h"
#include "projectiles.h"
#include "assets.h"

//#define GAME_VERSION_MAJOR 0
//#define GAME_VERSION_MINOR 1
//#define GAME_VERSION_ID (GAME_VERSION_MAJOR*1000 + GAME_VERSION_MINOR)

#define GAME_MAX_TICK_RATE 60
//#define GAME_MAX_TICK_RATE 20

#define MAX_PLAYERS 64
#define MAX_CHARACTER_TYPES 8
#define MAX_CHARACTERS 64
#define MAX_ITEM_TYPES 16
#define MAX_ITEMS 512

typedef enum{
	ROLE_LOCAL,
	ROLE_HOST,
	ROLE_REMOTE
} AppRole_e;

typedef enum{
	KB_GO_LEFT,
	KB_GO_RIGHT,
	KB_TURN_LEFT,
	KB_TURN_RIGHT,
	KB_ACTION_PRIMARY,
	KB_ACTION_SECONDARY,
	KB_RELOAD,
	KB_SWAP,
	KB_DROP_PRIMARY,
	KB_DROP_SECONDARY,
	KB_CYCLE,
	KB_SELECT_1,
	KB_SELECT_2,
	KB_SELECT_3,
	KB_SELECT_4
} AppPlayerKeybind_e;

typedef struct{
	int useGamepad;
	int gamepadNum;
	char gamepadName[32];
	int kb[16];
} AppPlayerKeybinds_t;

typedef struct{
	int fullScreen;
	int windowPos[2];
	int windowSize[2];
	int windowMaximized;
	int frameSmoothing;
	char playerName[32];
	Customizations_t customizations;
	int hostPort;
	int hostGameType;
	char hostTitle[64];
	AppPlayerKeybinds_t keybinds[2];
} AppSettings_t;

typedef enum{
	SCREEN_MAINMENU,
	SCREEN_GAME,
	SCREEN_EDITOR
} AppScreen_e;

typedef enum{
	MAINMENU_HOME,
	MAINMENU_OPTIONS,
	MAINMENU_SERVERLIST,
	MAINMENU_HOST,
	MAINMENU_LOADING,
	MAINMENU_CREDITS
} MainMenuScreen_e;

typedef enum{
	SUBMENU_NONE,
	SUBMENU_ESC
} InGameSubMenu_e;

typedef struct{
	double refreshRate;
	double lastStarted;
	double lastEnded;
	double timeTaken;
	double elapsed;
	int shouldUpdate;
} ExecutionJob_t;

typedef struct{
	AppSettings_t settings;
	AppAssets_t assets;
	AppScreen_e screen;
	MainMenuScreen_e mainMenuScreen;
	InGameSubMenu_e inGameSubMenu;
	ExecutionJob_t gameplayJob, graphicsJob;
	AppRole_e role;
	int isPaused;
	int shouldRestartRound;
	int frameSmoothing;
	int width, height;
	int halfWidth, halfHeight;
	Camera2D cam;
	Rectangle viewBounds;
	int uiFrame;
	float deltaTimeUI;
	float deltaTimeGame;
	float fineUIFrame;
	float fineGameFrame;
	int localPlayerId;
	int backgroundTransitionOut;
	GameInterfaceState_t gameInterfaceState;
	//NetworkingState_t networking;
	//MasterServerState_t master;
	int debugMode;
	struct{
		int levelPolysDrawn;
	} debugInfo;
	int shouldClose;
} AppState_t;

typedef enum{
	GAME_PRACTICE,
	GAME_REGULAR,
	GAME_DEATHMATCH
} GameType_e;

typedef enum{
	STATE_UNINITIALIZED,
	STATE_STARTING,
	STATE_PLAYING,
	STATE_ENDED
} GameState_e;

typedef struct{
	int initialized;
	GameType_e type;
	GameState_e state;
	int frame;
	int timer;
	int levelId;
	Level_t level;
	int playerCount;
	Player_t players[MAX_PLAYERS];
	CharacterType_t characterTypes[MAX_CHARACTER_TYPES];
	Character_t characters[MAX_CHARACTERS];
	ItemType_t itemTypes[MAX_ITEM_TYPES];
	Item_t items[MAX_ITEMS];
	cpSpace space;
	PhysicsState_t physicsState;
	ParticleSystemState_t particleSystemState;
	ProjectileSystemState_t projectileSystemState;
} GameState_t;

extern AppState_t app;
extern GameState_t game;

void LoadSettings();
void SaveSettings();

void StartGame(AppRole_e role, GameType_e type, int levelId);
void ResetGame();
void CloseGame();

void GameLoop();
void StartGameLoop();
void UpdatePlayerControls();
void UpdateGameLogic();
void UpdateGame();
void UpdateAppState();
void DrawGameScene();
void DrawGraphics();

//utils

#define randf() ((float)rand() / RAND_MAX)
int mod2(int a, int b);