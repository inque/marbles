#include "physics.h"
#include "game.h"

void InitializePhysics(){
	cpCollisionHandler* handler;

	cpSpaceInit(&game.space);
	cpSpaceSetGravity(&game.space, cpv(0, 9.0));
	//cpSpaceSetDamping(&game.space, 0.9);

	//game.physicsState.filterLevel = cpShapeFilterNew(0, (1 << GROUP_LEVEL), GROUP_ALL);

	handler = cpSpaceAddCollisionHandler(&game.space, COLLISION_TYPE_CHARACTER, COLLISION_TYPE_ITEM);
	handler->preSolveFunc = CallbackPresolveCharacter;
	handler->postSolveFunc = CallbackPostsolveCharacter;

	handler = cpSpaceAddCollisionHandler(&game.space, COLLISION_TYPE_ITEM, COLLISION_TYPE_ITEM);
	handler->preSolveFunc = CallbackPresolveItem;

}

void UpdatePhysics(){
	for(int i=0; i < PHYSICS_STEP_COUNT; i++) {
		cpSpaceStep(&game.space, PHYSICS_STEP_DELTA);
	}
}
