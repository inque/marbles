#pragma once
#include "raymath.h"
#include "stdbool.h"

void Wait(float ms);

bool PointInRect(Rectangle bounds, Vector2 point);
float DistToSegment(Vector2 p, Vector2 v, Vector2 w);
