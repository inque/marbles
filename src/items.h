#pragma once

#include "transforms.h"
#include "raylib.h"
#include "chipmunk/chipmunk.h"
#include "physics.h"

#define DROPPED_ITEM_DESPAWN_TIME 3600

#define ITEM_TYPE_MAX_VERTICES 32
#define ITEM_MAX_CONSTRAINTS 2
#define ITEM_MAX_ACTIONS 2

typedef enum{
	ITEM_JUMPY,
	ITEM_KEYCARD,
	ITEM_BLASTER,
	ITEM_ROCK
} ItemType_e;

typedef struct{
	char name[32];
	float weight;
	float moment;
	int aimable;
	int throwDamage;
	float throwDamageThreshold;
	int hasPhysics;
	int collisionVertCount;
	cpVect collisionVerts[ITEM_TYPE_MAX_VERTICES];
	int outlineVertCount;
	Vector2 outlineVerts[ITEM_TYPE_MAX_VERTICES];
	Vector2 approximateSize;
	Color strokeColor;
} ItemType_t;

typedef enum{
	ITEMFLAG_RESPAWNABLE = 1
} ItemFlags_e;

typedef struct{
	int active;
	ItemType_e type;
	int ownerCharacterId;
	int despawnTimer;
	int lastOwnerId;
	int lastChangedOwnerFrame;
	cpBody* body;
	cpShape* shape;
	cpConstraint* constraints[ITEM_MAX_CONSTRAINTS];
	ShapeUserData_t shapeUserData;
	Transform_t tform;
	float targetAngle;
	Vector2 posOffset;
	Vector2 posOffsetLast;
	int actionExecuted[ITEM_MAX_ACTIONS];
	int actionLastFrame[ITEM_MAX_ACTIONS];
	uint32_t flags;
	Vector2 respawnPos;
} Item_t;

void DefineItems();
void InitializeItemData(int itemId);
int SpawnItem(ItemType_e type, Vector2 pos);
void SynchronizeItem(int itemId);
void RemoveItem(int itemId);
void SetItemOwner(int itemId, int characterId);
void ItemAction(int itemId, int actionId);
cpBool CallbackPresolveItem(cpArbiter *arb, cpSpace *space, void *ignore);
void UpdateItems();
void DrawItemIcon(ItemType_e type, Vector2 pos, float angle, float scale);
void DrawItem(int itemId);
void DrawItems();