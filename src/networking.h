#pragma once
#include "masterserver.h"
//#include "networkingpackets.h"
#include "stdint.h"
#include "stdbool.h"

#define NET_MAGIC_ID 1279414861 //"MRBL"

enum{
	CHAN_STATUS,
	CHAN_GAME,
	CHAN_GAME_LOW,
	CHAN__COUNT
};

typedef struct{
	int connected;
	//int masterAccountId;
	int localAccountId;
	void* peer;
} PlayerSession_t;

typedef struct RequestMessage_st RequestMessage_t;
typedef struct ResponseMessage_st ResponseMessage_t;

void InitNetworking();
bool NetworkHostCreate();
bool NetworkHostJoin(uint32_t* ip, uint16_t port);
void CleanUpNetworking();
void ResetNetworkingCache();
void UpdateNetworking();
void GetIPv6(const char* ip, uint32_t* out);
void* GetHost();
RequestMessage_t* GetRequestBuffer();
ResponseMessage_t* GetResponseBuffer();

int PrepareResponseForPlayer(int playerId);
int PrepareResponseForCharacter(int characterId);
int PrepareResponseForItem(int itemId);

void SendRequest(int channel, int bytes);
void BroadcastResponse(int channel, int bytes);
