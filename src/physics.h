#pragma once

#include "chipmunk/chipmunk.h"

#define PHYSICS_STEP_COUNT 1
#define PHYSICS_STEP_DELTA 0.1

enum{
	COLLISION_TYPE_LEVEL = 1,
	COLLISION_TYPE_CHARACTER = 2,
	COLLISION_TYPE_ITEM = 3
};

//enum{
//	GROUP_LEVEL,
//	GROUP_CHARACTERS,
//	GROUP_ITEMS
//};

typedef enum{
	SHAPE_LEVEL_POLY,
	SHAPE_CHARACTER,
	SHAPE_ITEM
} ShapeOwnerType_e;

typedef struct{
	ShapeOwnerType_e type;
	int id;
} ShapeUserData_t;

typedef struct{
	//cpShapeFilter filterLevel, filterCharacter, filterItem;
	int unused;
} PhysicsState_t;

void InitializePhysics();
void UpdatePhysics();
