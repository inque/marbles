#pragma once
#include "raylib.h"
#include "transforms.h"

#define FIGURE_MAX_FRAMES 16

typedef struct{
	Vector2 center;
	int triCount;
	int indices[];
} FigureFrame;

typedef struct{
	int frameCount;
	const FigureFrame* frames[FIGURE_MAX_FRAMES];
	Vector2 verts[];
} Figure;

void DrawFigure(const Figure* figure, int frameId, Vector2 pos);
void DrawFigureEx(const Figure* figure, int frameId, Vector2 pos, float angle, Color tint);
void DrawFigureCustom(const Figure* figure, int frameId, Vector2 pos, float angle, Color tint);

void DrawPolyObjectStroke(int vertCount, Vector2* verts, Color strokeColor);
void DrawPolyObjectFill(int triangleCount, const int* triangleIndices, Vector2* verts, Color fillColor);
void BeginTransformManual(Vector2 pos, float angle, float scale);
void BeginTransform(Transform_t* tform);
void EndTransform();
