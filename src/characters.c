#include "characters.h"
#include "game.h"
#include "gameplay.h"
#include "graphics.h"
#include "networking.h"
#include "networkingpackets.h"
#include "raylib.h"
#include "stdio.h"
#include "string.h"

static void MakeSkinColorStrokeJustDarkerFill(CharacterType_t* characterType){
	for(int i = 0; i < CUSTOM_SKIN_VARIATIONS; i++){
		characterType->skinColorStroke[i] = characterType->skinColorFill[i];
		characterType->skinColorStroke[i].r -= 20;
		characterType->skinColorStroke[i].g -= 20;
		characterType->skinColorStroke[i].b -= 20;
	}
}

void DefineCharacters(){
	
	game.characterTypes[CHAR_BALL] = (CharacterType_t){
		.name = "ball",
		.angleMultiplier = 1.0f / 90,
		.itemReachDistance = 90,
		.skinColorFill = {
			{193, 148, 134, 255},
			{153, 145, 160, 255},
			{167, 190, 144, 255},
			{250, 223, 133, 255},
			{214, 123, 114, 255},
			{201, 160, 190, 255},
			{119, 108, 104, 255},
			{236, 165, 121, 255}
		}
	};
	MakeSkinColorStrokeJustDarkerFill(&game.characterTypes[CHAR_BALL]);

	game.characterTypes[CHAR_CUBE] = (CharacterType_t){
		.name = "cube",
		.angleMultiplier = 1,
		.itemReachDistance = 80
	};

}

void InitializeCharacterData(int characterId){
	Character_t* character = &game.characters[characterId];

	character->healthLastDamageFrame = -1;
	character->healthLastDamageByPlayerFrame = -1;
	character->healthLastDamageSource = DEATH_BY_UNKNOWN;
	character->healthAnimatedValue = character->health;
	character->livingFrames = 0;
	character->lookingAtItemId = -1;
	character->primaryItemAngle = 0.0f;
	character->externalTorque = 0.0f;
	for(int i=0; i < CHAR_MAX_INVENTORY_SLOTS; i++){
		character->inventoryItemIds[i] = -1;
	}
	character->inventorySecondarySelectionSlot = 1;
	switch(character->type){
	case CHAR_BALL: {
		float radius = 30.0f;
		character->ballProps.radius = radius;
		character->ballProps.speedMultiplier = 100.0f;

		character->ballProps.motorSpeed = 0.0f;
		character->ballProps.motorAccel = 0.0f;
		character->ballProps.motorGear = 0;
		character->ballProps.motorCursor = 0.0f;

		character->tform.size = (Vector2){radius*2, radius*2};
	} break;
	case CHAR_CUBE:{
		float boxSize = 50.0f;
		character->cubeProps.eyeAnimationFrame = rand() & 1023;
		character->tform.size = (Vector2){boxSize*1.5f, boxSize*1.5f};
	} break;
	default:
		break;
	}
}

int SpawnCharacter(int playerId, CharacterType_e type, Vector2 pos){
	for(int characterId = 0; characterId < MAX_CHARACTERS; characterId++){
		Character_t* character = &game.characters[characterId];
		if(character->active) continue;

		character->active = 1;
		character->playerId = playerId;
		character->type = type;
		character->health = 100;
		character->tform = NewTransform(pos, 0);
		InitializeCharacterData(characterId);
		switch(character->type){
			case CHAR_BALL: {
				float mass = character->ballProps.radius * 0.1f;
				float moment = (float)cpMomentForCircle((cpFloat)mass, 0, (cpFloat)character->ballProps.radius, cpvzero);

				character->bodies[0] = cpSpaceAddBody(&game.space, cpBodyNew(mass, moment));
				character->shapes[0] = cpSpaceAddShape(&game.space, cpCircleShapeNew(character->bodies[0], character->ballProps.radius, cpvzero));
				cpShapeSetFriction(character->shapes[0], (cpFloat)0.85);
				cpShapeSetElasticity(character->shapes[0], (cpFloat)0.2);

				character->constraints[0] = cpSpaceAddConstraint(&game.space, cpSimpleMotorNew(character->bodies[0], cpSpaceGetStaticBody(&game.space), 0.0));
				//cpConstraintSetMaxForce(character->constraints[0], 0.5);

			} break;
			case CHAR_CUBE:{
				float boxSize = 50.0f;
				float mass = boxSize * 0.1f;
				float moment = cpMomentForBox((cpFloat)mass, boxSize, boxSize);

				character->cubeProps.size = boxSize;
				character->bodies[0] = cpSpaceAddBody(&game.space, cpBodyNew(mass, moment));
				character->shapes[0] = cpSpaceAddShape(&game.space, cpBoxShapeNew(character->bodies[0], boxSize, boxSize, 1.0f));
			} break;
			default:
				break;
		}

		if(character->bodies[0]){
			cpBodySetPosition(character->bodies[0], toCpVect(pos));
		}

		character->shapeUserData.type = SHAPE_CHARACTER;
		character->shapeUserData.id = characterId;
		for(int i = 0; i < CHAR_MAX_SHAPES; i++){
			if(character->shapes[i] != NULL){
				cpShapeSetCollisionType(character->shapes[i], COLLISION_TYPE_CHARACTER);
				cpShapeSetUserData(character->shapes[i], &character->shapeUserData);
			}
		}

		SpawnParticles(EFFECT_SPAWN_POOF, 6, pos);

		if(playerId != -1){
			Player_t* player = &game.players[playerId];
			character->customizations = player->customizations;
		}else{
			RandomizeCustomizations(&character->customizations);
		}

		SynchronizeCharacter(characterId);
		return characterId;
	}
	return -1;
}

void SynchronizeCharacter(int characterId){
	if(app.role != ROLE_HOST) return;
	int length = PrepareResponseForCharacter(characterId);
	BroadcastResponse(CHAN_STATUS, length);
}

void RemoveCharacter(int characterId) {
	Character_t* character = &game.characters[characterId];
	if (!character->active) return;

	if(character->playerId != -1){
		Player_t* player = &game.players[character->playerId];
		player->characterId = -1;
	}

	for(int i = 0; i < CHAR_MAX_INVENTORY_SLOTS; i++){
		if(character->inventoryItemIds[i] != -1){
			SetItemOwner(character->inventoryItemIds[i], -1);
		}
	}

	for(int bodyId=0; bodyId < CHAR_MAX_BODIES; bodyId++)
		if(character->bodies[bodyId])
			cpSpaceRemoveBody(&game.space, character->bodies[bodyId]);

	for(int shapeId=0; shapeId < CHAR_MAX_SHAPES; shapeId++)
		if(character->shapes[shapeId])
			cpSpaceRemoveShape(&game.space, character->shapes[shapeId]);

	for(int constraintId=0; constraintId < CHAR_MAX_CONSTRAINTS; constraintId++)
		if (character->constraints[constraintId]) {
			cpSpaceRemoveConstraint(&game.space, character->constraints[constraintId]);
			character->constraints[constraintId] = NULL;
		}

	character->active = 0;
	SynchronizeCharacter(characterId);
}

int GiveItem(int characterId, ItemType_e type){
	Character_t* character = &game.characters[characterId];
	int itemId = SpawnItem(type, GetPos(&character->tform));
	if(itemId == -1) return -1;
	for(int i=0; i < CHAR_MAX_INVENTORY_SLOTS; i++){
		if(character->inventoryItemIds[i] == -1){
			character->inventoryItemIds[i] = itemId;
			SetItemOwner(itemId, characterId);
			break;
		}
	}
	return itemId;
}

int FindSlot(int characterId, int itemId){
	Character_t* character = &game.characters[characterId];
	for(int i=0; i < CHAR_MAX_INVENTORY_SLOTS; i++){
		if(character->inventoryItemIds[i] == itemId){
			return i;
		}
	}
	return -1;
	//return 0;
}

void ApplyDamage(int characterId, int amount, Vector2 pos, Vector2 normal){
	Character_t* character = &game.characters[characterId];
	if(character->health > 0){
		character->healthLastDamageFrame = game.frame;
	}
	character->health -= amount;
	SpawnParticles(EFFECT_BLOOD_SPLASH, 1 + amount / 5, pos);
}

cpBool CallbackPresolveCharacter(cpArbiter *arb, cpSpace *space, void *ignore){
	CP_ARBITER_GET_SHAPES(arb, a, b);
	ShapeUserData_t* ourData = (ShapeUserData_t*)cpShapeGetUserData(a);
	ShapeUserData_t* otherData = (ShapeUserData_t*)cpShapeGetUserData(b);
	Character_t* character = &game.characters[ourData->id];
	if(otherData == NULL) return cpTrue;
	if(otherData->type == SHAPE_ITEM){
		Item_t* item = &game.items[otherData->id];
		if(item->ownerCharacterId == ourData->id) return cpArbiterIgnore(arb);
		if((item->lastOwnerId == ourData->id) && ((game.frame - item->lastChangedOwnerFrame) < 30)){
			return cpArbiterIgnore(arb);
		}
		/*
		if(item->ownerCharacterId == -1){
			character->lookingAtItemId = otherData->id;
			character->lookingAtItemTimer = 20;
		}
		*/
	}
	return cpTrue;
}

void CallbackPostsolveCharacter(cpArbiter *arb, cpSpace *space, void *ignore){
	CP_ARBITER_GET_SHAPES(arb, a, b);
	ShapeUserData_t* ourData = (ShapeUserData_t*)cpShapeGetUserData(a);
	ShapeUserData_t* otherData = (ShapeUserData_t*)cpShapeGetUserData(b);
	Character_t* character = &game.characters[ourData->id];
	if(otherData == NULL) return;
	if(otherData->type == SHAPE_ITEM) {
		Item_t* item = &game.items[otherData->id];
		ItemType_t* itemType = &game.itemTypes[item->type];

		if(itemType->throwDamage){
			cpFloat itemSpeed = cpvlength(cpBodyGetVelocity(item->body));
			cpFloat collisionImpulse = cpvlength(cpArbiterTotalImpulse(arb));
			if( (itemSpeed > itemType->throwDamageThreshold) && (collisionImpulse > itemType->throwDamageThreshold) ){
				ApplyDamage(ourData->id, itemType->throwDamage, toVector2(cpArbiterGetPointA(arb, 0)), toVector2(cpArbiterGetNormal(arb)));
				character->healthLastDamageSource = DEATH_BY_ITEM;
				character->healthLastDamageItemType = item->type;
				if(item->lastOwnerId != -1){
					character->healthLastDamagePlayerId = item->lastOwnerId;
					character->healthLastDamageByPlayerFrame = game.frame;
				}
			}
		}
	}
}

static int CharacterKeyHit(int charactedId, PlayerButtons_e button){
	Character_t* character = &game.characters[charactedId];
	return ( (character->controls.buttons & button)
		 && !(character->controls.buttonsLast & button) );
}

static int FindItemsWithinCharactersReach(Character_t* character, bool cycle){
	CharacterType_t* characterType = &game.characterTypes[character->type];
	Vector2 pos = GetPos(&character->tform);

	for(int itemId = 0; itemId < MAX_ITEMS; itemId++){
		Item_t* item = &game.items[itemId];
		if(!item->active) continue;
		if(item->ownerCharacterId != -1) continue;
		if(cycle && (character->lookingAtItemId == itemId)) continue;

		float dist = Vector2Distance(pos, GetPos(&item->tform));
		if(dist < characterType->itemReachDistance){
			return itemId;
		}
	}
	return -1;
}

void UpdateCharacters(){
	for(int characterId = 0; characterId < MAX_CHARACTERS; characterId++) {
		Character_t *character = &game.characters[characterId];
		if (!character->active) continue;
		CharacterType_t* characterType = &game.characterTypes[character->type];

		character->livingFrames++;
		if(character->health <= 0) {
			character->health = 0;
			for(int i = 0; i < CHAR_MAX_INVENTORY_SLOTS; i++){
				int itemId = character->inventoryItemIds[i];
				if(itemId != -1){
					Item_t* item = &game.items[itemId];
					if(item->body) cpBodyApplyForceAtWorldPoint(item->body, cpv(0, -10), cpBodyGetPosition(item->body));
					SetItemOwner(itemId, -1);
					character->inventoryItemIds[i] = -1;
				}
			}
			if(character->playerId != -1){
				OnCharacterDeath(characterId);
				character->playerId = -1;
				SynchronizeCharacter(characterId);
			}
			if(!character->despawnTimer){
				RemoveCharacter(characterId);
				continue;
			}else{
				character->despawnTimer--;
			}
		}else{
			character->despawnTimer = CHARACTER_CORPSE_DESPAWN_TIME;
		}

		if(character->bodies[0] != NULL){
			UpdateTransform(&character->tform, character->bodies[0]);
		}

		if(character->lookingAtItemId == -1){
			character->lookingAtItemId = FindItemsWithinCharactersReach(character, false);
		}else{
			if(character->health){
				if(CharacterKeyHit(characterId, BUTTON_CYCLE)){
					character->lookingAtItemId = FindItemsWithinCharactersReach(character, true);
				}else{
					Item_t* closestItem = &game.items[character->lookingAtItemId];
					float dist = Vector2Distance(GetPos(&character->tform), GetPos(&closestItem->tform));
					if(dist > characterType->itemReachDistance){
						character->lookingAtItemId = -1;
					}
				}
			}else{
				character->lookingAtItemId = -1;
			}
		}

		/*
		character->primaryItemAngleAcceleration += character->controls.axis_r * 0.023f;
		character->primaryItemAngleAcceleration *= 0.87f;
		character->primaryItemAngle += character->primaryItemAngleAcceleration;
		*/
		/*
		character->primaryItemAngleAcceleration += character->controls.axis_r * 0.015f;
		character->primaryItemAngleAcceleration *= 0.92f;
		*/
		character->primaryItemAngleAcceleration += character->controls.axis_r * 0.0375f;
		character->primaryItemAngleAcceleration *= 0.9635f;
		character->primaryItemAngle += character->controls.axis_r * 0.09f;

		if(character->inventoryItemIds[0] != -1){
			Item_t* item = &game.items[character->inventoryItemIds[0]];
			item->targetAngle = character->primaryItemAngle;
			if(character->controls.buttons & BUTTON_PRIMARY){
				ItemAction(character->inventoryItemIds[0], 0);
			}
		}
		if(character->inventoryItemIds[character->inventorySecondarySelectionSlot] != -1){
			Item_t* item = &game.items[character->inventoryItemIds[character->inventorySecondarySelectionSlot]];
			item->targetAngle = GetAngle(&character->tform) * characterType->angleMultiplier + character->inventorySecondarySelectionSlot * 0.785f;
			if(character->controls.buttons & BUTTON_SECONDARY){
				ItemAction(character->inventoryItemIds[character->inventorySecondarySelectionSlot], 0);
			}
		}

		for(int inputActionId = 0; inputActionId < PLAYER_MAX_INPUT_ACTIONS; inputActionId++){
			PlayerAction_t* inputAction = &character->controls.inputActionQueue[inputActionId];
			if(inputAction->type == ACTION_NONE) break;
			CharacterAction_t* characterAction = &character->actionQueue[character->actionNext];
			if(characterAction->input.type != ACTION_NONE) break;
			characterAction->input = *inputAction;
			switch(characterAction->input.type){
				case ACTION_PICKUP:
					characterAction->postTimer = 20;
					characterAction->preTimer = 10;
					break;
				case ACTION_THROW:
					characterAction->postTimer = 10;
					characterAction->preTimer = 1;
					break;
				case ACTION_SWAP:
					characterAction->postTimer = 5;
					characterAction->preTimer = 1;
					break;
				default:
					characterAction->postTimer = 1;
					characterAction->preTimer = 1;
					break;
			}
			inputAction->type = ACTION_NONE;
			character->actionNext = (character->actionNext + 1) % CHAR_MAX_ACTIONS_QUEUED;
		}

		CharacterAction_t* currentAction = &character->actionQueue[character->actionCurrent];
		if(currentAction->input.type != ACTION_NONE){
			if(currentAction->preTimer){
				currentAction->preTimer--;
				if(!currentAction->preTimer){
					switch(currentAction->input.type){
						case ACTION_THROW: {
							int slotId = (currentAction->input.parameter) ? character->inventorySecondarySelectionSlot : 0;
							SetItemOwner(character->inventoryItemIds[slotId], -1);
							character->inventoryItemIds[slotId] = -1;
						} break;
						case ACTION_PICKUP: {
							int slotId = currentAction->input.parameter;
							if((slotId < 0) || (slotId >= CHAR_MAX_INVENTORY_SLOTS)) break;
							if(character->inventoryItemIds[slotId] != -1) break;
							if(character->lookingAtItemId == -1) break;
							character->inventoryItemIds[slotId] = character->lookingAtItemId;
							SetItemOwner(character->lookingAtItemId, characterId);
							character->lookingAtItemId = -1;
						} break;
						case ACTION_SELECT_SECONDARY: {
							if(currentAction->input.parameter < 1) break;
							if(currentAction->input.parameter >= CHAR_MAX_INVENTORY_SLOTS) break;
							character->inventorySecondarySelectionSlot = currentAction->input.parameter;
						} break;
						case ACTION_SWAP: {
							int primaryId = character->inventoryItemIds[0];
							character->inventoryItemIds[0] = character->inventoryItemIds[character->inventorySecondarySelectionSlot];
							character->inventoryItemIds[character->inventorySecondarySelectionSlot] = primaryId;
							if(character->inventoryItemIds[0] != -1) SynchronizeItem(character->inventoryItemIds[0]);
							if(primaryId != -1)	SynchronizeItem(primaryId);
						} break;
					}
				}
			}else{
				currentAction->postTimer--;
				if(!currentAction->postTimer){
					currentAction->input.type = ACTION_NONE;
					character->actionCurrent = (character->actionCurrent + 1) % CHAR_MAX_ACTIONS_QUEUED;
				}
			}
		}

		switch(character->type){
			case CHAR_BALL: {
				cpConstraint* motor = character->constraints[0];
				if(character->health){
					int gearMin = -5, gearMax = 5;

					if(character->ballProps.motorGear < gearMin) character->ballProps.motorGear = gearMin;
					if(character->ballProps.motorGear > gearMax) character->ballProps.motorGear = gearMax;

					character->ballProps.motorAccel = character->controls.axis_h * 0.5f;
					character->ballProps.motorAccel *= 0.7f;
					character->ballProps.motorSpeed += character->ballProps.motorAccel * 0.5f;
					character->ballProps.motorSpeed *= 0.7f;

					float motorRate = character->ballProps.speedMultiplier * character->controls.axis_h;
							motorRate += character->externalTorque;
							//printf("motor rate: %f\n", motorRate);
					cpSimpleMotorSetRate(motor, motorRate);
					//float motorRate = character->ballProps.motorSpeed * character->ballProps.speedMultiplier;
					//cpSimpleMotorSetRate(motor, motorRate);
					//cpBodySetTorque(character->bodies[0], character->torque + motorRate * 10);

					cpConstraintSetMaxForce(motor, (cpFloat)10000000.0);
				}else{
					cpConstraintSetMaxForce(motor, (cpFloat)0.0);
				}
			} break;
			case CHAR_CUBE: {
				if(character->health){
					cpBodySetTorque(character->bodies[0], character->controls.axis_h * 600.0f);
					cpBodyApplyForceAtWorldPoint(character->bodies[0], cpv(character->controls.axis_h * 23, 0), cpBodyGetPosition(character->bodies[0]));
				}
			} break;
			default:
				break;
		}

		if(character->externalTorque > 0.01f){
			character->externalTorque *= 0.6f;
		}else{
			character->externalTorque = 0.0f;
		}
	}
}

void DrawCharacters(){
	for(int characterId = 0; characterId < MAX_CHARACTERS; characterId++){
		Character_t *character = &game.characters[characterId];
		if(!character->active) continue;
		if(!IsInView(&character->tform)) continue;

		Vector2 pos = GetInterpolatedPos(&character->tform);
		float angle = GetInterpolatedAngle(&character->tform);
		CharacterType_t* characterType = &game.characterTypes[character->type];
		int charAngleDeg = (int)(angle * RAD2DEG * characterType->angleMultiplier);

		if(character->inventoryItemIds[character->inventorySecondarySelectionSlot] != -1){
			DrawItem(character->inventoryItemIds[character->inventorySecondarySelectionSlot]);
		}

		switch(character->type){
			case CHAR_BALL: {
				float radius = character->ballProps.radius;
				unsigned char opacity = 255;
				int spawnAnimationDuration = 20;
				if(character->livingFrames < spawnAnimationDuration){
					float t = (float)character->livingFrames / (float)spawnAnimationDuration;
					radius *= (0.5f+0.5f*(t*(2-t)));
					opacity = (char)( (0.5+0.5*(t*(2-t))) *255);
				}
				//main body
				Color fill = characterType->skinColorFill[character->customizations.skinColor];
				Color stroke = characterType->skinColorStroke[character->customizations.skinColor];
				fill.a = opacity / 8;
				stroke.a = opacity;

				DrawCircleV(pos, radius, fill);
				DrawCircleSector(pos, radius, -charAngleDeg - 5, -charAngleDeg + 5, 8, stroke);
				DrawRing(pos, radius, radius * 0.925f, 0, 360, 32, stroke);
				//motor hud

/*				int motorCircleCount = (int)character->ballProps.radius / 4;
				float circleDist = radius * 0.6f;
				int circlesColored = 0;
				int circlesToBeColored = (int)(character->torque*0.01)+1;
				for(int i=0; i < motorCircleCount; i++){
					float circleAngle = ((float)i / (float)motorCircleCount) * (float)M_PI * 2.0f;
					Color circleColor = LIGHTGRAY;
					if((circleAngle > character->ballProps.motorCursor) && (circlesColored < circlesToBeColored)){
						circleColor = (Color){50+circlesColored*10,200-circlesColored*10,50,255};
						circlesColored++;
					}
					Vector2 circlePos;
							circlePos.x = character->pos.x + circleDist * sinf(character->angle + circleAngle);
							circlePos.y = character->pos.y + circleDist * -cosf(character->angle + circleAngle);
					DrawCircleV(circlePos, 4.0f, circleColor);
				}*/

			} break;
			case CHAR_CUBE: {
				Color bodyColor = GRAY;
				BeginTransformManual(pos, angle, 1.0f);
				int origin = (int)(-character->cubeProps.size / 2);
				int size = (int)character->cubeProps.size;
				DrawRectangle(origin, origin, size, size, bodyColor);
				Vector2 lookingVec = (Vector2){ sinf(character->primaryItemAngle), -cosf(character->primaryItemAngle) };

				DrawEllipse(-10, -3, 10, 18, WHITE);
				DrawEllipse(10, -3, 10, 18, WHITE);
				if(character->health){
					DrawEllipse((int)(-10 + 6 * lookingVec.x), (int)(-3 + 6 * lookingVec.y), 4, 6, BLACK);
					DrawEllipse((int)( 10 + 6 * lookingVec.x), (int)(-3 + 6 * lookingVec.y), 4, 6, BLACK);
					int eyeAnimFrame = (character->cubeProps.eyeAnimationFrame++) % 360;
					if(eyeAnimFrame < 15){
						DrawRectangle(origin, origin, size, (int)(50 * fabsf( sinf(eyeAnimFrame * 3.14f / 15.0f) )), bodyColor);
					}
				}else{
					DrawLineEx( (Vector2){-10 -5, -3 -5}, (Vector2){-10 +5, -3 +5}, 3.0f, BLACK);
					DrawLineEx( (Vector2){-10 +5, -3 -5}, (Vector2){-10 -5, -3 +5}, 3.0f, BLACK);
					DrawLineEx( (Vector2){ 10 -5, -3 -5}, (Vector2){ 10 +5, -3 +5}, 3.0f, BLACK);
					DrawLineEx( (Vector2){ 10 +5, -3 -5}, (Vector2){ 10 -5, -3 +5}, 3.0f, BLACK);
				}
				EndTransform();
			} break;
			default:
				DrawLine((int)pos.x - 5, (int)pos.y - 5,
						 (int)pos.x + 5, (int)pos.y + 5, RED);
				DrawLine((int)pos.x - 5, (int)pos.y + 5,
						 (int)pos.x + 5, (int)pos.y - 5, RED);
		}

		if(character->inventoryItemIds[0] != -1){
			DrawItem(character->inventoryItemIds[0]);
		}

	}
}