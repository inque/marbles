#pragma once

#include "physics.h"
#include "transforms.h"
#include "raylib.h"
#include "chipmunk/chipmunk.h"
#include "stdint.h"

#define LV_MAX_LAYERS 8
#define LV_MAX_POLYGONS 128
#define LV_POLY_MAX_VERTICES 32
#define LV_POLY_MAX_SHAPES 16
#define LV_MAX_TRIGGERS 128
#define LV_MAX_JOINTS 64
#define LV_MAX_CHECKPOINTS 64
#define LV_MAX_ENTITIES 256


/*
important notes

Level structure:
	Layers[]
	Polygons[] -
		ordered by:
			- layer
			- textured, then untextured
			- left to right (X axis)

if the polygon order is slightly wrong, everything is gonna break horribly
*/

typedef enum{
	LV_JOINT_SPRING
} LevelJoint_e;

typedef enum{
	LV_TRIGGER_DEATH,
	LV_TRIGGER_CHECKPOINT,
	LV_TRIGGER_FINISH,
	LV_TRIGGER_HINT,
	LV_TRIGGER_CHAT,
	LV_TRIGGER_TRANSFORM
} LevelTrigger_e;

typedef enum{
	LV_BLOCK_DYNAMIC = 1,
	LV_BLOCK_BACKGROUND = 2,
	LV_BLOCK_DOOR = 4
} LevelBlockFlags_e;

typedef struct{
	LevelJoint_e type;
	int polyA, polyB;
	cpConstraint* constraint;
	union{
		struct JointProps_Spring_st{
			Vector2 anchorA, anchorB;
			float restLength;
			float stiffness;
			float damping;
		} springProps;
	};
} LevelJoint_t;

typedef struct{
	LevelTrigger_e type;
	int triggered;
	Rectangle bounds;
	union{
		struct TriggerProps_Checkpoint_st{
			int id;
		} checkpointProps;
		struct TriggerProps_Hint_st{
			char text[42];
		} hintProps;
		struct TriggerProps_Chat_st{
			//ChatMessageType_e type;
			int type;
			int variation;
			char text[42];
		} chatProps;
		struct TriggerProps_Transform_st{
			//CharacterType_e charType;
			int charType;
		} transformProps;
	};
} LevelTrigger_t;

typedef struct{
	int vertCount;
	Vector2 verts[LV_POLY_MAX_VERTICES];
	Vector2 uvCoords[LV_POLY_MAX_VERTICES];
	int triangleCount;
	int triangleIndices[LV_POLY_MAX_VERTICES * 3];
	Color fillColor, borderColor;
	uint32_t flags; //LevelBlockFlags_e
	float mass;
	float friction;
	float elasticity;
	cpBody* body;
	cpShape* shapes[LV_POLY_MAX_SHAPES];
	ShapeUserData_t shapeUserData;
	Transform_t tform;
} LevelPoly_t;

typedef struct{
	Vector2 pos;
} LevelEntity_t;

typedef struct{
	int polyStartId;
	int polyTexturedCount;
	int polyUntexturedCount;
	int triggerStartId;
	int triggerCount;
	int entityStartId;
	int entityCount;
	//int order; //negative: foreground, positive: background
	Texture* texture;
	int isVisible;
	//float parallax;
	char name[16];
	char texturePath[24];
	Rectangle bounds;
} LevelLayer_t;

typedef struct{
	Color background;
	Vector2 spawnPos;
	int layerCount;
	int polygonCount;
	LevelLayer_t layers[LV_MAX_LAYERS];
	LevelPoly_t polygons[LV_MAX_POLYGONS];
	int triggerCount;
	LevelTrigger_t triggers[LV_MAX_TRIGGERS];
	int jointCount;
	LevelEntity_t entities[LV_MAX_ENTITIES];
	int entityCount;
	LevelJoint_t joints[LV_MAX_JOINTS];
	int checkpointCount;
	Vector2 checkpoints[LV_MAX_CHECKPOINTS];
	Rectangle bounds;

} Level_t;


//-------- file format ------------

typedef struct{
	uint32_t len;
	uint16_t version;
	uint8_t layerCount;
	uint32_t bgColor;
	float bounds[4];
} LV_File_Header_t;
//followed by: layers

typedef struct{
	uint32_t len;
	uint16_t polygonTexturedCount;
	uint16_t polygonUntexturedCount;
	uint16_t triggerCount;
} LV_File_Layer_t;
//each followed by: polys, triggers

typedef struct{
	uint8_t vertCount;
	//todo
} LV_File_Poly_t;

//---------------------------------

void LoadLevel(int id);
bool SaveLevel(Level_t* level, const char* path);
void InitializeLevelPhysics();
void CleanUpLevel();
void UpdateLevel();

void DrawLevelBackground();
void DrawLevel();
void DebugDrawEntities();
void DebugDrawTriggers();

void LV_InitPoly(Level_t* level, int polyId);
void LV_UpdatePolyBounds(LevelPoly_t* poly);
int LV_InsertPoly(Level_t* level, int layerId, bool isTextured);
void LV_RemovePoly(Level_t* level, int polyId);
void LV_SwapLayers(Level_t* level, int layerA_id, int layerB_id);
void LV_RemoveLayer(Level_t* level, int layerId);
