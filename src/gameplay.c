#include "gameplay.h"
#include "game.h"
#include "string.h"
#include "stdio.h"

/*
Template:
	switch(game.state){
		case STATE_UNINITIALIZED: {
			game.timer = 360;
			game.state = STATE_STARTING;
		} break;
		case STATE_STARTING: {

		} break;
		case STATE_PLAYING: {

		} break;
		case STATE_ENDED: {

		} break;
	}
*/

void Scenario_Regular_GameTick(){
	int respawnTime = 30;
	switch(game.state){
		case STATE_UNINITIALIZED: {
			game.timer = 60;
			game.state = STATE_STARTING;
		} break;
		case STATE_STARTING: {
			game.timer--;

			if(game.timer < 0){
				game.timer = 0;
				game.state = STATE_PLAYING;

				for(int playerId=0; playerId < MAX_PLAYERS; playerId++) {
					Player_t *player = &game.players[playerId];
					if(!player->active) continue;
					if(!player->isNPC){
						player->respawnFrames = respawnTime;
					}
				}
			}
		} break;
		case STATE_PLAYING: {

			for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
				Player_t* player = &game.players[playerId];
				if(!player->active) continue;

				if(!player->isNPC){
					if(player->characterId == -1){
						if(player->respawnFrames > respawnTime){
							Vector2 spawnAt;
							if(player->stats.checkpointId != -1){
								spawnAt = game.level.checkpoints[player->stats.checkpointId];
							}else{
								spawnAt = game.level.spawnPos;
							}
							player->characterId = SpawnCharacter(playerId, CHAR_BALL, spawnAt);
							//player->characterId = SpawnCharacter(playerId, CHAR_CUBE, spawnAt);
							GiveItem(player->characterId, ITEM_JUMPY);
							//GiveItem(player->characterId, ITEM_BLASTER);
							//GiveItem(player->characterId, ITEM_ROCK);
						}
					}
				}
			}

			game.timer++;
		} break;
		case STATE_ENDED: {



		} break;
	}
}

void Scenario_Deathmatch_GameTick(){
	int respawnTime = 90;
	switch(game.state){
	case STATE_UNINITIALIZED: {
		game.timer = 10*60;
		game.state = STATE_STARTING;
		/*
		int botGuyId = CreatePlayer(1);
		Player_t* botGuy = &game.players[botGuyId];
		strcpy(botGuy->name, "Bobbert");
		*/
	} break;
	case STATE_STARTING: {
		game.timer--;

		if( (game.playerCount < 2) && (game.timer < 5*60) ){
			game.timer = 5*60;
		}

		if(game.timer < 0){
			game.timer = 4*3600;
			//game.timer = 1000;
			game.state = STATE_PLAYING;

			for(int playerId=0; playerId < MAX_PLAYERS; playerId++) {
				Player_t *player = &game.players[playerId];
				if(!player->active) continue;
				player->respawnFrames = 30;
			}
		}
	} break;
	case STATE_PLAYING: {

		for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
			Player_t* player = &game.players[playerId];
			if(!player->active) continue;

			if(player->characterId == -1){
				if(player->respawnFrames > respawnTime){
					Vector2 spawnAt;
					spawnAt = game.level.spawnPos;
					player->characterId = SpawnCharacter(playerId, CHAR_BALL, spawnAt);
					//player->characterId = SpawnCharacter(playerId, CHAR_CUBE, spawnAt);
					GiveItem(player->characterId, ITEM_JUMPY);
					//GiveItem(player->characterId, ITEM_BLASTER);
				}
			}
		}

		game.timer--;
		if(!game.timer){
			game.timer = 10*60;
			game.state = STATE_ENDED;
		}
	} break;
	case STATE_ENDED: {
		
		game.timer--;
		if(!game.timer){
			app.shouldRestartRound = 1;
		}
		
	} break;
	}
}

void OnCharacterDeath(int characterId){
	Character_t* character = &game.characters[characterId];
	Player_t* player = &game.players[character->playerId];
	bool killedByPlayer;
	Player_t* killerPlayer = NULL;
	if(character->healthLastDamageSource == DEATH_BY_ITEM){
		killedByPlayer = true;
	}else{
		if( (character->healthLastDamageByPlayerFrame != -1) && ((game.frame - character->healthLastDamageByPlayerFrame) < 5*60) ){
			killedByPlayer = true;
		}else{
			killedByPlayer = false;
		}
	}
	if(killedByPlayer){
		killerPlayer = &game.players[character->healthLastDamagePlayerId];
		killerPlayer->stats.killCount++;
		SynchronizePlayerStats(character->healthLastDamagePlayerId);
	}
	player->characterId = -1;
	player->stats.deathCount++;
	SynchronizePlayerStats(character->playerId);

	bool killfeed = (app.role != ROLE_LOCAL);

	if(killfeed){
		char text[8192];
		if(killedByPlayer){
			ItemType_t* itemType = &game.itemTypes[character->healthLastDamageItemType];
			sprintf(text, "%s ate %s's %s.", player->name, killerPlayer->name, itemType->name);
		}else{
			switch(character->healthLastDamageSource){
				case DEATH_BY_TRIGGER: {
					sprintf(text, "%s fell off the map.", player->name);
				} break;
				default: {
					sprintf(text, "%s died.", player->name);
				} break;
			}
		}
		
		SendChatMessage(CHATMSG_GLOBAL, 0, text, 0);
	}

}
