//master.c

#include "masterserver.h"
#include "masterserverpackets.h"
#include "networkingpackets.h"
#include "game.h"
#include "networking.h"
#include "enetheaders.h"
#include "string.h"
#include "stdio.h"
#include "time.h"

typedef struct{
	ENetAddress address;
	ENetSocket sock;
	union{
		MasterRequestMessage_t requestData;
		InfoRequestMessage_t infoRequestData;
	};
	union{
		MasterResponseMessage_t responseData;
		InfoResponseMessage_t infoResponseData;
	};
	ENetBuffer requestBuffer;
	ENetBuffer responseBuffer;
	double lastSent;
	double lastReceived;
	bool serverListNeedsRefreshing;
	int serverCount;
	ServerInfo_t serverList[SERVER_LIST_MAX_LENGTH];
} MasterServerState_t;

static MasterServerState_t master;


void InitializeMasterServerConnection(){
	enet_address_set_host(&master.address, MASTER_HOST);
	master.address.port = MASTER_PORT;

	master.sock = enet_socket_create(ENET_SOCKET_TYPE_DATAGRAM);
	enet_socket_set_option(master.sock, ENET_SOCKOPT_NONBLOCK, 1);
	enet_socket_set_option(master.sock, ENET_SOCKOPT_IPV6_V6ONLY, 0);
	enet_socket_bind(master.sock, NULL);

	//printf("master init wsa last err: %i\n", WSAGetLastError());

	master.requestBuffer.data = (void*)&master.requestData;
	master.responseBuffer.data = (void*)&master.responseData;
	master.requestBuffer.dataLength = sizeof(master.requestData);
	master.responseBuffer.dataLength = sizeof(master.responseData);


}

static void ProcessMasterPacket(MasterResponseMessage_t* msg, int bytes){
	if(msg->magic != NET_MAGIC_ID) return;
	switch(msg->code){
		case MASTER_RESPONSE_SERVERLIST: {

			if((msg->serverList.serverSize * msg->serverList.serverCount) > (bytes - 9)){
				//printf("Wrong message length. (SERVER_LIST)\n");
				return;
			}
			master.serverCount = msg->serverList.serverCount;
			if(master.serverCount >= SERVER_LIST_MAX_LENGTH){
				master.serverCount = (SERVER_LIST_MAX_LENGTH - 1);
			}
			struct MasterResponseServerListEntry_st* src = &msg->serverList.servers[0];
			for(int i = 0; i < master.serverCount; i++){
				ServerInfo_t* dst = &master.serverList[i];
				dst->isSynchronized = false;
				dst->lastPinged = 0;
				dst->lastReceived = GetTime();
				memcpy(&dst->ip, &src->ip, sizeof(dst->ip));
				dst->port = src->port;
				dst->title[0] = 0;
				src += (size_t)msg->serverList.serverSize;
			}
			master.serverListNeedsRefreshing = false;

		} break;
		default:
			break;
	}
}

static void GuessMasterRequestSize(){
	size_t res = 5;
	size_t maxSize = sizeof(master.requestData);
	switch(master.requestData.code){
	case MASTER_REQUEST_SERVERLIST:
		res += sizeof(struct MasterRequestServerList_st);
		break;
	case MASTER_REQUEST_HOST:
		res += sizeof(struct MasterRequestHost_st);
		break;
	}
	if(res > maxSize) res = maxSize;
	master.requestBuffer.dataLength = res;
}

static void SendMasterPacket(bool host){
	master.lastSent = GetTime();
	GuessMasterRequestSize();
	master.requestData.magic = NET_MAGIC_ID;

	ENetSocket sock = master.sock;
	if(host){
		ENetHost* host = GetHost();
		if(!host) return;
		sock = host->socket;
	}
	int res = enet_socket_send(sock, &master.address, &master.requestBuffer, 1);

	//int res = enet_socket_send(master.sock, &master.address, &master.requestBuffer, 1);
	if(res == -1){
		//printf("last error: %i\n", WSAGetLastError());
		//???
	}
}

static void ProcessInfoResponse(InfoResponseMessage_t* msg, ServerInfo_t* server, int bytes){
	if(msg->magic != NET_MAGIC_ID) return;
	switch(msg->code){
		case INFO_RESPONSE_STATUS: {
			if(!server->isSynchronized){
				server->version = msg->status.version;
				server->isCompatible = msg->status.isCompatible;
				strncpy((char*)&server->title, (char*)&msg->status.title, sizeof(server->title));
				server->title[sizeof(server->title)-1] = 0;
				server->isSynchronized = true;
			}
			server->gameType = msg->status.gameType;
			server->playersOn = msg->status.playersOn;
			server->playersMax = msg->status.playersMax;
			server->ping = (unsigned int)(GetTime()*1000) - msg->status.clientTimestamp;
			server->lastReceived = GetTime();
		} break;
	}
}

void UpdateMasterServerConnection(){
	ENetAddress addr;
	bool receivedAny = false;
	while(true){
		int bytesReceived = enet_socket_receive(master.sock, &addr, &master.responseBuffer, 1);

		if(bytesReceived == -1){
			//printf("recv last error: %i\n", WSAGetLastError());
		}

		if(bytesReceived <= 0) break;
		if((addr.port == master.address.port) && in6_equal(addr.host, master.address.host)){
			master.lastReceived = GetTime();
			ProcessMasterPacket(&master.responseData, bytesReceived);
			receivedAny = true;
		}else if(app.mainMenuScreen == MAINMENU_SERVERLIST){
			//printf("got a packed not related to master\n");
			for(int i=0; i < master.serverCount; i++){
				ServerInfo_t* server = &master.serverList[i];
				if((addr.port == server->port) && (memcmp(&addr.host, &server->ip, sizeof(addr.host)) == 0)){
					//printf("aha! got info, processing\n");
					ProcessInfoResponse(&master.infoResponseData, server, bytesReceived);
				}

			}
		}
	}

	double elapsedSinceLastSent = (GetTime() - master.lastSent);
	//double elapsedSinceLastReceived = receivedAny ? 0 : (GetTime() - master.lastReceived);

	if((app.screen == SCREEN_MAINMENU) && (app.mainMenuScreen == MAINMENU_SERVERLIST)){
		if(master.serverListNeedsRefreshing && (!receivedAny) && (elapsedSinceLastSent > 0.250)){
			master.requestData.code = MASTER_REQUEST_SERVERLIST;
			master.requestData.serverList.version = 2;
			SendMasterPacket(false);
		}

		double now = GetTime();
		for(int i = 0; i < master.serverCount; i++){
			ServerInfo_t* server = &master.serverList[i];
			if(server->isSynchronized && ((now - server->lastReceived) > 4)){
				server->isSynchronized = false;
			}
		}
	}

	if(app.screen == SCREEN_GAME){
		if(app.role == ROLE_HOST){
			if(elapsedSinceLastSent > 1.0){
				master.requestData.code = MASTER_REQUEST_HOST;
				master.requestData.host.gameId = 826430029; //"MRB1"
				master.requestData.host.unused = 0;
				SendMasterPacket(true);
			}
		}
	}
}

void RefreshServerList(){
	master.serverCount = 0;
	master.serverListNeedsRefreshing = true;
}

void GetListOfServers(int* count, ServerInfo_t** list){
	*count = master.serverCount;
	*list = (ServerInfo_t*)&master.serverList;
}

void PingServer(ServerInfo_t* server){
	ENetAddress addr;
	addr.port = server->port;
	memcpy(&addr.host, &server->ip, sizeof(addr.host));
	//enet_address_set_host(&addr, "localhost");

	//char ip[1024];
	//enet_address_get_host_ip(&addr, (char*)&ip, sizeof(ip));
	//printf("pinging. ip = %s. port = %i\n", (char*)&ip, server->port);

	addr.sin6_scope_id = 0;
	//addr.host.__in6_u.__u6_addr8 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xFF, 0xFF, 127, 0, 0, 1};
	//enet_address_set_host_ip(&addr, "::1/128");
	master.infoRequestData.magic = NET_MAGIC_ID;
	master.infoRequestData.code = INFO_REQUEST_STATUS;
	master.infoRequestData.status.version = 1;
	master.infoRequestData.status.clientTimestamp = (uint32_t)(GetTime()*1000);
	master.requestBuffer.dataLength = 5 + sizeof(struct InfoResponseStatus_st);
	enet_socket_send(master.sock, &addr, &master.requestBuffer, 1);
	server->lastPinged = GetTime();
}

bool ProcessInfoRequest(InfoRequestMessage_t* msg){
	ENetHost* host = GetHost();
	switch(msg->code){
		case INFO_REQUEST_STATUS: {
			master.infoResponseData.magic = NET_MAGIC_ID;
			master.infoResponseData.code = INFO_RESPONSE_STATUS;
			master.infoResponseData.status.clientTimestamp = msg->status.clientTimestamp;
			master.infoResponseData.status.version = 2;
			master.infoResponseData.status.gameType = game.type;
			master.infoResponseData.status.isCompatible = (msg->status.version == 2) ? 1 : 0;
			master.infoResponseData.status.playersMax = 32;
			master.infoResponseData.status.playersOn = (char)game.playerCount;
			strncpy((char*)&master.infoResponseData.status.title, app.settings.hostTitle, sizeof(master.infoResponseData.status.title));
			master.responseBuffer.dataLength = 5 + sizeof(struct InfoResponseStatus_st);
			enet_host_send_raw(host, &host->receivedAddress, (enet_uint8*)&master.infoResponseData, master.responseBuffer.dataLength);
			char tmp[1024];
			enet_address_get_host_ip(&host->receivedAddress, (char*)&tmp, sizeof(tmp));
			//printf("info responded to: %s\n", tmp);
		} break;
		default:
			return 0;
	}
	return 1;
}
