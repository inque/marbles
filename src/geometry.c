#include "chipmunk/chipmunk.h"
#include "geometry.h"
#include "stdio.h"
#include "stdbool.h"

//triangulation function based on: https://www3.cs.stonybrook.edu/~skiena/algorist/book/programs/triangulate.c

static bool rl_IsTriangleClockWise(Vector2 p1, Vector2 p2, Vector2 p3){
    float a = p2.x * p1.y + p3.x * p2.y + p1.x * p3.y;
    float b = p1.x * p2.y + p2.x * p3.y + p3.x * p1.y;
    return (a > b);
}

static bool cp_IsTriangleClockWise(cpVect p1, cpVect p2, cpVect p3){
	cpFloat a = p2.x * p1.y + p3.x * p2.y + p1.x * p3.y;
	cpFloat b = p1.x * p2.y + p2.x * p3.y + p3.x * p1.y;
	return (a > b);
}

static bool cp_IsPointInTriangle(cpVect p, cpVect* t){
	for(int i=0; i<3; i++)
		if(cp_IsTriangleClockWise(t[i], t[(i + 1) % 3], p)) return false;
	return true;
}

static bool cp_Ear_Q(int i, int j, int k, int vertCount, const cpVect* verts){
	cpVect t[3];

	t[0].x = verts[i].x;
	t[0].y = verts[i].y;

	t[1].x = verts[j].x;
	t[1].y = verts[j].y;

	t[2].x = verts[k].x;
	t[2].y = verts[k].y;

	if(cp_IsTriangleClockWise(t[0], t[1], t[2])) return false;

	for(int m=0; m < vertCount; m++){
		if((m != i) && (m != j) && (m != k))
			if(cp_IsPointInTriangle(verts[m], (cpVect*) &t)) return false;
	}
	return true;
}

void cp_Triangulate(int vertCount, const cpVect *verts, SplitOutput* out){
	int l[SPLIT_MAX_POLYGONS], r[SPLIT_MAX_POLYGONS]; // left/right neighbor indices
	for(int i=0; i < vertCount; i++){
		l[i] = ((i-1) + vertCount) % vertCount;
		r[i] = ((i+1) + vertCount) % vertCount;
	}
	out->polyCount = 0;
	int i = vertCount-1;
	int iterationCounter = 0;
	while(out->polyCount < (vertCount - 2)){
		i = r[i];
		if( (iterationCounter++) > vertCount ) return;
		if(cp_Ear_Q(l[i], i, r[i], vertCount, verts)){
			SplitPoly* newPoly = &out->polygons[out->polyCount++];
			newPoly->vertCount = 3;
			newPoly->indices[0] = l[i];
			newPoly->indices[1] = i;
			newPoly->indices[2] = r[i];
			newPoly->verts[0].x = verts[newPoly->indices[0]].x;
			newPoly->verts[0].y = verts[newPoly->indices[0]].y;
			newPoly->verts[1].x = verts[newPoly->indices[1]].x;
			newPoly->verts[1].y = verts[newPoly->indices[1]].y;
			newPoly->verts[2].x = verts[newPoly->indices[2]].x;
			newPoly->verts[2].y = verts[newPoly->indices[2]].y;
			//printf("added triangle: %i %i %i\n", l[i], i, r[i]);
			if(out->polyCount >= SPLIT_MAX_POLYGONS) return;
			l[ r[i] ] = l[i];
			r[ l[i] ] = r[i];
		}
	}
}

//void cp_MergeHulls(SplitOutput* out, int vertCount){
//	SplitOutput input;
//	bool polyMerged[SPLIT_MAX_POLYGONS];
//	memcpy(&input, out, sizeof(SplitOutput));
//	memset(&polyMerged, 0, sizeof(polyMerged));
//
//	for(int polyIdA=0; polyIdA < input.polyCount; polyIdA++){
//		if(polyMerged[polyIdA]) continue;
//		SplitPoly* polyA = &input.polygons[polyIdA];
//		for(int polyIdB=0; )
//
//	}
//
//}

bool rl_IsConvex(int vertCount, Vector2* verts){
    for(int i=0; i < vertCount; i++) {
        if(!rl_IsTriangleClockWise(verts[i], verts[(i + 1) % vertCount], verts[(i + 2) % vertCount])) return false;
    }
    return true;
}
