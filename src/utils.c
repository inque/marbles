#include "utils.h"
#include "math.h"

//from raylib/core.c

#if defined(_WIN32)
	void __stdcall Sleep(unsigned long msTimeout);
#elif defined(__linux__) || defined(PLATFORM_WEB)
	#include "unistd.h"
	#include "time.h"
#endif



bool PointInRect(Rectangle bounds, Vector2 point){
	if( (point.x >= bounds.x) && (point.x <= (bounds.x + bounds.width))
	 && (point.y >= bounds.y) && (point.y <= (bounds.y + bounds.height))){
		 return true;
	}
	return false;
}

//from https://stackoverflow.com/a/1501725
float DistToSegment(Vector2 p, Vector2 v, Vector2 w){
	float l2 = Vector2Distance(v, w);
	if(l2 == 0) return Vector2Distance(p, v);
	float t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / (l2 * l2);
	t = fmaxf(0, fminf(1, t));
	return Vector2Distance(p, (Vector2){
		.x = v.x + t * (w.x - v.x),
		.y = v.y + t * (w.y - v.y)
	});
}


//from raylib/core.c

void Wait(float ms)
{

#if defined(SUPPORT_BUSY_WAIT_LOOP) && !defined(PLATFORM_UWP)
	double prevTime = GetTime();
	double nextTime = 0.0;

	// Busy wait loop
	while ((nextTime - prevTime) < ms/1000.0f) nextTime = GetTime();
#else
#if defined(SUPPORT_HALFBUSY_WAIT_LOOP)
#define MAX_HALFBUSY_WAIT_TIME  4
	double destTime = GetTime() + ms/1000;
	if (ms > MAX_HALFBUSY_WAIT_TIME) ms -= MAX_HALFBUSY_WAIT_TIME;
#endif

#if defined(_WIN32)
	Sleep((unsigned int)ms);
#elif defined(__linux__) || defined(PLATFORM_WEB)
	struct timespec req = { 0 };
	time_t sec = (int)(ms/1000.0f);
	ms -= (float)(sec*1000);
	req.tv_sec = sec;
	req.tv_nsec = ms*1000000L;

	// NOTE: Use nanosleep() on Unix platforms... usleep() it's deprecated.
	while (nanosleep(&req, &req) == -1) continue;
#elif defined(__APPLE__)
	usleep(ms*1000.0f);
#endif

#if defined(SUPPORT_HALFBUSY_WAIT_LOOP)
	while (GetTime() < destTime) { }
#endif
#endif
}