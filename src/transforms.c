#include "transforms.h"
#include "game.h"
#include "raylib.h"

#define BOILERPLATE() int a = game.frame & 1; float t = app.deltaTimeGame; int b = 1 - a;

Transform_t NewTransform(Vector2 pos, float angle){
	Transform_t res;
	for(int i=0; i < 2; i++){
		res.pos[i] = pos;
		res.angle[i] = angle;
		res.frameReceived[i] = game.frame;
	}
	return res;
}

void UpdateTransform(Transform_t* tform, cpBody* body){
	BOILERPLATE();
	cpVect pos = cpBodyGetPosition(body);
	tform->pos[a] = (Vector2){pos.x, pos.y};
	tform->angle[a] = cpBodyGetAngle(body);
	tform->frameReceived[a] = game.frame;
}

void SetTransformData(Transform_t* tform, Vector2 pos, float angle){
	BOILERPLATE();
	tform->pos[a] = pos;
	tform->angle[a] = angle;
	tform->frameReceived[a] = game.frame;
}

void SetTransformStatic(Transform_t* tform, Vector2 pos, float angle){
	for(int i=0; i < 2; i++){
		tform->pos[i] = pos;
		tform->angle[i] = angle;
		tform->frameReceived[i] = game.frame;
	}
}

bool DoesIntersectRect(Transform_t* tform, Rectangle rec){
	BOILERPLATE();
	return CheckCollisionRecs(GetBounds(tform), rec);
}

bool IsInView(Transform_t* tform){
	BOILERPLATE();
	return DoesIntersectRect(tform, app.viewBounds);
}

bool HasChanged(Transform_t* tform){
	BOILERPLATE();
	if(tform->pos[a].x != tform->pos[b].x) return true;
	if(tform->pos[a].y != tform->pos[b].y) return true;
	if(tform->angle[a] != tform->angle[b]) return true;
	return false;
}

Vector2 GetPos(Transform_t* tform){
	BOILERPLATE();
	return tform->pos[a];
}

float GetAngle(Transform_t* tform){
	BOILERPLATE();
	return tform->angle[a];
}

Rectangle GetBounds(Transform_t* tform){
	BOILERPLATE();
	return (Rectangle){ tform->pos[a].x - tform->size.x / 2,
						tform->pos[a].y - tform->size.y / 2,
						tform->size.x,
						tform->size.y };
}

Vector2 GetInterpolatedPos(Transform_t* tform){
	BOILERPLATE();
	if(!app.frameSmoothing){
		return tform->pos[a];
	}else{
		//return Vector2Lerp(tform->pos[1-a], tform->pos[a], t);
		return Vector2Lerp(tform->pos[a], tform->pos[1-a], t);
		//return Vector2Lerp(tform->pos[a], tform->pos[1-a], t * 0.5f);
	}
}

float GetInterpolatedAngle(Transform_t* tform){
	BOILERPLATE();
	if(!app.frameSmoothing){
		return tform->angle[a];
	}else{
		return tform->angle[a] + t * (tform->angle[1-a] - tform->angle[a]);
	}
}
