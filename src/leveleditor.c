#include "leveleditor.h"
#include "game.h"
#include "utils.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "math.h"
#include "raygui.h"

LevelEditorState_t* editor = NULL;

void StartLevelEditor(){
	CloseLevelEditor();

	StartGame(ROLE_LOCAL, GAME_REGULAR, -1);
	app.debugMode = 1;

	editor = calloc(1, sizeof(LevelEditorState_t));
	editor->showTriggers = 1;

	app.screen = SCREEN_EDITOR;
	app.cam.target = (Vector2){0, 0};
	app.cam.zoom = 1;

	//CleanUpLevel();

	game.level.background = (Color){250, 243, 230};

	game.level.layerCount = 2;
	game.level.layers[0] = (LevelLayer_t){
		.name = "background",
		.isVisible = 1
	};
	game.level.layers[1] = (LevelLayer_t){
		.name = "main",
		.isVisible = 1
	};
}

void CloseLevelEditor(){
	if(!editor) return;
	free(editor);
}

static void ResetTextInputs(){
	editor->areWeRenamingSelectedLayer = 0;
	editor->areWeModifyingLayerTexturePath = 0;
}

static bool AreWeEditingAnything(){
	return (editor->areWeRenamingSelectedLayer
		 || editor->areWeModifyingLayerTexturePath);
}

static bool DoesMouseOverlap(Rectangle bounds){
	Vector2 mousePos = GetMousePosition();
	return PointInRect(bounds, mousePos);
}

static void BottomTooltip(Rectangle bounds, char* text){
	if(DoesMouseOverlap(bounds)){
		 editor->bottomTooltip_p = text;
	}
}

static void ToolbarWindow(){
	const int smallMargin = 2;
	Rectangle windowBounds = {5, 5, app.width - 10, 80};
	Rectangle buttonBounds = {windowBounds.x + 5, windowBounds.y + 25, 40, 20};
	Rectangle lowerButtonBounds = {windowBounds.x + 5, buttonBounds.y + 5 + buttonBounds.height, 26, 20};
	Rectangle rightButtonBounds = {windowBounds.x + windowBounds.width - 40, windowBounds.y + 25, 45, 20};
	bool kbShortcuts = !AreWeEditingAnything();
	GuiWindowBox(windowBounds, "Toolbar");
	if(DoesMouseOverlap(windowBounds)){
		editor->isHoveringOverWindow = true;
	}
	//document
	if(GuiButton(buttonBounds, "new")){
	}
	buttonBounds.x += buttonBounds.width + smallMargin;
	if(GuiButton(buttonBounds, "open")){
	}
	buttonBounds.x += buttonBounds.width + smallMargin;
	if(GuiButton(buttonBounds, "save")){
	}
	buttonBounds.x += buttonBounds.width + smallMargin;
	if(GuiButton(buttonBounds, "save as")){
	}
	buttonBounds.x += buttonBounds.width + smallMargin;
	//tools
	BottomTooltip(lowerButtonBounds, "Select (Q)");
	if(GuiToggle(lowerButtonBounds, "Sel", (editor->tool == LVE_TOOL_SELECT))){
		editor->tool = LVE_TOOL_SELECT;
	}
	lowerButtonBounds.x += lowerButtonBounds.width + smallMargin;
	BottomTooltip(lowerButtonBounds, "Move (W)");
	if(GuiToggle(lowerButtonBounds, "Move", (editor->tool == LVE_TOOL_MOVE))){
		editor->tool = LVE_TOOL_MOVE;
	}
	lowerButtonBounds.x += lowerButtonBounds.width + smallMargin;
	BottomTooltip(lowerButtonBounds, "Edit vertices (E)");
	if(GuiToggle(lowerButtonBounds, "Vert", (editor->tool == LVE_TOOL_VERTEX))){
		editor->tool = LVE_TOOL_VERTEX;
	}
	lowerButtonBounds.x += lowerButtonBounds.width + smallMargin;
	BottomTooltip(lowerButtonBounds, "Add Rectangle (R)");
	if(GuiToggle(lowerButtonBounds, "Rect", (editor->tool == LVE_TOOL_RECT))){
		editor->tool = LVE_TOOL_RECT;
	}
	lowerButtonBounds.x += lowerButtonBounds.width + smallMargin;
	BottomTooltip(lowerButtonBounds, "Trigger (T)");
	if(GuiToggle(lowerButtonBounds, "Trig", (editor->tool == LVE_TOOL_TRIGGER))){
		editor->showTriggers = 1;
		editor->tool = LVE_TOOL_TRIGGER;
	}
	lowerButtonBounds.x += lowerButtonBounds.width + smallMargin;
	//palette
	Rectangle paletteBounds = {lowerButtonBounds.x + 50, lowerButtonBounds.y - 4, 30, 30};
	for(int i=0; i < 10; i++){
		DrawRectangleLinesEx(paletteBounds, 1, BLACK);
		paletteBounds.x += paletteBounds.width + smallMargin;
	}
	//right
	if(GuiButton(rightButtonBounds, "Run [R]")){
		app.screen = SCREEN_GAME;
	}
	rightButtonBounds.x -= rightButtonBounds.width + smallMargin;
	if(GuiToggle(rightButtonBounds, "Show trigs", editor->showTriggers)){
		editor->showTriggers = !editor->showTriggers;
	}
	rightButtonBounds.x -= rightButtonBounds.width + smallMargin;

}

static void PropertyWindow(){
	const int smallMargin = 2;
	Rectangle windowBounds = {app.width - 200, 85, 190, 160};
	Rectangle propBounds = {windowBounds.x + 5, windowBounds.y + 25, windowBounds.width - 10, 20};
	char text[8192];
	GuiWindowBox(windowBounds, "Properties");
	if(DoesMouseOverlap(windowBounds)){
		editor->isHoveringOverWindow = true;
	}
	switch(editor->selectionTarget){
		case LVE_SEL_LAYER: {
			LevelLayer_t* selectedLayer = &game.level.layers[editor->layerSelectedId];
			GuiLabel(propBounds, "Layer.");
			propBounds.y += propBounds.height + smallMargin;
			GuiLabel(propBounds, "Texture path:");
			propBounds.y += propBounds.height + smallMargin;
			if(GuiTextBox(propBounds, selectedLayer->texturePath, sizeof(selectedLayer->texturePath), editor->areWeModifyingLayerTexturePath)){
				editor->areWeModifyingLayerTexturePath = !editor->areWeModifyingLayerTexturePath;
				if(!editor->areWeModifyingLayerTexturePath){
					selectedLayer->texture = ResolveTexture(selectedLayer->texturePath);
				}else{
					//DecrementAsset(selectedLayer->texturePath)
				}
			}
			if(selectedLayer->texture){
				DrawTextureEx(*selectedLayer->texture, (Vector2){propBounds.x, propBounds.y + 20}, 0, 0.1f, WHITE);
			}
			propBounds.y += propBounds.height + smallMargin;
			sprintf(text, "%i %i %i", selectedLayer->polyStartId, selectedLayer->polyTexturedCount, selectedLayer->polyUntexturedCount);
			GuiLabel(propBounds, text);
		} break;
		case LVE_SEL_POLY: {
			GuiLabel(propBounds, "Poly.");
			propBounds.y += propBounds.height + smallMargin;
			//sprintf(text, "%i polys selected. first: %i", levelEditorState->polysSelected, levelEditorState->polySelection[0]);
			//GuiLabel(propBounds, text);
			if(!editor->polysSelected) break;
			LevelPoly_t* firstPoly = &game.level.polygons[editor->polySelection[0]];
			firstPoly->borderColor = GuiColorPicker((Rectangle){propBounds.x, propBounds.y, 120, 60}, firstPoly->borderColor);

		} break;
	}
}

static void LayerEditorWindow(){
	const int smallMargin = 2;
	Rectangle windowBounds = {app.width - 175, 90 + 175, 170, 230};
	Rectangle listBounds = {windowBounds.x + 5, windowBounds.y + 50, windowBounds.width - 10, 18};
	Rectangle buttonBounds = {windowBounds.x + 5, listBounds.y + 5 + listBounds.height * LV_MAX_LAYERS, 20, 20};
	if(IsKeyDown(KEY_LEFT_SHIFT) || IsKeyDown(KEY_RIGHT_SHIFT)){
		if(IsKeyPressed(KEY_UP) && editor->layerSelectedId){
			editor->layerSelectedId--;
		}else if(IsKeyPressed(KEY_DOWN) && (editor->layerSelectedId+1 < game.level.layerCount)){
			editor->layerSelectedId++;
		}else if(IsKeyPressed(KEY_RIGHT)){
			editor->selectionTarget = LVE_SEL_LAYER;
		}
	}
	GuiWindowBox(windowBounds, "Layers");
	if(DoesMouseOverlap(windowBounds)){
		editor->isHoveringOverWindow = true;
	}
	//list
	for(int layerId=0; layerId < game.level.layerCount; layerId++){
		LevelLayer_t* layer = &game.level.layers[layerId];
		bool isLayerSelected = (layerId == editor->layerSelectedId);
		Rectangle layerVisBounds = {listBounds.x + 3, listBounds.y, 20, 20};
		Rectangle layerTitleBounds = {listBounds.x + 28, listBounds.y, listBounds.width - 32, listBounds.height};
		if(isLayerSelected){
			DrawRectangleRec(layerTitleBounds, (Color){200,200,250,255});
		}
		if(GuiButton(layerVisBounds, layer->isVisible ? "V" : "x")){
			layer->isVisible = !layer->isVisible;
		}
		if(isLayerSelected && editor->areWeRenamingSelectedLayer){
			if(GuiTextBox(layerTitleBounds, layer->name, sizeof(layer->name), editor->areWeRenamingSelectedLayer)){
				editor->areWeRenamingSelectedLayer = !editor->areWeRenamingSelectedLayer;
			}
		}else{
			if(GuiLabelButton(layerTitleBounds, layer->name)){
				if(isLayerSelected){
					editor->areWeRenamingSelectedLayer = 1;
				}else{
					editor->layerSelectedId = layerId;
					editor->selectionTarget = LVE_SEL_LAYER;
				}
			}
		}
		listBounds.y += listBounds.height;
	}
	//buttons
	if(GuiButton(buttonBounds, "+")){
		ResetTextInputs();
		if(game.level.layerCount < LV_MAX_LAYERS){
			int layersAboveAndIncludingSelected = (game.level.layerCount - editor->layerSelectedId);
			memmove(&game.level.layers[editor->layerSelectedId + 1],
					&game.level.layers[editor->layerSelectedId],
					sizeof(LevelLayer_t) * layersAboveAndIncludingSelected);
			game.level.layerCount++;
			int newLayerId = editor->layerSelectedId;
			LevelLayer_t* newLayer = &game.level.layers[newLayerId];
			*newLayer = (LevelLayer_t){
				.name = "new layer",
				.isVisible = 1
			};
			if(newLayerId > 0){
				LevelLayer_t* layerBelow = &game.level.layers[newLayerId - 1];
				newLayer->polyStartId = layerBelow->polyStartId + layerBelow->polyTexturedCount + layerBelow->polyUntexturedCount;
			}
		}
	}
	buttonBounds.x += buttonBounds.width + smallMargin;
	if(GuiButton(buttonBounds, "-")){
		ResetTextInputs();
		// if(game.level.layerCount > 1){
		// 	int layersAboveSelected = (game.level.layerCount - 1 - levelEditorState->layerSelectedId);
		// 	memmove(&game.level.layers[levelEditorState->layerSelectedId],
		// 			&game.level.layers[levelEditorState->layerSelectedId + 1],
		// 			sizeof(LevelLayer_t) * layersAboveSelected);
		// 	game.level.layerCount--;
		// }
		LV_RemoveLayer(&game.level, editor->layerSelectedId);
	}
	buttonBounds.x += buttonBounds.width + smallMargin;
	if(GuiButton(buttonBounds, "<")){
		ResetTextInputs();
		if((game.level.layerCount >= 2) && editor->layerSelectedId){
			// LevelLayer_t tmp = game.level.layers[levelEditorState->layerSelectedId];
			// game.level.layers[levelEditorState->layerSelectedId] = game.level.layers[levelEditorState->layerSelectedId - 1];
			// game.level.layers[levelEditorState->layerSelectedId - 1] = tmp;
			LV_SwapLayers(&game.level,
							editor->layerSelectedId,
							editor->layerSelectedId - 1);
			editor->layerSelectedId--;
		}
	}
	buttonBounds.x += buttonBounds.width + smallMargin;
	if(GuiButton(buttonBounds, ">")){
		ResetTextInputs();
		if((game.level.layerCount >= 2) && (editor->layerSelectedId < (game.level.layerCount - 1))){
			// LevelLayer_t tmp = game.level.layers[levelEditorState->layerSelectedId];
			// game.level.layers[levelEditorState->layerSelectedId] = game.level.layers[levelEditorState->layerSelectedId + 1];
			// game.level.layers[levelEditorState->layerSelectedId + 1] = tmp;
			LV_SwapLayers(&game.level,
							editor->layerSelectedId,
							editor->layerSelectedId + 1);
			editor->layerSelectedId++;
		}
	}
	buttonBounds.x += buttonBounds.width + smallMargin;
}

void UpdateLevelEditor(){
	char text[8192];
	editor->bottomTooltip_p = NULL;
	editor->isHoveringOverWindow = false;

	Vector2 mouseScreenPos = GetMousePosition();
	Vector2 mouseDelta = Vector2Subtract(mouseScreenPos, editor->mousePosLast);
	Vector2 cursor = GetScreenToWorld2D(mouseScreenPos, app.cam);
	float lineThickness = 2 * fmaxf(1, 1/app.cam.zoom);
    
	bool deletePressed = IsKeyPressed(KEY_DELETE);
	bool rmbPressed = IsMouseButtonPressed(MOUSE_RIGHT_BUTTON);
	bool lmbDown = IsMouseButtonDown(MOUSE_LEFT_BUTTON);
	bool lmbPressed = IsMouseButtonPressed(MOUSE_LEFT_BUTTON);
	bool lmbUp = IsMouseButtonReleased(MOUSE_LEFT_BUTTON);
	if(lmbPressed){
		editor->isDragging = true;
		editor->dragOrigin = cursor;
	}else if(lmbUp){
		editor->isDragging = false;
	}


	Rectangle selection = { editor->dragOrigin.x,
							editor->dragOrigin.y,
							cursor.x - editor->dragOrigin.x,
							cursor.y - editor->dragOrigin.y };
	if(selection.width < 0){
		selection.width *= -1;
		selection.x -= selection.width;
	}
	if(selection.height < 0){
		selection.height *= -1;
		selection.y -= selection.height;
	}


	sprintf(text, "x: %.01f y: %.01f", cursor.x, cursor.y);
	DrawText(text, app.width - 180, app.height - 40, 18, DARKGRAY);
	sprintf(text, "tool: %s", toolNames[editor->tool]);
	DrawText(text, app.width - 180, app.height - 60, 18, DARKGRAY);
	sprintf(text, "zoom: %.01f", app.cam.zoom);
	app.cam.zoom = GuiSlider((Rectangle){app.width - 180, app.height - 20, 85, 15}, "-", "+", app.cam.zoom, 0.1f, 10.0f);
	DrawText(text, app.width - 180 + 105, app.height - 20, 18, DARKGRAY);


	ToolbarWindow();
	PropertyWindow();
	LayerEditorWindow();

	UpdateViewBounds();
	BeginMode2D(app.cam);

	DrawRectangleLinesEx((Rectangle){50,50,200,200}, lineThickness, (Color){0,0,0, 100});

	//mark selection
	if(editor->polysSelected){
		for(int i=0; i < editor->polysSelected; i++){
			int polyId = editor->polySelection[i];
			LevelPoly_t* poly = &game.level.polygons[polyId];
			if(deletePressed){
				LV_RemovePoly(&game.level, polyId);
			}else{
				Rectangle bounds = GetBounds(&poly->tform);
				DrawRectangleLines(bounds.x - 5, bounds.y - 5, bounds.width + 10, bounds.height + 10, (Color){60, 20, 60, 200});
			}
		}
		if(deletePressed){
			editor->polysSelected = 0;
			editor->selectionTarget = LVE_SEL_NONE;
		}
	}
	
	//draw triggers
	for(int triggerId=0; triggerId < game.level.triggerCount; triggerId++){
		LevelTrigger_t* trigger = &game.level.triggers[triggerId];
		DrawRectangleLinesEx(trigger->bounds, 1, GREEN);
	}

	if(!editor->isHoveringOverWindow){
	//!!! tools !!!
		switch(editor->tool){
			case LVE_TOOL_SELECT: {
				if(lmbDown){
					DrawRectangleLinesEx(selection, 2, (Color){100, 100, 100, 100});

				}
				
				if(lmbUp){
					bool singleClick = (!(selection.width || selection.height));
					bool additive = IsKeyDown(KEY_LEFT_SHIFT);
					editor->selectionTarget = LVE_SEL_NONE;
					if(!additive) editor->polysSelected = 0;
					LevelLayer_t* selectedLayer = &game.level.layers[editor->layerSelectedId];
					int polyCount = selectedLayer->polyTexturedCount + selectedLayer->polyUntexturedCount;
					for(int i=0; i < polyCount; i++){
						int polyId = selectedLayer->polyStartId + polyCount - 1 - i;
						LevelPoly_t* poly = &game.level.polygons[polyId];
						if(additive){
							bool shouldSkip = false;
							for(int i2=0; i2 < editor->polysSelected; i2++){
								if(editor->polySelection[i2] == polyId){
									shouldSkip = true;
									break;
								}
							}
							if(shouldSkip) continue;
						}
						if(CheckCollisionRecs(selection, GetBounds(&poly->tform))){
							editor->polySelection[editor->polysSelected++] = polyId;
							editor->selectionTarget = LVE_SEL_POLY;
							if(singleClick) break;
						}
					}
				}

			} break;
			case LVE_TOOL_VERTEX: {
				
				if(editor->vertToolDragging){
					if(lmbDown){
						LevelPoly_t* poly = &game.level.polygons[editor->vertToolPolyId];
						Vector2 newPos = cursor;
						Vector2 polyPos = GetPos(&poly->tform);
						poly->verts[editor->vertToolVertexId] = newPos;
						if((newPos.x < polyPos.x) || (newPos.y < polyPos.y)){
							SetTransformStatic(&poly->tform, newPos, 0);
						}
					}else{
						//LV_UpdatePolyBounds(&game.level.polygons[editor->vertToolPolyId]);
						LV_InitPoly(&game.level, editor->vertToolPolyId);
						editor->vertToolDragging = false;
					}
				}else{
					Rectangle selZone = (Rectangle){cursor.x - 5, cursor.y - 5, 10, 10};
					LevelLayer_t* selectedLayer = &game.level.layers[editor->layerSelectedId];
					int polyCount = selectedLayer->polyTexturedCount + selectedLayer->polyUntexturedCount;
					for(int i=0; i < polyCount; i++){
						int polyId = selectedLayer->polyStartId + polyCount - 1 - i;
						LevelPoly_t* poly = &game.level.polygons[polyId];
						if(!CheckCollisionRecs(GetBounds(&poly->tform), selZone)) continue;
						for(int vertexId=0; vertexId < poly->vertCount; vertexId++){
							Vector2 vertPos = poly->verts[vertexId];
							Rectangle vertBox = {vertPos.x - 5, vertPos.y - 5, 10, 10};
							if(CheckCollisionRecs(vertBox, selZone)){
								DrawRectangleRec(vertBox, RED);
								if(lmbPressed){
									editor->vertToolDragging = 1;
									editor->vertToolPolyId = polyId;
									editor->vertToolVertexId = vertexId;
								}else if(rmbPressed){
									bool allowDeletion = (poly->vertCount > 3);
									if(allowDeletion){
										for(int i2=0; i2 < (poly->vertCount - 1 - vertexId); i2++){
											int shiftingVertId = vertexId + i2;
											poly->verts[shiftingVertId] = poly->verts[shiftingVertId+1];
											poly->uvCoords[shiftingVertId] = poly->uvCoords[shiftingVertId+1];
										}
										poly->vertCount--;
									}
								}
							}else{
								DrawRectangleRec(vertBox, WHITE);
								DrawRectangleLinesEx(vertBox, 1, BLACK);
								bool allowInsert = (!editor->vertToolDragging)
												 	&& (poly->vertCount > 1)
												 	&& ((poly->vertCount+1) < LV_POLY_MAX_VERTICES);
								if(allowInsert){
									int prevVertexId = vertexId ? vertexId-1 : poly->vertCount-1;
									Vector2 prevVertPos = poly->verts[prevVertexId];
									float d = DistToSegment(cursor, prevVertPos, vertPos);
									if(d < 5.0f){
										if(lmbPressed){
											float d1 = Vector2Distance(cursor, vertPos);
											float d2 = Vector2Distance(cursor, prevVertPos);
											float ratio = (d2 != 0) ? (d1 / d2) : 0.000001f;
											//shift right
											for(int i2=0; i2 < (poly->vertCount - 1 - prevVertexId); i2++){
												int shiftingVertId = poly->vertCount - i2;
												poly->verts[shiftingVertId] = poly->verts[shiftingVertId - 1];
												poly->uvCoords[shiftingVertId] = poly->verts[shiftingVertId - 1];
											}
											//insert vertex
											poly->vertCount++;
											poly->verts[vertexId] = cursor;
											poly->uvCoords[vertexId] = Vector2Lerp( poly->uvCoords[vertexId],
																					poly->uvCoords[prevVertexId],
																					ratio);
											//finalize
											LV_InitPoly(&game.level, polyId);
											editor->vertToolDragging = 1;
											editor->vertToolPolyId = polyId;
											editor->vertToolVertexId = vertexId;
										}
										DrawLineEx(vertPos, prevVertPos, 4, GRAY);
									}
								}
							}
						}
					}
				}

			} break;
			case LVE_TOOL_RECT: {
				if(game.level.polygonCount+1 >= LV_MAX_POLYGONS) break;
				if(lmbDown){
					DrawRectangleLinesEx(selection, 2, (Color){100, 0, 0 , 100});
				}
				if(lmbUp){
					int newPolyId = LV_InsertPoly(&game.level, editor->layerSelectedId, false);
					LevelPoly_t* newPoly = &game.level.polygons[newPolyId];
					memset(newPoly, 0, sizeof(LevelPoly_t));
					newPoly->borderColor = RED;

					newPoly->verts[0] = (Vector2){selection.x, selection.y};
					newPoly->verts[1] = (Vector2){selection.x + selection.width, selection.y};
					newPoly->verts[2] = (Vector2){selection.x + selection.width, selection.y + selection.height};
					newPoly->verts[3] = (Vector2){selection.x, selection.y + selection.height};
					newPoly->vertCount = 4;
					LV_InitPoly(&game.level, newPolyId);

					editor->polysSelected = 1;
					editor->polySelection[0] = newPolyId;
					editor->selectionTarget = LVE_SEL_POLY;
				}
			} break;
		}
	}

	EndMode2D();
	
	if(IsMouseButtonDown(MOUSE_RIGHT_BUTTON)){
		app.cam.target = Vector2Subtract(app.cam.target, Vector2Scale(mouseDelta, 1 / app.cam.zoom));
	}
	//app.cam.zoom *= 1 + 0.05f * (float)GetMouseWheelMove();
	app.cam.zoom *= 1 + 0.025f * (float)GetMouseWheelMove();

	if(!editor->areWeRenamingSelectedLayer){
		if(IsKeyPressed(KEY_Q)){
			editor->tool = LVE_TOOL_SELECT;
		}else if(IsKeyPressed(KEY_R)){
			editor->tool = LVE_TOOL_RECT;
		}
	}



	if(editor->bottomTooltip_p){
		DrawText(editor->bottomTooltip_p, 20, app.height - 40, 18, DARKBLUE);
	}

	if(IsKeyPressed(KEY_ESCAPE)) app.screen = SCREEN_MAINMENU;
	editor->mousePosLast = mouseScreenPos;
}
