#include "graphics.h"
#include "game.h"
#include "rlgl.h"
#include "raymath.h"
#include "rlgl.h"
#include "math.h"

void DrawFigure(const Figure* figure, int frameId, Vector2 pos){
	const FigureFrame* frame = figure->frames[frameId];
	if(frame == NULL) return;
	Texture2D* texture = &app.assets.tex1;
	Vector2 offset = Vector2Subtract(pos, frame->center);
	if(rlCheckBufferLimit(frame->triCount*3)) rlglDraw();
	
	rlBegin(RL_TRIANGLES);
	rlEnableTexture(texture->id);
		rlColor4ub(255, 255, 255, 255);
		//rlNormal3f(0, 0, 1);
		for(int i=frame->triCount*3-1; i >= 0; i--){
			Vector2 vert = figure->verts[frame->indices[i]];
			rlTexCoord2f(vert.x / texture->width, vert.y / texture->height);
			rlVertex2f(vert.x + offset.x, vert.y + offset.y);
		}
	rlEnd();
	rlDisableTexture();
}

void DrawFigureEx(const Figure* figure, int frameId, Vector2 pos, float angle, Color tint){
	const FigureFrame* frame = figure->frames[frameId];
	if(frame == NULL) return;
	Texture2D* texture = &app.assets.tex1;
	//Vector2 offset = Vector2Subtract(pos, frame->center);
	if(rlCheckBufferLimit(frame->triCount*3)) rlglDraw();
	float orient[] = {cosf(angle), sinf(angle)};
	rlBegin(RL_TRIANGLES);
	rlEnableTexture(texture->id);
		rlColor4ub(tint.r, tint.g, tint.b, tint.a);
		//rlColor3f(2.0f,1.0f,1.0f);
		//rlNormal3f(0, 0, 1);
		for(int i=frame->triCount*3-1; i >= 0; i--){
			Vector2 src = figure->verts[frame->indices[i]];
			Vector2 centered = Vector2Subtract(src, frame->center);
			Vector2 rotated = {(centered.x * orient[0]) - (centered.y * orient[1]),
							   (centered.x * orient[1]) + (centered.y * orient[0])};
			//Vector2 rotated = centered;
			rlTexCoord2f(src.x / texture->width, src.y / texture->height);
			rlVertex2f(rotated.x + pos.x, rotated.y + pos.y);
		}
	rlEnd();
	rlDisableTexture();
}

void DrawFigureCustom(const Figure* figure, int frameId, Vector2 pos, float angle, Color tint){
	const FigureFrame* frame = figure->frames[frameId];
	if(frame == NULL) return;
	Texture2D* texture = &app.assets.tex1;
	if(rlCheckBufferLimit(frame->triCount*3)) rlglDraw();
	float orient[] = {cosf(angle), sinf(angle)};
	rlBegin(RL_TRIANGLES);
	rlEnableTexture(texture->id);
		//rlColor4ub(tint.r, tint.g, tint.b, tint.a);
		for(int i=frame->triCount*3-1; i >= 0; i--){
			Vector2 src = figure->verts[frame->indices[i]];
			Vector2 centered = Vector2Subtract(src, frame->center);
			Vector2 rotated = {(centered.x * orient[0]) - (centered.y * orient[1]),
							   (centered.x * orient[1]) + (centered.y * orient[0])};
			rlColor4ub(tint.r, tint.g, tint.b, tint.a);
			rlTexCoord2f(0.5f + src.x / texture->width, src.y / texture->height);
			rlVertex2f(rotated.x + pos.x, rotated.y + pos.y);
		}
		rlColor4ub(255, 255, 255, 255);
		for(int i=frame->triCount*3-1; i >= 0; i--){
			Vector2 src = figure->verts[frame->indices[i]];
			Vector2 centered = Vector2Subtract(src, frame->center);
			Vector2 rotated = {(centered.x * orient[0]) - (centered.y * orient[1]),
							   (centered.x * orient[1]) + (centered.y * orient[0])};
			rlTexCoord2f(src.x / texture->width, src.y / texture->height);
			rlVertex2f(rotated.x + pos.x, rotated.y + pos.y);
		}
	rlEnd();
	rlDisableTexture();

	//Vector2 maskOffset = {pos.x + (texture->width >> 1), pos.y};
	//DrawFigureEx(figure, frameId, maskOffset, angle, tint);
	//DrawFigureEx(figure, frameId, pos, angle, WHITE);
}


void DrawPolyObjectStroke(int vertCount, Vector2* verts, Color strokeColor){
	for(int vertId = 0; vertId < vertCount; vertId++){
		DrawLineEx(verts[vertId], verts[(vertId + 1) % vertCount], 4.0f, strokeColor);
		DrawPoly(verts[(vertId + 1) % vertCount], 3, 2.0f, (float)PI*2+Vector2Angle(verts[vertId], verts[(vertId + 2) % vertCount]), strokeColor);
	}
}

void DrawPolyObjectFill(int triangleCount, const int* triangleIndices, Vector2* verts, Color fillColor){
	
	if(triangleCount < 1) return;
	int indexCount = (triangleCount * 3);
	if(rlCheckBufferLimit(indexCount)) rlglDraw();
	rlBegin(RL_TRIANGLES);
	//rlDisableBackfaceCulling();
		rlColor4ub(fillColor.r, fillColor.g, fillColor.b, fillColor.a);
		for(int i = 0; i < indexCount; i += 3){
			rlVertex2f(verts[triangleIndices[i+2]].x, verts[triangleIndices[i+2]].y);
			rlVertex2f(verts[triangleIndices[i+1]].x, verts[triangleIndices[i+1]].y);
			rlVertex2f(verts[triangleIndices[i  ]].x, verts[triangleIndices[i  ]].y);
		}
	rlEnd();
	
}

void BeginTransformManual(Vector2 pos, float angle, float scale) {
	rlPushMatrix();
	rlTranslatef(pos.x, pos.y, 0.0f);
	rlRotatef(angle * RAD2DEG, 0.0f, 0.0f, 1.0f);
	if(scale != 1.0f) rlScalef(scale, scale, 1.0f);
}

void BeginTransform(Transform_t* tform){
	BeginTransformManual(GetInterpolatedPos(tform), GetInterpolatedAngle(tform), 1);
}

void EndTransform(){
	rlPopMatrix();
}
