#pragma once

#define SERVER_LIST_MAX_LENGTH 256

//#include "networking.h"
#include "masterserverpackets.h"
#include "stdint.h"
#include "stdbool.h"

#define MASTER_HOST "cokesoft.chickenkiller.com"
#define MASTER_PORT 56999

typedef struct{
	bool isSynchronized;
	bool isCompatible;
	double lastPinged;
	double lastReceived;
	uint32_t ip[4];
	uint16_t port;
	unsigned int ping;
	int version;
	int playersOn;
	int playersMax;
	int gameType;
	char title[64];
} ServerInfo_t;

void InitializeMasterServerConnection();
void UpdateMasterServerConnection();
void RefreshServerList();
void GetListOfServers(int* count, ServerInfo_t** list);
void PingServer(ServerInfo_t* server);
bool ProcessInfoRequest(InfoRequestMessage_t* msg);
