#include "projectiles.h"
#include "game.h"
#include "raymath.h"
#include "stdio.h"
#include "stdbool.h"

void RemoveProjectile(int projectileId){
	ProjectileSystemState_t* state = &game.projectileSystemState;
	if(state->projectileMaxActiveId == projectileId){
		state->projectileMaxActiveId = 0;
		for(int i = projectileId - 1; i >= 0; i--){
			if(state->projectiles[i].type != PROJ_INACTIVE){
				state->projectileMaxActiveId = i;
				break;
			}
		}
	}
	if(state->projectileNextMinFreeId > projectileId) state->projectileNextMinFreeId = projectileId;
	state->projectiles[projectileId].type = PROJ_INACTIVE;
}

void AddedProjectileAt(int projectileId){
	ProjectileSystemState_t* state = &game.projectileSystemState;
	if(state->projectileMaxActiveId    < projectileId) state->projectileMaxActiveId = projectileId;
	if(state->projectileNextMinFreeId == projectileId) state->projectileNextMinFreeId++;
}

void SpawnProjectile(ProjectileType_e type, int playerId, int itemId, Vector2 pos, Vector2 orientation){
	ProjectileSystemState_t* state = &game.projectileSystemState;
	int iterateTo = (state->projectileMaxActiveId+1);
	if(iterateTo >= (MAX_PROJECTILES - 2)) iterateTo = (MAX_PROJECTILES - 2);
	for(int projectileId = state->projectileNextMinFreeId; projectileId <= iterateTo; projectileId++){
		Projectile_t* projectile = &state->projectiles[projectileId];
		if(projectile->type == PROJ_INACTIVE){
			AddedProjectileAt(projectileId);
			projectile->type = type;
			projectile->timer = 60;
			projectile->playerId = playerId;
			if(itemId != -1){
				Item_t* item = &game.items[itemId];
				projectile->itemTypeId = item->type;
			}else{
				projectile->itemTypeId = -1;
			}
			projectile->flags = 0;
			projectile->pos = pos;
			projectile->velocity = Vector2Scale(Vector2Normalize(orientation), 40.0f);
			//projectile->posLast = pos;
			for(int i = 0; i < PROJECTILE_TAIL_ELEMENTS; i++){
				projectile->tail[i] = pos;
			}
			//printf("spawned at: %f %f\n", projectile->pos.x, projectile->pos.y);
			return;
		}
	}

}

void UpdateProjectiles(){
	ProjectileSystemState_t* state = &game.projectileSystemState;
	for(int projectileId = 0; projectileId < (state->projectileMaxActiveId+1); projectileId++){
		Projectile_t* projectile = &state->projectiles[projectileId];
		if(projectile->type == PROJ_INACTIVE) continue;
		if(!(projectile->flags & PROJFLAG_LANDED)){
			//projectile->posLast = projectile->pos;
			Vector2 posLast = projectile->pos;
			projectile->pos = Vector2Add(projectile->pos, projectile->velocity);
			//printf("updating at: %f %f\n", projectile->pos.x, projectile->pos.y);
			cpSegmentQueryInfo queryInfo;
			if(cpSpaceSegmentQueryFirst(&game.space, cpv(posLast.x, posLast.y), cpv(projectile->pos.x, projectile->pos.y), 1.0f, CP_SHAPE_FILTER_ALL, &queryInfo)){
				Vector2 normal = (Vector2){queryInfo.normal.x, queryInfo.normal.y};
				Vector2 point = (Vector2){queryInfo.point.x, queryInfo.point.y};
				float dot = Vector2DotProduct(normal, Vector2Normalize(projectile->velocity));
				bool landed = (dot > 0.25f);
				projectile->reflectionTimer = 6;
				projectile->velocity = Vector2Subtract(projectile->velocity, Vector2Scale(Vector2Multiply(projectile->velocity, normal), 2.0f) );

				ShapeUserData_t* shapeUserData = cpShapeGetUserData(queryInfo.shape);
				if(shapeUserData != NULL){
					switch(shapeUserData->type){
						case SHAPE_CHARACTER: {
							Character_t* character = &game.characters[shapeUserData->id];
							character->healthLastDamageSource = DEATH_BY_ITEM;
							character->healthLastDamagePlayerId = projectile->playerId;
							character->healthLastDamageItemType = projectile->itemTypeId;
							character->healthLastDamageByPlayerFrame = game.frame;
							//ApplyDamage(shapeUserData->id, 30, point, normal);
							ApplyDamage(shapeUserData->id, 30, GetPos(&character->tform), normal);
							landed = true;
						} break;
						default:
							//projectile->timer -= 20;
							landed = true;
							break;
					}
				}

				if(landed){
					projectile->pos = point;
					projectile->flags |= PROJFLAG_LANDED;
					projectile->timer = 30;
					continue;
				}else{
					//projectile->pos = Vector2Add(point, projectile->velocity);
					projectile->pos = point;
				}
			}

			projectile->velocity = Vector2Scale(projectile->velocity, 0.999f);
			projectile->velocity.y += 0.01f;
		}
		for(int i = 0; i < PROJECTILE_TAIL_ELEMENTS; i++){
			Vector2 start = (!i) ? projectile->pos : projectile->tail[i-1];
			projectile->tail[i] = Vector2Lerp(projectile->tail[i], start, 0.2f);
			if(projectile->reflectionTimer){
				projectile->reflectionTimer--;
				break;
			}
		}
		projectile->timer--;
		if(projectile->timer <= 0){
			RemoveProjectile(projectileId);
		}
	}
}

void DrawProjectiles(){
	ProjectileSystemState_t* state = &game.projectileSystemState;
	for(int projectileId = 0; projectileId < (state->projectileMaxActiveId+1); projectileId++) {
		Projectile_t *projectile = &state->projectiles[projectileId];
		if(projectile->type == PROJ_INACTIVE) continue;
		if((!CheckCollisionPointRec(projectile->pos, app.viewBounds)) && (!CheckCollisionPointRec(projectile->tail[PROJECTILE_TAIL_ELEMENTS-1], app.viewBounds))){
			continue;
		}
		switch(projectile->type){
			case PROJ_LASER:
				//char opacity = (projectile->timer < 64) ? (char)(192 - 2 * projectile->timer) : 192;
				//DrawLineEx(Vector2Subtract(projectile->posLast, Vector2Scale(projectile->velocity, 20.0f)), projectile->pos, 7.0f, (Color){100, 253, 112, opacity});
				for(int i=0; i < PROJECTILE_TAIL_ELEMENTS; i++){
					Vector2 start = (!i) ? projectile->pos : projectile->tail[i-1];
					Vector2 end = projectile->tail[i];
					Color laserColor = (Color){100, 253, 112, 255};
					DrawLineEx(start, end, 12.0f, (Color){170, 253, 152, 60});
					DrawLineEx(start, end, 7.0f, laserColor);
					DrawPoly(start, 5, 3.5f, 0.0f, laserColor);
					DrawPoly(end, 5, 3.5f, 0.0f, laserColor);
					DrawLineEx(start, end, 3.0f, (Color){200, 255, 182, 255});
				}
				break;
		}
	}
}
