#pragma once
#include "items.h"
#include "raylib.h"
#include "chipmunk/chipmunk.h"

#define MAX_PROJECTILES 1024
#define PROJECTILE_TAIL_ELEMENTS 2

typedef enum{
	PROJ_INACTIVE = 0,
	PROJ_LASER
} ProjectileType_e;

typedef enum{
	PROJFLAG_LANDED = 1
} ProjectileFlags_e;

typedef struct{
	ProjectileType_e type;
	int timer;
	int playerId;
	ItemType_e itemTypeId;
	int flags;
	Vector2 pos;
	Vector2 velocity;
	//Vector2 posLast;
	Vector2 tail[PROJECTILE_TAIL_ELEMENTS];
	int reflectionTimer;
} Projectile_t;

typedef struct{
	int projectileMaxActiveId, projectileNextMinFreeId;
	Projectile_t projectiles[MAX_PROJECTILES];
} ProjectileSystemState_t;

void SpawnProjectile(ProjectileType_e type, int playerId, int itemId, Vector2 pos, Vector2 orientation);
void UpdateProjectiles();
void DrawProjectiles();
void RemoveProjectile(int projectileId);
void AddedProjectileAt(int projectileId);
