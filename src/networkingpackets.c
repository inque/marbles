#include "networkingpackets.h"
#include "stdio.h"
#define _USE_MATH_DEFINES
#include "math.h"

#ifdef _MSC_VER
#define isnanf isnan
#endif

void PackCustomizations(PackedCustomizations_t* dst, Customizations_t* src){
	dst->skinColor = src->skinColor;
	dst->hairType = src->hairType;
}

void UnpackCustomizations(Customizations_t* dst, PackedCustomizations_t* src){
	dst->skinColor = src->skinColor;
	dst->hairType = src->hairType;
}

void PackTform(PackedTransform_t* dst, Transform_t* src){
	Vector2 pos = GetPos(src);
	dst->pos[0] = pos.x;
	dst->pos[1] = pos.y;
	dst->angle = GetAngle(src);
	//double srcAngle = fmod(GetAngle(src) / (2*M_PI), 1);
	//dst->angle = (uint16_t)(UINT16_MAX * srcAngle);
}

void UnpackTform(Transform_t* dst, PackedTransform_t* src){
	Vector2 pos = (Vector2){src->pos[0], src->pos[1]};
	SetTransformData(dst, pos, src->angle);
	//double srcAngle = ((double)src->angle / UINT16_MAX) * (2*M_PI);
	//SetTransformData(dst, pos, src->angle);
}

void PackControls(PackedPlayerControls_t* dst, PlayerControls_t* src){
	dst->buttons = src->buttons;
	dst->axis_h = src->axis_h;
	dst->axis_v = src->axis_v;
	dst->axis_r = src->axis_r;
	for(int i=0; i < PLAYER_MAX_INPUT_ACTIONS; i++){
		if(src->inputActionQueue[i].type != ACTION_NONE){
			dst->actions[i].type = src->inputActionQueue[i].type;
			dst->actions[i].parameter = src->inputActionQueue[i].parameter;
			src->inputActionQueue[i].type = ACTION_NONE;
			//printf("action %i packed\n", dst->actions[i].type);
		}else{
			dst->actions[i].type = ACTION_NONE;
		}
	}
}

void UnpackControls(PlayerControls_t* dst, PackedPlayerControls_t* src){
	dst->buttonsLast = dst->buttons;
	dst->buttons = src->buttons;
	dst->axis_h = src->axis_h;
	dst->axis_v = src->axis_v;
	dst->axis_r = src->axis_r;
	for(int i=0; i < PLAYER_MAX_INPUT_ACTIONS; i++){
		if( (src->actions[i].type != ACTION_NONE) && (dst->inputActionQueue[i].type == ACTION_NONE) ){
			dst->inputActionQueue[i].type = src->actions[i].type;
			dst->inputActionQueue[i].parameter = src->actions[i].parameter;
			//printf("action %i - %i unpacked\n", dst->inputActionQueue[i].type, dst->inputActionQueue[i].parameter);
		}
	}
	if(isnanf(dst->axis_h)) dst->axis_h = 0;
	if(isnanf(dst->axis_v)) dst->axis_v = 0;
	if(isnanf(dst->axis_r)) dst->axis_r = 0;
	
}

void PackPlayerStats(PackedPlayerStats* dst, PlayerStats_t* src){
	dst->checkpointId = src->checkpointId;
	dst->killCount = src->killCount;
	dst->deathCount = src->deathCount;
}

void UnpackPlayerStats(PlayerStats_t* dst, PackedPlayerStats* src){
	dst->checkpointId = (src->checkpointId == 0xFF) ? -1 : src->checkpointId;
	dst->killCount = src->killCount;
	dst->deathCount = src->deathCount;
}

void PackProjectile(FrameResponseProjectileInfo_t* dst, Projectile_t* src){
	dst->type = src->type;
	if(src->type != PROJ_INACTIVE){
		dst->pos[0] = src->pos.x;
		dst->pos[1] = src->pos.y;
		for(int i=0; i < PROJECTILE_TAIL_ELEMENTS; i++){
			dst->tail[i][0] = src->tail[i].x;
			dst->tail[i][1] = src->tail[i].y;
		}
	}
}

void UnpackProjectile(Projectile_t* dst, FrameResponseProjectileInfo_t* src){
	dst->type = src->type;
	dst->pos = (Vector2){src->pos[0], src->pos[1]};
	for(int i=0; i < PROJECTILE_TAIL_ELEMENTS; i++){
		dst->tail[i].x = src->tail[i][0];
		dst->tail[i].y = src->tail[i][1];
	}
}
