#include "particles.h"
#include "game.h"
#include "networking.h"
#include "networkingpackets.h"
#include "raymath.h"
#include "stdlib.h"
#include "stdio.h"

static SimpleParticle_t* AddSimpleParticle(ParticleSystemState_t* state, SimpleParticleType_e type, Vector2 pos, float randomPosRadius, Vector2 minVelocity, Vector2 maxVelocity){
	int iterateTo = (state->simpleMaxActiveId+1);
	if(iterateTo >= (MAX_SIMPLE_PARTICLES - 1)) iterateTo = (MAX_SIMPLE_PARTICLES - 1);
	SimpleParticle_t* particle = &state->simpleParticles[0];
	for(int particleId = state->simpleNextMinFreeId; particleId <= iterateTo; particleId++){
		particle = &state->simpleParticles[particleId];
		if(particle->type == PARTICLE_S_INACTIVE){
			if(state->simpleMaxActiveId    < particleId) state->simpleMaxActiveId = particleId;
			if(state->simpleNextMinFreeId == particleId) state->simpleNextMinFreeId++;
			particle->type = type;
			particle->creationFrame = game.frame;
			if(randomPosRadius){
				particle->pos.x = pos.x + randomPosRadius * (0.5f - randf());
				particle->pos.y = pos.y + randomPosRadius * (0.5f - randf());
				particle->velocity.x = minVelocity.x + (maxVelocity.x - minVelocity.x) * randf();
				particle->velocity.y = minVelocity.y + (maxVelocity.y - minVelocity.y) * randf();
			}else{
				particle->pos = pos;
				particle->velocity.x = 0.0f;
				particle->velocity.y = 0.0f;
			}
			break;
		}
	}
	return particle;
}

static void RemoveSimpleParticle(ParticleSystemState_t* state, int particleId){
	if(state->simpleMaxActiveId == particleId){
		state->simpleMaxActiveId = 0;
		for(int i = particleId - 1; i >= 0; i--) {
			if(state->simpleParticles[i].type != PARTICLE_S_INACTIVE){
				state->simpleMaxActiveId = i;
				break;
			}
		}
	}
	if(state->simpleNextMinFreeId > particleId) state->simpleNextMinFreeId = particleId;
	state->simpleParticles[particleId].type = PARTICLE_S_INACTIVE;
}

void SpawnParticles(ParticleEffectType_e type, int amount, Vector2 pos){
	ParticleSystemState_t* state = &game.particleSystemState;
	SimpleParticle_t* particle;
	switch(type){
		case EFFECT_SPAWN_POOF: {
			for(int i=0; i < amount; i++){
				AddSimpleParticle(state, PARTICLE_S_SPARK, pos, 60.0f, (Vector2){-10, -8}, (Vector2){10, 2} );
			}
			for(int i=0; i < 3+amount/3; i++){
				particle = AddSimpleParticle(state, PARTICLE_S_FLASH, pos, 35.0f, Vector2Zero(), Vector2Zero());
				particle->extra_f = 0.5f + randf();
			}
		} break;
		case EFFECT_BLOOD_SPLASH: {
			for(int i = 0; i < amount; i++){
				AddSimpleParticle(state, PARTICLE_S_BLOOD, pos, 4.0f, (Vector2){-10, -8}, (Vector2){10, 2});
			}
		} break;
	}

	if(app.role == ROLE_HOST){
		ResponseMessage_t* msg = GetResponseBuffer();
		msg->code = RESPONSE_PARTICLES;
		msg->particles.type = type;
		msg->particles.amount = amount;
		msg->particles.pos[0] = pos.x;
		msg->particles.pos[1] = pos.y;
		BroadcastResponse(CHAN_STATUS, 1+sizeof(struct ResponseParticles_st));
	}
}

void UpdateParticles(){
	ParticleSystemState_t* state = &game.particleSystemState;
	for(int simpleParticleId = 0; simpleParticleId <= state->simpleMaxActiveId; simpleParticleId++) {
		SimpleParticle_t *particle = &state->simpleParticles[simpleParticleId];
		if(particle->type == PARTICLE_S_INACTIVE) continue;
		particle->pos = Vector2Add(particle->pos, particle->velocity);
		int lifespan = 60;
		int elapsed = (game.frame - particle->creationFrame);
		switch(particle->type){
			case PARTICLE_S_SPARK: {
				lifespan = 30 + (simpleParticleId % 6) * 5;
				particle->velocity.x *= 0.980f;
				particle->velocity.y = particle->velocity.y*0.995f + 0.3f;
			} break;
			case PARTICLE_S_FLASH: {
				lifespan = 30;
			} break;
			case PARTICLE_S_BLOOD: {
				lifespan = 20 + (simpleParticleId % 6) * 2;
				particle->velocity.x *= 0.940f;
				particle->velocity.y = particle->velocity.y*0.995f + 0.3f;
			}
		}
		if(elapsed > lifespan){
			RemoveSimpleParticle(state, simpleParticleId);
		}
	}
}

void DrawParticles(){
	ParticleSystemState_t* state = &game.particleSystemState;
	for(int simpleParticleId = 0; simpleParticleId <= state->simpleMaxActiveId; simpleParticleId++){
		SimpleParticle_t* particle = &state->simpleParticles[simpleParticleId];
		if(particle->type == PARTICLE_S_INACTIVE) continue;
		if(!CheckCollisionPointRec(particle->pos, app.viewBounds));
		int elapsed = (game.frame - particle->creationFrame);
		switch(particle->type){
			case PARTICLE_S_INACTIVE:
				continue;
			case PARTICLE_S_SPARK: {
				//DrawRectangleV(particle->pos, (Vector2){10, 10}, (Color){240,210,50,200} );
				Vector2 endPoint = Vector2Subtract(particle->pos, Vector2Scale(particle->velocity, 3.0f));
				DrawLineEx(particle->pos, endPoint, 3.0f, (Color) {247, 223, 206, 150});
			} break;
			case PARTICLE_S_FLASH: {
				float opacity = fminf(1.0f, (float)elapsed / 8);
				if(elapsed > 10) opacity -= fminf(1.0f, ((float)elapsed - 10) / 15);
								 opacity *= 0.2f;
				Color flashColor = (Color){244, 251, 132, (char)(opacity*255)};
				//BeginBlendMode(BLEND_ADDITIVE);
				DrawPoly(particle->pos, 5, particle->extra_f * (10.0f + (float)elapsed * 5.0f), particle->extra_f * 60.0f + (float)elapsed * 4.0f, flashColor);
				//EndBlendMode();
			} break;
			case PARTICLE_S_BLOOD: {
				Vector2 endPoint = Vector2Subtract(particle->pos, Vector2Scale(particle->velocity, 3.5f));
				DrawLineEx(particle->pos, endPoint, 3.0f, (Color) {247, 40, 43, 190});
			}
		}
	}
}
