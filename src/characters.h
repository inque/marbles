#pragma once

#include "players.h"
#include "transforms.h"

#define CHARACTER_CORPSE_DESPAWN_TIME 8*60

#define CHAR_MAX_BODIES 8
#define CHAR_MAX_SHAPES 8
#define CHAR_MAX_CONSTRAINTS 8
#define CHAR_MAX_INVENTORY_SLOTS 4
#define CHAR_MAX_ACTIONS_QUEUED 8

typedef enum{
	CHAR_BALL,
	CHAR_CUBE
} CharacterType_e;

typedef struct{
	char name[32];
	float angleMultiplier;
	float itemReachDistance;
	Color skinColorStroke[CUSTOM_SKIN_VARIATIONS];
	Color skinColorFill[CUSTOM_SKIN_VARIATIONS];
} CharacterType_t;

typedef struct{
	PlayerAction_t input;
	int preTimer;
	int postTimer;
} CharacterAction_t;

typedef enum{
	DEATH_BY_UNKNOWN,
	DEATH_BY_TRIGGER,
	DEATH_BY_ITEM
} CharacterDamageSource_e;

typedef struct{
	int active;
	int playerId;
	CharacterType_e type;
	Customizations_t customizations;
	int despawnTimer;
	int livingFrames;
	int health;
	int healthLastDamageFrame;
	int healthLastDamageByPlayerFrame;
	CharacterDamageSource_e healthLastDamageSource;
	int healthLastDamagePlayerId;
	int healthLastDamageItemType;
	int healthAnimatedValue;
	Transform_t tform;
	cpBody* bodies[CHAR_MAX_BODIES];
	cpShape* shapes[CHAR_MAX_SHAPES];
	ShapeUserData_t shapeUserData;
	cpConstraint* constraints[CHAR_MAX_CONSTRAINTS];
	PlayerControls_t controls;
	CharacterAction_t actionQueue[CHAR_MAX_ACTIONS_QUEUED];
	int actionCurrent;
	int actionNext;
	int lookingAtItemId;
	int inventoryItemIds[CHAR_MAX_INVENTORY_SLOTS];
	int inventorySecondarySelectionSlot;
	float primaryItemAngle;
	float primaryItemAngleAcceleration;
	float externalTorque;
	//type-specific properties:
	union{
		struct CharProps_Ball_st{
			float radius;
			float speedMultiplier;
			float motorSpeed;
			float motorAccel;
			int motorGear;
			float motorCursor;
		} ballProps;
		struct CharProps_Cube_st{
			float size;
			int eyeAnimationFrame;
		} cubeProps;
	};
} Character_t;

void DefineCharacters();
void InitializeCharacterData(int characterId);
int SpawnCharacter(int playerId, CharacterType_e type, Vector2 pos);
void SynchronizeCharacter(int characterId);
void RemoveCharacter(int characterId);
int GiveItem(int characterId, ItemType_e type);
int FindSlot(int characterId, int itemId);
void ApplyDamage(int characterId, int amount, Vector2 pos, Vector2 normal);
cpBool CallbackPresolveCharacter(cpArbiter *arb, cpSpace *space, void *ignore);
void CallbackPostsolveCharacter(cpArbiter *arb, cpSpace *space, void *ignore);
void UpdateCharacters();
void DrawCharacters();
