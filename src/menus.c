#include "menus.h"
#include "game.h"
#include "leveleditor.h"
#include "networking.h"
#include "masterserver.h"
#include "raylib.h"
#include "stdlib.h"
#define RAYGUI_IMPLEMENTATION
#include "raygui.h"

void DrawMenuBackground(float t){
	Color bgColor = (Color){16, 16, 32, 255};

	if(t == 1.0f){
		//ClearBackground(bgColor);
		DrawRectangle(0, 0, app.width, app.height, Fade(bgColor, 0.3f));
	}else{
		float swipeX = (float)app.width * (t * t);
		DrawRectanglePro((Rectangle){0, 0, swipeX * 1.5f, app.height + 200}, (Vector2){0, 200}, 1.0f, bgColor);
	}

	if((app.uiFrame >> 4) & 1){
		for(int i = 0; i < (app.uiFrame % 4); i++){
			DrawRectangle((app.width * randf()), (app.height * randf()), 7, 2, (Color){224, 160, 160, 255});
		}
	}

	if( (!(((app.uiFrame+400) >> 7) & 6)) && (!((app.uiFrame) & 37)) ){
		float y = -200 + (app.fineUIFrame / 5);
			  y += ((float)(app.uiFrame & 27) / 6);
			  y += sinf(app.fineUIFrame * 50) * 4;
			  y = fmodf(y, (float)app.height);
		Color lineColor = (Color){255, 255, 230, 16};
		DrawRectangle(0, (int)y, app.width, 4, lineColor);
	}

	DrawRectangleGradientH(0, 0, 800, app.height, (Color){30, 2, 40, (char)(40 * t)}, (Color){4, 20, 30, 0} );

	if(app.uiFrame < 100){
		float blackOut = 1 - (app.fineUIFrame / 100.0f);
			  blackOut = (blackOut * blackOut);
		DrawRectangle(0, 0, app.width, app.height, (Color){0, 0, 0, (char)(blackOut*255)});
	}

}

static bool OptionsMenu(){
	bool pressed;
	Color textColor = (Color){190, 168, 177, 255};

	pressed = (GuiButton((Rectangle){20, 20, 60, 32}, "Return"));
	if(pressed || IsKeyPressed(KEY_ESCAPE)){
		return true;
	}


	return false;
}

void MainMenuScreen(){
	bool pressed;
	Color textColor = (Color){190, 168, 177, 255};
	Vector2 mouse = GetMousePosition();

	switch(app.mainMenuScreen){
		case MAINMENU_HOME: {
			DrawText("Bouncy Marbles", app.halfWidth - 155, app.height / 3 - 70, 48, textColor);
			DrawText("v 0.02b", app.halfWidth + 80, app.height / 3 - 20, 18, textColor);
			DrawText("by inq", 20, app.height - 30, 18, (Color){93, 50, 88, 255} );
			if(app.uiFrame > 160) DrawText("For testing only.", app.halfWidth - 95, app.height - 42, 24, (Color){255, 255, 200, 5});

			int buttonMargin = 2;
			Rectangle buttonDim = (Rectangle){app.halfWidth - 100, app.halfHeight - 20, 200, 32};

			pressed = (GuiButton(buttonDim, "New Game"));
			if(pressed || IsKeyPressed(KEY_N) || IsKeyPressed(KEY_ENTER)){
				StartGame(ROLE_LOCAL, GAME_REGULAR, 0);
				//StartGame(ROLE_LOCAL, GAME_REGULAR, 2);
			}
			buttonDim.y += buttonDim.height + buttonMargin;

			pressed = (GuiButton(buttonDim, "Join a server"));
			if(pressed || IsKeyPressed(KEY_J)){
				app.mainMenuScreen = MAINMENU_SERVERLIST;
				RefreshServerList();
			}
			buttonDim.y += buttonDim.height + buttonMargin;

			pressed = (GuiButton(buttonDim, "Host a server"));
			if(pressed || IsKeyPressed(KEY_H)){
				app.mainMenuScreen = MAINMENU_HOST;
			}
			buttonDim.y += buttonDim.height + buttonMargin;

			pressed = (GuiButton(buttonDim, "Options"));
			if(pressed || IsKeyPressed(KEY_O)){
				app.mainMenuScreen = MAINMENU_OPTIONS;
			}
			buttonDim.y += buttonDim.height + buttonMargin;

			pressed = (GuiButton(buttonDim, "Quit"));
			if(pressed || IsKeyPressed(KEY_Q) || IsKeyPressed(KEY_ESCAPE)){
				app.shouldClose = 1;
			}

			
			buttonDim.x = app.width - 170;
			buttonDim.y += 60;
			buttonDim.width = 100;
			pressed = (GuiButton(buttonDim, "Level editor"));
			if(pressed || IsKeyPressed(KEY_L)){
				StartLevelEditor();
			}

			static bool editingName;
			buttonDim.x = app.halfWidth + app.width / 6;
			buttonDim.y = app.halfHeight;
			buttonDim.width = 180;
			if(GuiTextBox(buttonDim, (char*)&app.settings.playerName, 18, editingName)){
				editingName = !editingName;
				if(!editingName) SaveSettings();
			}
			buttonDim.y -= 25;
			GuiLabel(buttonDim, "Your name:");

		} break;
		case MAINMENU_OPTIONS: {

			if(OptionsMenu()){
				app.mainMenuScreen = MAINMENU_HOME;
			}

		} break;
		case MAINMENU_SERVERLIST: {

			int serverCount;
			ServerInfo_t* serverList;
			GetListOfServers(&serverCount, &serverList);

			if(serverCount){
				Rectangle serverButtonDim = (Rectangle){200, 80, app.width - 400, 30};
				for(int i=0; i < serverCount; i++){
					ServerInfo_t* server = &serverList[i];
					if(server->isSynchronized){
						char text[8192];
						sprintf(text, "%s. [%i/%i] v0.%02i. Ping: %i", (const char*)&server->title, server->playersOn, server->playersMax, server->version, server->ping);
						pressed = GuiButton(serverButtonDim, (const char*)&text);
						if(pressed){
							NetworkHostJoin((uint32_t*)&server->ip, server->port);
						}
						serverButtonDim.y += 40;
					}else{
						if((server->lastPinged == 0) || ((GetTime() - server->lastPinged) > 0.5) ){
							PingServer(server);
						}
					}
				}
			}else{
				DrawText("No servers found.", 210, 90, 20, textColor);
			}

			pressed = (GuiButton((Rectangle){60, 32, 200, 32}, "Return"));
			if(pressed || IsKeyPressed(KEY_ESCAPE)){
				app.mainMenuScreen = MAINMENU_HOME;
			}

			static char directIp[1024] = "::ffff:127.0.0.1";
			static int directPort = 32767;
			static bool editingDirectIp, editingDirectPort;
			GuiLabel((Rectangle){60, app.height - 110, 200, 32}, "Direct connect:");
			if(GuiTextBox((Rectangle){60, app.height - 80, 160, 32}, (char*)&directIp, 64, editingDirectIp)){
				editingDirectIp = !editingDirectIp;
			}
			if(GuiValueBox((Rectangle){60+170, app.height - 80, 50, 32}, "", &directPort, 0, INT16_MAX, editingDirectPort)){
				editingDirectPort = !editingDirectPort;
			}
			if(GuiButton((Rectangle){60+170+60, app.height - 80, 30, 32}, "Go")){
				uint32_t ip[4];
				GetIPv6((const char*)&directIp, (uint32_t*)&ip);
				NetworkHostJoin((uint32_t*)&ip, directPort);
			}

		} break;
		case MAINMENU_HOST: {
			
			int buttonMargin = 2;
			Rectangle buttonDim = (Rectangle){60, app.halfHeight + 120, 200, 32};

			static bool editingTitle, editingPort;

			pressed = (GuiButton(buttonDim, "Start"));
			if(!(editingTitle || editingPort)){
				pressed = pressed || IsKeyPressed(KEY_S) || IsKeyPressed(KEY_ENTER);
			}
			if(pressed){
				app.backgroundTransitionOut = 40;
				NetworkHostCreate();
				StartGame(ROLE_HOST, app.settings.hostGameType, 1);
			}
			buttonDim.y -= buttonDim.height + buttonMargin;

			if(GuiValueBox(buttonDim, "(Port) ", &app.settings.hostPort, 1024, INT16_MAX, editingPort)){
				editingPort = !editingPort;
			}
			buttonDim.y -= buttonDim.height + buttonMargin;

			if(GuiTextBox(buttonDim, (char*)&app.settings.hostTitle, 18, editingTitle)){
				editingTitle = !editingTitle;
			}
			buttonDim.y -= buttonDim.height + buttonMargin;


			buttonDim.y = 32;
			pressed = (GuiButton(buttonDim, "Return"));
			if(pressed || IsKeyPressed(KEY_ESCAPE)){
				app.mainMenuScreen = MAINMENU_HOME;
			}

		} break;
		case MAINMENU_LOADING: {
			DrawText(app.gameInterfaceState.loadingTextHeader, app.halfWidth - MeasureText(app.gameInterfaceState.loadingTextHeader, 30)/2, app.halfHeight - 50, 30, app.gameInterfaceState.loadingFailed ? RED : textColor);
			DrawText(app.gameInterfaceState.loadingTextSub, app.halfWidth - MeasureText(app.gameInterfaceState.loadingTextSub, 18)/2, app.halfHeight + 20, 18, textColor);

			if(app.gameInterfaceState.loadingAbortable && IsKeyPressed(KEY_ESCAPE)){
				app.mainMenuScreen = MAINMENU_HOME;
				CleanUpNetworking();
			}
		} break;
		case MAINMENU_CREDITS: {
			DrawText("That's the end of this demo.", app.halfWidth - 130, app.height / 3 - 20, 24, textColor);
			DrawText("Thank you for playing.", app.halfWidth - 100, app.height / 3 + 20, 24, textColor);

			if (IsKeyPressed(KEY_ESCAPE)) {
				app.shouldClose = 1;
			}

		} break;
	}

}

void InGameEscScreen(){
	bool pressed;

	pressed = GuiButton((Rectangle){app.halfWidth - 100, app.halfHeight - 20, 200, 32}, "Return");
	if(pressed || IsKeyPressed(KEY_R)){
		app.inGameSubMenu = SUBMENU_NONE;
		if(app.role == ROLE_LOCAL){
			app.isPaused = 0;
		}
	}

	pressed = GuiButton((Rectangle){app.halfWidth - 100, app.halfHeight + 14, 200, 32}, "Quit to Menu");
	if(pressed || IsKeyPressed(KEY_Q)){
		CloseGame();
		CleanUpNetworking();
		app.screen = SCREEN_MAINMENU;
		app.mainMenuScreen = MAINMENU_HOME;
	}

}