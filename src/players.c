#include "players.h"
#include "game.h"
#include "networking.h"
#include "networkingpackets.h"
#include "raymath.h"
#include "chipmunk/chipmunk.h"
#include "string.h"
#include "stdio.h"
#define _USE_MATH_DEFINES
#include "math.h"

int CreatePlayer(int isNPC) {
	for(int playerId = 0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = &game.players[playerId];
		if(player->active) continue;

		player->active = 1;
		player->characterId = -1;
		memset(&player->controls, 0, sizeof(PlayerControls_t));
		player->deathFrames = 0;
		player->respawnFrames = 0;
		ResetPlayerStats(playerId);
		player->session.connected = 0;
		player->session.localAccountId = -1;
		player->isNPC = isNPC;
		if(isNPC){
			player->botLogic.characterInSight = -1;
			player->botLogic.lastSearchedFrame = 0;
			RandomizeCustomizations(&player->customizations);
		}else{
			game.playerCount++;
		}

		return playerId;
	}
	return -1;
}

void SynchronizePlayer(int playerId){
	if(app.role != ROLE_HOST) return;
	int length = PrepareResponseForPlayer(playerId);
	BroadcastResponse(CHAN_STATUS, length);
}

void SynchronizePlayerStats(int playerId){
	if(app.role != ROLE_HOST) return;
	Player_t* player = &game.players[playerId];
	ResponseMessage_t* msg = GetResponseBuffer();
	msg->code = RESPONSE_PLAYER_STATS;
	msg->playerStats.playerId = playerId;
	PackPlayerStats(&msg->playerStats.stats, &player->stats);
	BroadcastResponse(CHAN_STATUS, 1+sizeof(struct ResponsePlayerStats_st));
}

void RemovePlayer(int playerId){
	Player_t* player = &game.players[playerId];
	if(!player->active) return;
	if(player->characterId != -1){
		Character_t* character = &game.characters[player->characterId];
		if(character->playerId == playerId){
			character->playerId = -1;
		}
	}
	if(!player->isNPC){
		game.playerCount--;
	}
	player->active = 0;
	SynchronizePlayer(playerId);
}

void ResetPlayerStats(int playerId){
	Player_t* player = &game.players[playerId];
	memset(&player->stats, 0, sizeof(player->stats));
	player->stats.checkpointId = -1;
}

static void UpdateBotLogic(int playerId, Player_t* player, Character_t* character, struct BotLogic_st* logic){
	if(logic->characterInSight == -1){
		if((game.frame - logic->lastSearchedFrame) < 30) return;
		logic->lastSearchedFrame = game.frame;

		for(int humanPlayerId = 0; humanPlayerId < MAX_PLAYERS; humanPlayerId++){
			Player_t* humanPlayer = &game.players[humanPlayerId];
			if((!humanPlayer->active) || (humanPlayer->isNPC) || (humanPlayer->characterId == -1)) continue;
			Character_t* humanCharacter = &game.characters[humanPlayer->characterId];
			if(!humanCharacter->health) continue;

			if ((humanCharacter->inventoryItemIds[0] == -1) && (humanCharacter->inventoryItemIds[humanCharacter->inventorySecondarySelectionSlot] == -1)) continue;

			Vector2 ourPos = GetPos(&character->tform);
			Vector2 humanPos = GetPos(&humanCharacter->tform);

			float dist = Vector2Distance(ourPos, humanPos);
			if(dist > 1850.0f) continue;

			cpSegmentQueryInfo queryInfo;
			cpVect rayEnd = toCpVect(humanPos);
			cpVect rayStart = toCpVect(ourPos);
				   rayStart = cpvadd(rayStart, cpvmult(cpvnormalize(cpvsub(rayEnd, rayStart)), character->tform.size.y));
			if(cpSpaceSegmentQueryFirst(&game.space, rayStart, rayEnd, 1, CP_SHAPE_FILTER_ALL, &queryInfo)){
				ShapeUserData_t* shapeUserData = cpShapeGetUserData(queryInfo.shape);
				if( (shapeUserData == NULL) || (shapeUserData->type != SHAPE_CHARACTER) ) continue;

				//printf("someone spotted! ours: %i. theirs: %i. shape id: %i. \n", player->characterId, humanPlayer->characterId, shapeUserData->id);

				logic->characterInSight = humanPlayerId;
				logic->characterSpottedFrame = game.frame;
			}


		}

	}else{
		Character_t* humanCharacter = &game.characters[logic->characterInSight];
		if(character->inventoryItemIds[0] != -1){
			Item_t* item = &game.items[character->inventoryItemIds[0]];

			Vector2 ourPos   = GetPos(&character->tform);
			Vector2 humanPos = GetPos(&humanCharacter->tform);
			float targetAngle = fmodf(Vector2Angle(ourPos, humanPos) * DEG2RAD, M_PI * 2);
			
			//float sourceAngle = character->primaryItemAngle;
			float sourceAngle = fmodf(GetAngle(&item->tform), M_PI * 2);
			//printf("target: %f, source: %f\n", targetAngle, sourceAngle);

			float turn = (targetAngle - sourceAngle);
			if(fabsf(turn) < 0.1){
				player->controls.buttons |= BUTTON_PRIMARY;
			}
			while (turn < -M_PI * 2) turn += M_PI * 2;
			while (turn >  M_PI * 2) turn -= M_PI * 2;
			//if(turn > 1) turn = 1;
			//if(turn < -1) turn = -1;
			turn = (turn < 0) ? -1 : 1;
			player->controls.axis_r = turn * 0.5;

		}

		if((game.frame - logic->characterSpottedFrame) > 20){
			logic->characterInSight = -1;
		}
	}
}

void UpdatePlayers(){
	for(int playerId=0; playerId < MAX_PLAYERS; playerId++){
		Player_t* player = &game.players[playerId];
		if(!player->active) continue;

		if(player->characterId != -1){
			Character_t* character = &game.characters[player->characterId];

			if(player->isNPC) UpdateBotLogic(playerId, player, character, &player->botLogic);

			character->controls = player->controls;
			for(int i = 0; i < PLAYER_MAX_INPUT_ACTIONS; i++){
				player->controls.inputActionQueue[i].type = ACTION_NONE;
			}

			if(!character->health){
				if(player->deathFrames > PLAYER_DEATH_DURATION){
					player->characterId = -1;
				}else{
					player->deathFrames++;
				}
			}else{
				player->deathFrames = 0;
			}
			player->respawnFrames = 0;
		}else{
			player->respawnFrames++;
		}

	}
}

void AddPlayerAction(int playerId, PlayerAction_t action){
	Player_t* player = &game.players[playerId];
	for(int i = 0; i < PLAYER_MAX_INPUT_ACTIONS; i++){
		if(player->controls.inputActionQueue[i].type == ACTION_NONE){
			player->controls.inputActionQueue[i] = action;
			return;
		}
	}
}

void RandomizeCustomizations(Customizations_t* customizations){
	customizations->skinColor = rand() % CUSTOM_SKIN_VARIATIONS;
	customizations->hairType  = rand() % CUSTOM_HAIR_TYPE_VARIATIONS;
}
