#pragma once
#include "raylib.h"

#define MAX_SIMPLE_PARTICLES 1024

typedef enum{
	EFFECT_SPAWN_POOF,
	EFFECT_BLOOD_SPLASH
} ParticleEffectType_e;

typedef enum{
	PARTICLE_S_INACTIVE = 0,
	PARTICLE_S_SPARK,
	PARTICLE_S_FLASH,
	PARTICLE_S_BLOOD
} SimpleParticleType_e;

typedef struct{
	SimpleParticleType_e type;
	int creationFrame;
	Vector2 pos;
	Vector2 velocity;
	union{
		int extra_i;
		float extra_f;
	};
} SimpleParticle_t;

typedef struct{
	int simpleMaxActiveId, simpleNextMinFreeId;
	SimpleParticle_t simpleParticles[MAX_SIMPLE_PARTICLES];
} ParticleSystemState_t;

void SpawnParticles(ParticleEffectType_e type, int amount, Vector2 pos);
void UpdateParticles();
void DrawParticles();
