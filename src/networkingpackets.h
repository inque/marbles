#pragma once
#include "players.h"
#include "gameinterface.h"
#include "projectiles.h"
#include "stdint.h"

typedef enum {
	REQUEST_AUTH = 0x40,
	REQUEST_INPUTS = 0x41,
	REQUEST_CHATMSG = 0x51
} RequestMessage_e;

typedef enum {
	RESPONSE_AUTH = 0x40,
	RESPONSE_NEWGAME = 0x50,
	RESPONSE_CHATMSG = 0x51,
	RESPONSE_PLAYER = 0x60,
	RESPONSE_CHARACTER = 0x61,
	RESPONSE_ITEM = 0x62,
	RESPONSE_PARTICLES = 0x63,
	RESPONSE_PLAYER_STATS = 0x64,
	RESPONSE_FRAME = 0x70
} ResponseMessage_e;


#pragma pack(push, 1)

typedef struct{
	uint8_t skinColor;
	uint8_t hairType;
} PackedCustomizations_t;

typedef struct{
	float pos[2];
	float angle;
	//uint16_t angle;
} PackedTransform_t;

typedef struct{
	float axis_h, axis_v, axis_r;
	uint32_t buttons;
	struct{
		uint8_t type;
		uint8_t parameter;
	} actions[PLAYER_MAX_INPUT_ACTIONS];
} PackedPlayerControls_t;

typedef struct{
	uint8_t checkpointId;
	int16_t killCount;
	int16_t deathCount;
} PackedPlayerStats;

typedef struct{
	uint8_t characterId;
	uint8_t inventorySecondarySelectionSlot;
	uint16_t health;
	PackedTransform_t tform;
} FrameResponseCharacterInfo_t;

typedef struct{
	uint8_t itemId;
	PackedTransform_t tform;
	float posOffset[2];
} FrameResponseItemInfo_t;

#define MAX_PROJECTILES_PER_PACKET 16
typedef struct{
	uint8_t type;
	float pos[2];
	float tail[PROJECTILE_TAIL_ELEMENTS][2];
} FrameResponseProjectileInfo_t;

typedef struct RequestMessage_st{
	//uint32_t magic;
	uint8_t code;
	union {
		char data[8192-1];
		struct RequestAuth_st{
			uint32_t version;
			char username[32];
			PackedCustomizations_t customizations;
		} auth;
		struct RequestInputs_st{
			PackedPlayerControls_t controls;
		} inputs;
		struct RequestChat_st{
			char text[CHAT_MESSAGE_LEN];
		} chat;
	};
} RequestMessage_t;

typedef struct ResponseMessage_st{
	//uint32_t magic;
	uint8_t code;
	union {
		char data[8192-1];
		struct ResponseAuth_st{
			uint8_t succeded;
			union{
				struct ResponseAuthFailure_st{
					char errorMessage[64];
				} failure;
				struct ResponseAuthSuccess_st{
					uint8_t playerId;
				} success;
			};
		} auth;
		struct ResponseNewGame_st{
			uint8_t gameType;
			uint8_t levelId;
		} newGame;
		struct ResponseChat_st{
			uint8_t type;
			uint8_t senderId;
			uint8_t variation;
			char text[CHAT_MESSAGE_LEN];
		} chat;
		struct ResponsePlayer_st{
			uint8_t playerId;
			uint8_t active;
			char name[32];
			uint8_t characterId;
			PackedCustomizations_t customizations;
			PackedPlayerStats stats;
		} player;
		struct ResponseCharacter_st{
			uint8_t characterId;
			uint8_t active;
			uint8_t playerId;
			uint8_t type;
			uint16_t health;
			PackedCustomizations_t customizations;
			PackedTransform_t tform;
		} character;
		struct ResponseItem_st{
			uint8_t itemId;
			uint8_t active;
			uint8_t type;
			uint8_t ownerId;
			uint8_t inventorySlot;
			PackedTransform_t tform;
		} item;
		struct ResponseParticles_st{
			uint8_t type;
			int16_t amount;
			float pos[2];
		} particles;
		struct ResponsePlayerStats_st{
			uint8_t playerId;
			PackedPlayerStats stats;
		} playerStats;
		struct ResponseFrame_st{
			uint32_t frame;
			uint8_t type;
			uint8_t state;
			uint16_t timer;
			uint8_t yourCharacterLookingAtItemId;
			uint8_t characterCount;
			uint8_t itemCount;
			uint8_t projectileBaseId;
			uint8_t projectileCount;
			char _rest[1];
				/* followed by:
				 * FrameResponseCharacterInfo_t characters[characterCount];
				 * FrameResponseItemInfo_t items[itemCount];
				 * FrameResponseProjectileInfo_t projectiles[projectileCount];
				 * */
		} frame;
	};
} ResponseMessage_t;

#pragma pack(pop)

void PackCustomizations(PackedCustomizations_t* dst, Customizations_t* src);
void UnpackCustomizations(Customizations_t* dst, PackedCustomizations_t* src);
void PackTform(PackedTransform_t* dst, Transform_t* src);
void UnpackTform(Transform_t* dst, PackedTransform_t* src);
void PackControls(PackedPlayerControls_t* dst, PlayerControls_t* src);
void UnpackControls(PlayerControls_t* dst, PackedPlayerControls_t* src);
void PackPlayerStats(PackedPlayerStats* dst, PlayerStats_t* src);
void UnpackPlayerStats(PlayerStats_t* dst, PackedPlayerStats* src);
void PackProjectile(FrameResponseProjectileInfo_t* dst, Projectile_t* src);
void UnpackProjectile(Projectile_t* dst, FrameResponseProjectileInfo_t* src);
