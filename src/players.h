#pragma once

#include "raylib.h"
#include "chipmunk/chipmunk.h"
#include "items.h"
#include "physics.h"
#include "networking.h"

#define PLAYER_DEATH_DURATION 1*60

#define PLAYER_MAX_INPUT_ACTIONS 4

#define CUSTOM_SKIN_VARIATIONS 8
#define CUSTOM_HAIR_TYPE_VARIATIONS 8

typedef struct{
	int skinColor;
	int hairType;
} Customizations_t;

typedef enum{
	BUTTON_PRIMARY = 1,
	BUTTON_SECONDARY = 2,
	BUTTON_CYCLE = 4
} PlayerButtons_e;

typedef enum{
	ACTION_NONE,
	ACTION_PICKUP,
	ACTION_THROW,
	ACTION_SELECT_SECONDARY,
	ACTION_SWAP
} PlayerAction_e;

typedef struct{
	PlayerAction_e type;
	int parameter;
} PlayerAction_t;

typedef struct{
	float axis_h, axis_v, axis_r;
	uint32_t buttons; //PlayerButtons_e
	uint32_t buttonsLast;
	PlayerAction_t inputActionQueue[PLAYER_MAX_INPUT_ACTIONS];
} PlayerControls_t;

typedef struct{
	int checkpointId;
	int killCount;
	int deathCount;
} PlayerStats_t;

typedef struct{
	int active;
	char name[32];
	int characterId;
	Customizations_t customizations;
	int deathFrames;
	int respawnFrames;
	PlayerControls_t controls;
	PlayerStats_t stats;
	int isNPC;
	struct BotLogic_st{
		int characterInSight;
		int characterSpottedFrame;
		int lastSearchedFrame;
	} botLogic;
	PlayerSession_t session;
} Player_t;


int CreatePlayer(int isNPC);
void SynchronizePlayer(int playerId);
void SynchronizePlayerStats(int playerId);
void RemovePlayer(int playerId);
void ResetPlayerStats(int playerId);
void UpdatePlayers();
void AddPlayerAction(int playerId, PlayerAction_t action);
void RandomizeCustomizations(Customizations_t* customizations);
