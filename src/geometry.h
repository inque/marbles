#pragma once

#include "raylib.h"

#include "stdbool.h"

#define SPLIT_MAX_POLYGONS 16
#define SPLIT_MAX_VERTICES 32

typedef struct{
	int vertCount;
	cpVect verts[SPLIT_MAX_VERTICES];
	int indices[SPLIT_MAX_VERTICES];
} SplitPoly;

typedef struct{
	int polyCount;
	SplitPoly polygons[SPLIT_MAX_POLYGONS];
} SplitOutput;

//cp- chipmunk (cpVect)
//rl- raylib (Vector2)

void cp_Triangulate(int vertCount, const cpVect *verts, SplitOutput* out);
//void cp_MergeHulls(SplitOutput* out, int vertCount);

bool rl_IsConvex(int vertCount, Vector2* verts);


