#include "items.h"
#include "game.h"
#include "graphics.h"
#include "projectiles.h"
#include "raymath.h"
#include "string.h"
#include "stdio.h"
#define _USE_MATH_DEFINES
#include "math.h"

void DefineItems(){
	ItemType_t* itemType;

	game.itemTypes[ITEM_JUMPY] = (ItemType_t){
		.name = "Stick",
		.strokeColor = (Color){169, 148, 141, 255},
		.hasPhysics = 1,
		.weight = 0.01f,
		.collisionVerts = { {-5, -25}, {5, -25}, {2, 17}, {-2, 17} },
		.collisionVertCount = 4
	};

	game.itemTypes[ITEM_KEYCARD] = (ItemType_t){
			.name = "Key Card",
			.strokeColor = (Color){235, 136, 118, 255},
			.hasPhysics = 1,
			.weight = 0.08f,
			.collisionVerts = { {-15, -10}, {-8, -20}, {12, -20}, {12, 20}, {-15, 20} },
			.collisionVertCount = 5
	};

	game.itemTypes[ITEM_BLASTER] = (ItemType_t){
			.name = "Blaster",
			.strokeColor = (Color){197, 132, 232, 255},
			.hasPhysics = 1,
			.weight = 0.14f,
			.collisionVerts = { {-12, -10}, {39, -15}, {39, 5}, {6, 28}, {-11, 28} },
			.collisionVertCount = 5
	};

	game.itemTypes[ITEM_ROCK] = (ItemType_t){
			.name = "Rock",
			.strokeColor = (Color){90, 90, 90, 255},
			.aimable = 1,
			.throwDamage = 20,
			.throwDamageThreshold = 12.5f,
			.hasPhysics = 1,
			.weight = 0.45f,
			.collisionVerts = { {-7,-6}, {0, -9}, {7,-6}, {7,6}, {0, 9}, {-7,6} },
			.collisionVertCount = 6
	};


	for(int itemTypeId=0; itemTypeId < MAX_ITEM_TYPES; itemTypeId++){
		itemType = &game.itemTypes[itemTypeId];
		if(itemType->hasPhysics){
			itemType->moment = cpMomentForPoly(itemType->weight, itemType->collisionVertCount, (cpVect*)&itemType->collisionVerts, cpvzero, (cpFloat)1.0);
		}
		switch(itemTypeId){
			default: {
				itemType->outlineVertCount = itemType->collisionVertCount;
				for (int i = 0; i < itemType->outlineVertCount; i++) {
					itemType->outlineVerts[i].x = itemType->collisionVerts[i].x;
					itemType->outlineVerts[i].y = itemType->collisionVerts[i].y;
				}
				itemType->approximateSize = (Vector2) {42, 42};
			} break;
		}
	}

}

void InitializeItemData(int itemId){
	Item_t* item = &game.items[itemId];

	for(int i=0; i < ITEM_MAX_CONSTRAINTS; i++){
		item->constraints[i] = NULL;
	}
	for(int i=0; i < ITEM_MAX_ACTIONS; i++){
		item->actionExecuted[i] = 0;
		item->actionLastFrame[i] = -1;
	}
	item->ownerCharacterId = -1;
	item->lastOwnerId = -1;
	item->despawnTimer = DROPPED_ITEM_DESPAWN_TIME;

	item->posOffset = Vector2Zero();
	item->flags = 0;

	ItemType_t* itemType = &game.itemTypes[item->type];
	item->tform.size = itemType->approximateSize;
}

int SpawnItem(ItemType_e type, Vector2 pos){
	for(int itemId = 0; itemId < MAX_ITEMS; itemId++){
		Item_t* item = &game.items[itemId];
		if(item->active) continue;

		item->active = 1;
		item->type = type;
		SetTransformData(&item->tform, pos, 0.0f);
		InitializeItemData(itemId);

		ItemType_t* itemType = &game.itemTypes[type];
		if(itemType->hasPhysics){
			item->body = cpSpaceAddBody(&game.space, cpBodyNew(itemType->weight, itemType->moment));
			item->shape = cpSpaceAddShape(&game.space, cpPolyShapeNew(item->body, itemType->collisionVertCount, (cpVect*)itemType->collisionVerts, cpTransformIdentity, (cpFloat)1.0));
			cpBodySetPosition(item->body, cpv((cpFloat)pos.x, (cpFloat)pos.y));

			item->shapeUserData.type = SHAPE_ITEM;
			item->shapeUserData.id = itemId;
			cpShapeSetCollisionType(item->shape, COLLISION_TYPE_ITEM);
			cpShapeSetUserData(item->shape, &item->shapeUserData);
		}else{
			item->body = NULL;
			item->shape = NULL;
		}

//		switch(item->type){
//			case ITEM_JUMPY: {
//			} break;
//		}

		//SynchronizeItem(itemId);
		return itemId;
	}
	return -1;
}

void SynchronizeItem(int itemId){
	if(app.role != ROLE_HOST) return;
	int length = PrepareResponseForItem(itemId);
	BroadcastResponse(CHAN_STATUS, length);
}

void RemoveItem(int itemId){
	Item_t* item = &game.items[itemId];
	if(!item->active) return;

	if(item->body) cpSpaceRemoveBody(&game.space, item->body);
	if(item->shape) cpSpaceRemoveShape(&game.space, item->shape);

	if( (item->flags & ITEMFLAG_RESPAWNABLE) && (game.state != STATE_UNINITIALIZED) ){
		int newItemId = SpawnItem(item->type, item->respawnPos);
		if(newItemId != -1){
			Item_t* newItem = &game.items[newItemId];
			newItem->flags = item->flags;
			newItem->respawnPos = item->respawnPos;
		}
		SynchronizeItem(newItemId);
	}

	item->active = 0;
	SynchronizeItem(itemId);
}

static void DisownItem(Item_t* item){
	if(!item->body) return;
	for(int i=0; i < ITEM_MAX_CONSTRAINTS; i++){
		if(item->constraints[i]){
			//printf("rem cnstr ITEM bg\n");
			cpSpaceRemoveConstraint(&game.space, item->constraints[i]);
			item->constraints[i] = NULL;
			//printf("rem cnstr ITEM end\n");
		}
	}
	//cpVect offsetPos = cpv((item->pos.x + item->posOffset.x), (item->pos.y + item->posOffset.y));
	cpVect offsetPos = toCpVect(Vector2Add(GetPos(&item->tform), item->posOffset));
	cpVect offsetDelta = cpv(5*(item->posOffset.x - item->posOffsetLast.x), 5*(item->posOffset.y - item->posOffsetLast.y));
	cpBodySetPosition(item->body, offsetPos);
	//UpdateTransform(&item->tform, item->body);
	//cpBodyApplyForceAtLocalPoint(item->body, offsetDelta, cpvzero);
	cpBodySetVelocity(item->body, cpvadd(cpBodyGetVelocity(item->body), offsetDelta));
	item->posOffset = Vector2Zero();
	item->posOffsetLast = Vector2Zero();
}

void SetItemOwner(int itemId, int characterId){
	Item_t* item = &game.items[itemId];
	if(characterId == -1){
		item->ownerCharacterId = -1;
		item->lastChangedOwnerFrame = game.frame;
		DisownItem(item);
	}else{
		if(item->ownerCharacterId == characterId) return;
		if(item->ownerCharacterId != -1) DisownItem(item);
		Character_t* character = &game.characters[characterId];
		cpBodySetPosition(item->body, cpBodyGetPosition(character->bodies[0]));

		//item->constraint = cpSpaceAddConstraint(&game.space, cpDampedSpringNew(item->body, character->bodies[0], cpv(item->pos.x, item->pos.y), cpv(character->pos.x, character->pos.y), 0.1, 0.5, 0.2));
		//item->constraints[0] = cpSpaceAddConstraint(&game.space, cpDampedSpringNew(item->body, character->bodies[0], cpvzero, cpvzero, 0.0, 0.999, 0.1));
		//item->constraints[0] = cpSpaceAddConstraint(&game.space, cpDampedRotarySpringNew(item->body, character->bodies[0], 0.0, 0.8, 0.8));
		//item->constraints[0] = cpSpaceAddConstraint(&game.space, cpPinJointNew(item->body, character->bodies[0], cpvzero, cpvzero));
		//item->constraints[0] = cpSpaceAddConstraint(&game.space, cpDampedSpringNew(item->body, character->bodies[0], cpvzero, cpv(0, 1.0), 0.0, 0.999, 0.05));
		item->constraints[0] = cpSpaceAddConstraint(&game.space, cpPivotJointNew(item->body, character->bodies[0], cpBodyGetPosition(item->body)));

		item->ownerCharacterId = characterId;
		item->lastOwnerId = characterId;
		item->lastChangedOwnerFrame = game.frame;
	}
	SynchronizeItem(itemId);
}

void ItemAction(int itemId, int actionId){
	Item_t* item = &game.items[itemId];
	if(item->actionExecuted[actionId]) return;
	item->actionExecuted[actionId] = 1;
	item->actionLastFrame[actionId] = game.frame;
	int playerId = -1;
	if(item->ownerCharacterId != -1){
		Character_t* character = &game.characters[item->lastOwnerId];
		playerId = character->playerId;
	}
	switch(item->type){
		case ITEM_BLASTER:{
			float angle = GetAngle(&item->tform) + M_PI_2;
			Vector2 orientation = (Vector2){sinf(angle), -cosf(angle)};
			SpawnProjectile(PROJ_LASER, playerId, itemId, Vector2Add(GetPos(&item->tform), Vector2Scale(orientation, 52.0f)), orientation);
			cpBodyApplyImpulseAtLocalPoint(item->body, cpv(-10 * orientation.x, -10 * orientation.y), cpvzero);
			//printf("pew! %f %f\n", item->pos.x, item->pos.y);
		} break;
	}
}

cpBool CallbackPresolveItem(cpArbiter *arb, cpSpace *space, void *ignore){
	CP_ARBITER_GET_SHAPES(arb, a, b);
	ShapeUserData_t* ourData = (ShapeUserData_t*)cpShapeGetUserData(a);
	ShapeUserData_t* otherData = (ShapeUserData_t*)cpShapeGetUserData(b);
	Item_t* item = &game.items[ourData->id];
	if(otherData == NULL) return cpTrue;
	if(otherData->type == SHAPE_ITEM) {
		Item_t* otherItem = &game.items[otherData->id];
		if( (item->ownerCharacterId != -1) && (item->ownerCharacterId == otherItem->ownerCharacterId) ){
			return cpArbiterIgnore(arb);
		}
	}
	return cpTrue;
}

static void JumpyPushBody(Character_t* character, cpVect orientation, cpVect point, cpFloat forceMultiplier){
	cpBody* srcBody = character->bodies[0];
	cpPointQueryInfo queryInfo;
	cpBody* staticBody = cpSpaceGetStaticBody(&game.space);
	cpShape* nearestShape = cpSpacePointQueryNearest(&game.space, point, 4.0, CP_SHAPE_FILTER_ALL, &queryInfo);
	if(!nearestShape) return;
	cpBody* nearestBody = cpShapeGetBody(nearestShape);

	if(character->primaryItemAngleAcceleration != 0){
		float torque = fabsf(character->primaryItemAngleAcceleration);
		//forceMultiplier += (1.25f * torque) * ((forceMultiplier < 0) ? -1 : 1);
		forceMultiplier *= 1 + 0.25f * torque;
		//cpBodySetAngularVelocity(dstBody, 1000.0f * torque + cpBodyGetAngularVelocity(dstBody));
		//cpBodySetTorque(dstBody, 10000000 * torque + cpBodyGetTorque(dstBody));
		CharacterType_t* characterType = &game.characterTypes[character->type];
		character->externalTorque += (-4 / characterType->angleMultiplier) * character->primaryItemAngleAcceleration;
	}

	ShapeUserData_t* shapeUserData = cpShapeGetUserData(nearestShape);
	if(shapeUserData){
		if(shapeUserData->type == SHAPE_CHARACTER){
			Character_t* dstCharacter = &game.characters[shapeUserData->id];
			if(dstCharacter->health > 0){
				dstCharacter->healthLastDamageSource = DEATH_BY_ITEM;
				dstCharacter->healthLastDamagePlayerId = character->playerId;
				dstCharacter->healthLastDamageByPlayerFrame = game.frame;
			}
			ApplyDamage(shapeUserData->id, 20, (Vector2){queryInfo.point.x, queryInfo.point.y}, (Vector2){orientation.x, orientation.y});
		}else if(shapeUserData->type == SHAPE_ITEM){
			forceMultiplier *= 0.01f;
		}
	}

	cpFloat force = forceMultiplier * fabs(cpvdot(orientation, queryInfo.gradient));
	cpBodyApplyImpulseAtWorldPoint(srcBody, cpvmult(orientation, force), queryInfo.point);
	if(nearestBody != staticBody){
		cpBodyApplyImpulseAtWorldPoint(nearestBody, cpvmult(orientation, force * -1.0f), queryInfo.point);
	}
	//SpawnParticles(EFFECT_SPAWN_POOF, 3, (Vector2){queryInfo.point.x, queryInfo.point.y});

}

void UpdateItems(){
	for(int itemId = 0; itemId < MAX_ITEMS; itemId++){
		Item_t* item = &game.items[itemId];
		if(!item->active) continue;

		Character_t* character = NULL;

		if(item->ownerCharacterId == -1){
			if(!item->despawnTimer){
				RemoveItem(itemId);
				continue;
			}else{
				item->despawnTimer--;
			}
		}else{
			item->despawnTimer = DROPPED_ITEM_DESPAWN_TIME;
			character = &game.characters[item->ownerCharacterId];
		}

		if(item->body){
			UpdateTransform(&item->tform, item->body);
			if(character){
				cpBodySetAngle(item->body, item->targetAngle);
				//cpBodySetTorque(item->body, character->primaryItemAngle - item->angle );
			}
		}
		Vector2 pos = GetPos(&item->tform);
		float angle = GetAngle(&item->tform);

		item->posOffsetLast = item->posOffset;
		item->posOffset = Vector2Zero();
		if(item->actionExecuted[0]){
			int elapsed = (game.frame - item->actionLastFrame[0]);
			switch(item->type){
			case ITEM_JUMPY: {
				if(item->constraints[0] && character){
					cpVect orientation = cpv(sinf(angle), -cosf(angle));
					float length = (20.0f + (float)elapsed * 1.2f) * sinf(5.5f+((float)elapsed / 30.0f)*2*3.14f);
					if(elapsed < 8) length *= ((float)elapsed / 8);
					if(elapsed > 22) length *= ((30 - (float)elapsed) / 8);
					if(elapsed == 6){
						cpVect point = cpvadd(toCpVect(pos), cpvmult(orientation, 50.0));
						JumpyPushBody(character, orientation, point, -60.0f);
					}
					if(elapsed == 18){
						cpVect point = cpvadd(toCpVect(pos), cpvmult(orientation, -43.0));
						JumpyPushBody(character, orientation, point, 150.0f);
					}
					//if(elapsed == 20) length = 0.0f;
					//							cpDampedSpringSetRestLength(item->constraints[0], length);
					//item->posOffset = (Vector2){ length * sinf(item->angle), length * -cosf(item->angle) };
					item->posOffset = (Vector2){ length * (float)orientation.x, length * (float)orientation.y };
				}
				if(elapsed > 30) item->actionExecuted[0] = 0;
			} break;
			case ITEM_ROCK: {
				if(item->constraints[0] && character){
					cpVect orientation = cpv(sinf(angle), -cosf(angle));
					float length = (20.0f + (float)elapsed * 1.7f) * sinf(5.3f+((float)elapsed / 30.0f)*2*3.14f);
					if(elapsed < 8) length *= ((float)elapsed / 8);
					item->posOffset = (Vector2){ length * (float)orientation.x, length * (float)orientation.y };
					item->posOffset.x += orientation.y * 5;
					item->posOffset.y += orientation.x * 5;
					if(elapsed == 20){
						int slotId = FindSlot(item->ownerCharacterId, itemId);
						if(slotId != -1) character->inventoryItemIds[slotId] = -1;
						SetItemOwner(itemId, -1);
						cpBodyApplyImpulseAtWorldPoint(item->body, cpvmult(orientation, -10), item->body->p);
					}
				}
				if(elapsed >= 20){
					item->actionExecuted[0] = 0;
				}
			} break;
			default: {
				if(elapsed > 30) item->actionExecuted[0] = 0;
			} break;
			}
		}

		if(!DoesIntersectRect(&item->tform, game.level.bounds)){
			RemoveItem(itemId);
			return;
		}

	}
}

void DrawItemIcon(ItemType_e type, Vector2 pos, float angle, float scale){
	BeginTransformManual(pos, angle, scale);
	ItemType_t* itemType = &game.itemTypes[type];
	switch(type){
		default:
			DrawPolyObjectStroke(itemType->outlineVertCount, (Vector2*)&itemType->outlineVerts, itemType->strokeColor);
			break;
		case ITEM_BLASTER: {
			const Vector2 shape1[] = { {0, -11}, {13, -9}, {22, -12}, {23, 7},
									   {14, 4}, {10, 9}, {12, 23}, {6, 29},
									   {-6, 28}, {-11, 24}, {-10, 4} };
			const Vector2 shape2[] = { {-14, -13}, {-2, -8}, {-11, 0} };
			const Vector2 shape3[] = { {25, -11}, {36, -21}, {37, 11}, {25, 3} };
			Color color1 = (Color){145, 154, 164, 255};
			Color color2 = (Color){197, 132, 232, 255};

			DrawPolyObjectStroke(sizeof(shape1) / sizeof(Vector2), (Vector2*)&shape1, color1);
			DrawRectangle(38, -8, 13, 5, color1);
			DrawPolyObjectStroke(sizeof(shape2) / sizeof(Vector2), (Vector2*)&shape2, color2);
			DrawPolyObjectStroke(sizeof(shape3) / sizeof(Vector2), (Vector2*)&shape3, color2);
		} break;
	}
	EndTransform();
}

void DrawItem(int itemId){
	Item_t* item = &game.items[itemId];
	ItemType_t* itemType = &game.itemTypes[item->type];
	Vector2 pos = GetInterpolatedPos(&item->tform);
	float angle = GetInterpolatedAngle(&item->tform);

	switch(item->type){
		default: {
			BeginTransformManual(Vector2Add(pos, item->posOffset), angle, 1.0f);
			DrawPolyObjectStroke(itemType->outlineVertCount, (Vector2*)&itemType->outlineVerts, itemType->strokeColor);
			EndTransform();
			//DrawRectangleLines(item->pos.x, item->pos.y, 10, 10, RED);
		} break;
		case ITEM_BLASTER:
			DrawItemIcon(ITEM_BLASTER, pos, angle, 1.0f);
			break;
	}

	if(itemType->aimable && (item->ownerCharacterId != -1)){
		//Vector2 orientation = (Vector2){sinf(angle), -cosf(angle)};
		BeginTransformManual(pos, angle, 1.0f);
		DrawLine(0, 5, 0, 25, DARKGRAY);
		DrawLine(-4, 20, 0, 25, DARKGRAY);
		DrawLine( 4, 20, 0, 25, DARKGRAY);
		EndTransform();
	}

}

void DrawItems(){
	for(int itemId = 0; itemId < MAX_ITEMS; itemId++){
		Item_t* item = &game.items[itemId];
		if(!item->active) continue;
		if(!IsInView(&item->tform)) continue;
		if(item->ownerCharacterId != -1) continue;
		DrawItem(itemId);
	}
}
