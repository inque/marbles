#include "game.h"
#include "assets.h"
#include "networking.h"
#include "raylib.h"
#include "stdio.h"
#include "string.h"
#include "time.h"

#include "chipmunk/chipmunk.h"
#if (CP_USE_DOUBLES)
	#error "Can't compile because Chipmunk and Raylib should use the same float type. (CP_USE_DOUBLES = 0)"
#endif

int main(){
	srand(time(NULL));
	memset(&app, 0, sizeof(app));
	game.initialized = 0;
	LoadSettings();
	InitNetworking();
	SetConfigFlags(FLAG_WINDOW_RESIZABLE);
	//SetConfigFlags(FLAG_VSYNC_HINT | FLAG_WINDOW_RESIZABLE);
	InitWindow(app.settings.windowSize[0], app.settings.windowSize[1], "Marbles?");
	SetExitKey(0);
	//SetGesturesEnabled(0);
	SetWindowMinSize(640, 450);
	if(LoadAssets()){
		return 1;
	}
	//app.debugMode = 1;
	app.screen = SCREEN_MAINMENU;
	app.mainMenuScreen = MAINMENU_HOME;
	StartGameLoop();
	CleanUpNetworking();
	SaveSettings();
	return 0;
}
