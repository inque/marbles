#include "assets.h"
#include "game.h"
#define FIGURES_H_IMP
#include "generated/figures.h"
#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

Texture* ResolveTexture(const char* src){
	if(!strcmp(src, "tex1")){
		return &app.assets.tex1;
	}
	return NULL;
}

static Texture2D LoadTextureFromImageEx(Image image){
}

static inline uint32_t* GetPixelPtr(Color* pixels, int width, int x, int y){
	return (uint32_t*)(&pixels[y*width + x]);
}

static Texture2D LoadMaskedTexture(const char* path, bool expanded){
	const uint32_t ALPHA = (0xFF << 24);
	const uint32_t COLOR = (0x01 << 24) - 1;
	const Color expMaskColor = (Color){255, 0, 255, 255};
	  const int expMaskThreshold = 60;
	const float expMaskAmplify = 2.0f;

	Image img = LoadImage(path);
	uint32_t bgColor = ((uint32_t*)(img.data))[0] | ALPHA;
	Color* pixels;
	Color* pixelsOriginal;
	int originalWidth = img.width;
	if(expanded){
		pixelsOriginal = GetImageData(img);
		img.width *= 2;
		pixels = RL_CALLOC(sizeof(Color), img.width * img.height);
	}else{
		pixels = GetImageData(img);
	}
	for(int iy=0; iy < img.height; iy++)
	for(int ix=0; ix < originalWidth; ix++){
		uint32_t* dst = GetPixelPtr(pixels, img.width, ix, iy);
		if(expanded){
			uint32_t* src = GetPixelPtr(pixelsOriginal, originalWidth, ix, iy);
			*dst = *src;
		}
		if(*dst == bgColor){
			// if(ix && iy && (ix < img.width-1) && (iy < img.height-1)){
			// 	for(int d=0; d < 4; d++){
			// 		int offset[] = {
			// 			(d < 2) ? ix+(d%2)*2-1 : ix,
			// 			(d > 1) ? iy+(d%2)*2-1 : iy
			// 		};
			// 		uint32_t* other = GetPixelPtr(pixels, img.width, offset[0], offset[1]);
			// 		if((*other != bgColor) && (*other & ALPHA)){
			// 			*dst = *other;
			// 			break;
			// 		}
			// 	}
			// }
			*dst &= COLOR;
		}else if(expanded){
			Color dstCol = *(Color*)dst;
			int diff[] = { expMaskColor.r - dstCol.r,
						   expMaskColor.g - dstCol.g,
						   expMaskColor.b - dstCol.b };
			int diffTotal = abs(diff[0]) + abs(diff[1]) + abs(diff[2]);
			//if(*dst == *(uint32_t*)&expMaskColor){
			if(diffTotal < expMaskThreshold){
				//uint32_t* mirrorDst = GetPixelPtr(pixels, img.width, ix + originalWidth, iy);
				//*mirrorDst = *dst;
				//*mirrorDst = -1;
				Color* mirrorDstColor = (Color*)GetPixelPtr(pixels, img.width, ix + originalWidth, iy);
				mirrorDstColor->r = 255 - expMaskThreshold + diff[0];
				mirrorDstColor->g = 255 - expMaskThreshold + diff[1];
				mirrorDstColor->b = 255 - expMaskThreshold + diff[2];
				mirrorDstColor->a = 255;
				*dst &= COLOR;
			}
		}
	}
	RL_FREE(img.data);
	img.data = pixels;
	img.format = UNCOMPRESSED_R8G8B8A8;
	img.mipmaps = 1;
	Texture2D tex = LoadTextureFromImage(img);
	
	UnloadImage(img);
	return tex;
}

bool LoadAssets(){
	app.assets.tex1 = LoadMaskedTexture("data/gfx/tex1.png", true);
	
	return false;
}
