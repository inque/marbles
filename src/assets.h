#pragma once
#include "raylib.h"
#include "stdbool.h"

typedef struct{
	Texture2D tex1;
} AppAssets_t;

bool LoadAssets();
Texture* ResolveTexture(const char* src);
