#include "level.h"
#include "game.h"
#include "geometry.h"
#include "graphics.h"
#include "items.h"
#include "raymath.h"
#include "string.h"
#include "stdio.h"
#define _USE_MATH_DEFINES
#include "math.h"

void LV_InitPoly(Level_t* level, int polyId){
	LevelPoly_t* poly = &level->polygons[polyId];
	Vector2 pos = GetPos(&poly->tform);
	float angle = GetAngle(&poly->tform);
	bool isDynamic = (poly->flags & LV_BLOCK_DYNAMIC);
	for(int i=0; i < poly->vertCount; i++){
		poly->verts[i].x -= pos.x;
		poly->verts[i].y -= pos.y;
	}
	LV_UpdatePolyBounds(poly);
	SplitOutput triangulation;
	cp_Triangulate(poly->vertCount, (const cpVect*)&poly->verts, &triangulation);
	poly->triangleCount = triangulation.polyCount;
	for(int triangleId=0; triangleId < triangulation.polyCount; triangleId++){
		int* indices = &poly->triangleIndices[triangleId * 3];
		SplitPoly* triangle = &triangulation.polygons[triangleId];
		for(int i = 0; i < 3; i++){
			indices[i] = triangle->indices[i];
		}
	}
}

void LV_UpdatePolyBounds(LevelPoly_t* poly){
	bool isDynamic = (poly->flags & LV_BLOCK_DYNAMIC);
	Vector2 pos = isDynamic ? GetPos(&poly->tform) : Vector2Zero();
	Rectangle bounds = (Rectangle){INT32_MAX, INT32_MAX, -INT32_MAX, -INT32_MAX};
	for(int i=0; i < poly->vertCount; i++){
		poly->verts[i].x -= pos.x;
		poly->verts[i].y -= pos.y;
		if(poly->verts[i].x < bounds.x){
			bounds.x = poly->verts[i].x;
		}else if(poly->verts[i].x > bounds.width){
			bounds.width = poly->verts[i].x;
		}
		if(poly->verts[i].y < bounds.y){
			bounds.y = poly->verts[i].y;
		}else if(poly->verts[i].y > bounds.height){
			bounds.height = poly->verts[i].y;
		}
	}
	poly->tform.size = (Vector2){bounds.width - bounds.x, bounds.height - bounds.y};
	if(!isDynamic){
		SetTransformStatic(&poly->tform, (Vector2){bounds.x + (poly->tform.size.x / 2), bounds.y + (poly->tform.size.y / 2)}, 0);
	}
}

int LV_InsertPoly(Level_t* level, int layerId, bool isTextured){
	if(level->polygonCount+1 >= LV_MAX_POLYGONS) return -1;
	LevelLayer_t* layer = &level->layers[layerId];
	int basePoly = layer->polyStartId + layer->polyTexturedCount;
	if(isTextured) basePoly += layer->polyUntexturedCount;
	int polysToMove = level->polygonCount - basePoly;
	if((!isTextured) && layer->polyUntexturedCount) polysToMove++;
	int layersAboveCurrent = level->layerCount - 1 - layerId;
	if(polysToMove > 0){
		memmove(&level->polygons[basePoly+1],
				&level->polygons[basePoly],
				polysToMove * sizeof(LevelPoly_t));
	}
	for(int i=0; i < layersAboveCurrent; i++){
		LevelLayer_t* otherLayer = &level->layers[layerId + 1 + i];
		otherLayer->polyStartId++;
	}
	if(isTextured){
		layer->polyTexturedCount++;
	}else{
		layer->polyUntexturedCount++;
	}
	level->polygonCount++;
	return basePoly;
}

void LV_RemovePoly(Level_t* level, int polyId){
	if(polyId+1 > level->polygonCount) return;
	bool foundInLayers = false;
	int layerId;
	for(layerId = 0; layerId < level->layerCount; layerId++){
		LevelLayer_t* layer = &level->layers[layerId];
		if(polyId < (layer->polyStartId + layer->polyTexturedCount)){
			layer->polyTexturedCount--;
			foundInLayers = true;
			break;
		}
		if(polyId < (layer->polyStartId + layer->polyTexturedCount + layer->polyUntexturedCount)){
			layer->polyUntexturedCount--;
			foundInLayers = true;
			break;
		}
	}
	if(foundInLayers){
		int layersAboveCurrent = level->layerCount - 1 - layerId;
		for(int i=0; i < layersAboveCurrent; i++){
			LevelLayer_t* otherLayer = &level->layers[layerId + 1 + i];
			otherLayer->polyStartId--;
		}
	}
	int polysToMove = level->polygonCount - polyId;
	if(polysToMove > 0){
		memmove(&level->polygons[polyId],
				&level->polygons[polyId+1],
				polysToMove * sizeof(LevelPoly_t));
	}
};

void LV_SwapLayers(Level_t* level, int layerA_id, int layerB_id){
	if(layerA_id == layerB_id) return;
	int minLayerId = (layerA_id < layerB_id) ? layerA_id : layerB_id;
	int maxLayerId = (layerA_id > layerB_id) ? layerA_id : layerB_id;
	LevelLayer_t minLayer = level->layers[minLayerId];
	LevelLayer_t maxLayer = level->layers[maxLayerId];
	printf("initial. MIN: start %i  MAX: start %i\n", minLayer.polyStartId, maxLayer.polyStartId);
	//save min layer polys
	LevelPoly_t minLayerPolys[LV_MAX_POLYGONS];
	int minLayerPolyCount = minLayer.polyTexturedCount + minLayer.polyUntexturedCount;
	memcpy( &minLayerPolys,
			&level->polygons[minLayer.polyStartId],
			minLayerPolyCount * sizeof(LevelPoly_t) );
	//save max layer polys
	LevelPoly_t maxLayerPolys[LV_MAX_POLYGONS];
	int maxLayerPolyCount = maxLayer.polyTexturedCount + maxLayer.polyUntexturedCount;
	memcpy( &maxLayerPolys,
			&level->polygons[maxLayer.polyStartId],
			maxLayerPolyCount * sizeof(LevelPoly_t) );
	//shift
	int polyCountDifference = maxLayerPolyCount - minLayerPolyCount;
	int layersInBetween = (maxLayerId - 1 - minLayerId);
	if(layersInBetween){
		memmove( &level->polygons[minLayer.polyStartId + minLayerPolyCount + polyCountDifference],
				 &level->polygons[minLayer.polyStartId + minLayerPolyCount],
				 polyCountDifference * sizeof(LevelPoly_t) );
		for(int i=0; i < layersInBetween; i++){
			LevelLayer_t* layer = &level->layers[minLayerId + 1 + i];
			layer->polyStartId += polyCountDifference;
		}
	}
	//copy to destination
	printf("prev. MIN: start %i size %i  MAX: start %i size %i\n", minLayer.polyStartId, minLayerPolyCount, maxLayer.polyStartId, maxLayerPolyCount);
	int maxLayerPolyStartId = maxLayer.polyStartId;
	maxLayer.polyStartId = minLayer.polyStartId;
	minLayer.polyStartId = maxLayerPolyStartId + polyCountDifference;
	printf("next. MIN: start %i size %i  MAX: start %i size %i\n", minLayer.polyStartId, minLayerPolyCount, maxLayer.polyStartId, maxLayerPolyCount);
	level->layers[minLayerId] = maxLayer;
	level->layers[maxLayerId] = minLayer;
	memcpy( &level->polygons[maxLayer.polyStartId],
			&maxLayerPolys,
			maxLayerPolyCount * sizeof(LevelPoly_t) );
	memcpy( &level->polygons[minLayer.polyStartId],
			&minLayerPolys,
			minLayerPolyCount * sizeof(LevelPoly_t) );
}

void LV_RemoveLayer(Level_t* level, int layerId){
	LevelLayer_t* layer = &level->layers[layerId];
	int polyCount = layer->polyTexturedCount + layer->polyUntexturedCount;
	int layersAboveCurrent = level->layerCount - 1 - layerId;
	int polysToMove = level->polygonCount - (layer->polyStartId + polyCount);
	if(level->layerCount <= 1) return;
	if(polysToMove){
		memmove(&level->polygons[layer->polyStartId],
				&level->polygons[layer->polyStartId + polyCount],
				polysToMove * sizeof(LevelPoly_t));
	}
	if(layersAboveCurrent){
		memmove(&level->layers[layerId],
				&level->layers[layerId + 1],
				layersAboveCurrent * sizeof(LevelLayer_t));
		for(int i=0; i < layersAboveCurrent; i++){
			LevelLayer_t* anotherLayer = &level->layers[layerId + i];
			anotherLayer->polyStartId -= polyCount;
		}
	}
	level->polygonCount -= polyCount;
	level->layerCount--;
}

static const uint32_t LEVEL_FILE_MAGIC = 1819698540;

bool SaveLevel(Level_t* level, const char* path){
	FILE* file = fopen(path, "w");
	if(!file) return true;
	fwrite(&LEVEL_FILE_MAGIC, sizeof(uint32_t), 1, file);
	
	LV_File_Header_t header = {
		.len = sizeof(header),
		.version = 0,
		.layerCount = level->layerCount,
		.bgColor = *(uint32_t*)&level->background,
		.bounds = *(float*)&level->bounds
	};
	fwrite(&header, sizeof(header), 1, file);

	//todo

	fclose(file);
	return false;
}


void LoadLevel(int id){
	bool noPhysics = (app.role == ROLE_REMOTE);
	Level_t* level = &game.level;
	cpSpace* space = noPhysics ? NULL : &game.space;
	memset(level, 0, sizeof(Level_t));

	switch(id){
		case 0: {
			level->background = (Color){247, 244, 242, 255};
			level->spawnPos = (Vector2){460, 180};
			//level->spawnPos = (Vector2){-2005, 264};
			//level->spawnPos = (Vector2){-730, 235};

			LevelPoly_t* poly;

			Color terrainColor = (Color){173, 180, 179, 255};
			Color wallColor = (Color){163, 151, 170, 255};
			Color treeTopsColor = (Color){150, 179, 149, 255};
			Color treeTrunksColor = (Color){177, 159, 148, 255};
//			Color bgTerrainColor = (Color){223, 224, 222, 255};
//			Color bgTreeColor = (Color){210, 224, 209, 255};

			//background

//			Vector2 polyBg1[] = {
//					{-510, 497}, {85, 467}, {50, 770}, {-560, 780}
//			};
//			poly = &level->polygons[level->polygonCount++];
//			memcpy(&poly->verts, &polyBg1, sizeof(polyBg1));
//			poly->vertCount = sizeof(polyBg1)/sizeof(Vector2);
//			poly->borderColor = bgTerrainColor;
//			poly->flags = LV_BLOCK_BACKGROUND;
//
//			Vector2 polyBg2[] = {
//					{1460, 490}, {1750, 490}, {1742, 770}, {1487, 780}
//			};
//			poly = &level->polygons[level->polygonCount++];
//			memcpy(&poly->verts, &polyBg2, sizeof(polyBg2));
//			poly->vertCount = sizeof(polyBg2)/sizeof(Vector2);
//			poly->borderColor = bgTerrainColor;
//			poly->flags = LV_BLOCK_BACKGROUND;
//
//			Vector2 polyBg3[] = {
//					{840, 212}, {882, 275}, {855, 276}, {878, 317},
//					{848, 320}, {843, 376}, {823, 376}, {820, 320},
//					{790, 321}, {816, 284}, {800, 281}
//			};
//			poly = &level->polygons[level->polygonCount++];
//			memcpy(&poly->verts, &polyBg3, sizeof(polyBg3));
//			poly->vertCount = sizeof(polyBg3)/sizeof(Vector2);
//			poly->borderColor = bgTreeColor;
//			poly->flags = LV_BLOCK_BACKGROUND;


			//level

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{120,  174}, {210,  174}, {275,  380}, {1030,  380},
					{1065, 410}, {1292, 410}, {1355, 420}, {1395, 441},
					{1437, 479}, {1455, 800}, {75,   800}
				},
				.vertCount = 11,
				.borderColor = terrainColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{1773, 411}, {1839, 450}, {2586, 450}, {2568, 800}, {1758, 800}
				},
				.vertCount = 5,
				.borderColor = terrainColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = { {2540, 183}, {2596, 183}, {2596, 439}, {2540, 439} },
				.vertCount = 4,
				.borderColor = wallColor
			};

			/*level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{2258, 133}, {2300, 176}, {2337, 181}, {2359, 210},
					{2294, 207}, {2244, 161}, {2018, 151}, {1960, 27},
					{1981, 20},  {2042, 128}
				},
				.vertCount = 10,
				.borderColor = wallColor
			};*/

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{2258, 133}, {2300, 176}, {2337, 181}, {2359, 210},
					{2294, 207}, {2244, 161}
				},
				.vertCount = 6,
				.borderColor = wallColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{2018, 151}, {1960, 27}, {1981, 20},  {2042, 128},
					{2258, 133}, {2244, 161}
				},
				.vertCount = 6,
				.borderColor = wallColor
			};


			//tree section

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{2792, 457}, {2826, 457}, {2830, 488}, {2824, 512},
					{2796, 514}, {2790, 483}
				},
				.vertCount = 6,
				.borderColor = treeTrunksColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = { {2783, 186}, {2823, 185}, {2863, 488}, {2753, 488} },
				.vertCount = 4,
				.borderColor = treeTopsColor,
				.flags = LV_BLOCK_DYNAMIC,
				.tform = NewTransform((Vector2){2807, 327}, 0),
				.mass = 50.0f,
				.friction = 0.75f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
						{3038, 500}, {3059, 500}, {3065, 528}, {3059, 545},
						{3041, 550}, {3034, 532}
				},
				.vertCount = 6,
				.borderColor = treeTrunksColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {3016, 278}, {3058, 274}, {3100, 477}, {2980, 477} },
					.vertCount = 4,
					.borderColor = treeTopsColor,
					.flags = LV_BLOCK_DYNAMIC,
					.tform = NewTransform((Vector2){3051, 407}, 0),
					.mass = 50.0f,
					.friction = 0.75f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
						{3252, 518}, {3274, 518}, {3281, 527}, {3275, 569},
						{3255, 575}, {3243, 561}
				},
				.vertCount = 6,
				.borderColor = treeTrunksColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = { {3262, 414}, {3289, 413}, {3314, 500}, {3220, 500} },
				.vertCount = 4,
				.borderColor = treeTopsColor,
				.flags = LV_BLOCK_DYNAMIC,
				.tform = NewTransform((Vector2){3254, 437}, 0),
				.mass = 50.0f,
				.friction = 0.75f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {2904, 491}, {2945, 507}, {2913, 531} },
					.vertCount = 3,
					.borderColor = (Color){227, 193, 236, 255}
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {3146, 534}, {3186, 526}, {3167, 555} },
					.vertCount = 3,
					.borderColor = (Color){186, 223, 237, 255}
			};

			//rightmost stairs section

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = {
							{3381, 598}, {3943, 598}, {3943, 529}, {3976,521}, {3978, 640}, {3382, 638}
					},
					.vertCount = 6,
					.borderColor = terrainColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {3858, 508}, {3945, 476}, {3954, 495}, {3867, 527} },
					.vertCount = 4,
					.borderColor = wallColor,
					.friction = 0.9f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {3980, 417+10}, {4073, 417}, {4073, 432}, {3980, 432+10} },
					.vertCount = 4,
					.borderColor = wallColor,
					.friction = 0.9f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {4106, 335+8}, {4195, 335}, {4195, 359}, {4106, 359+8} },
					.vertCount = 4,
					.borderColor = wallColor,
					.friction = 0.9f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {3983, 203}, {4072, 203+25}, {4072, 221+25}, {3983, 221} },
					.vertCount = 4,
					.borderColor = wallColor,
					.friction = 0.9f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {4229, 5}, {4253, 5}, {4253, 627}, {4229, 627} },
					.vertCount = 4,
					.borderColor = terrainColor
			};

			//hanging platforms

			int platformBasePoly = level->polygonCount;
			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {2900, -500}, {3920, -500}, {3920, -480}, {2900, -480} },
					.vertCount = 4,
					.borderColor = terrainColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {3550, 10}, {3865, 10}, {3865, 20}, {3550, 20} },
					.vertCount = 4,
					.borderColor = treeTrunksColor,
					.flags = LV_BLOCK_DYNAMIC,
					.tform = NewTransform((Vector2){3708, 15}, 0),
					.mass = 70.0f
			};

			level->joints[level->jointCount++] = (LevelJoint_t){
					.type = LV_JOINT_SPRING,
					.polyA = platformBasePoly,
					.polyB = (level->polygonCount-1),
					.springProps.restLength = 1.0f,
					.springProps.damping = 0.65f,
					.springProps.stiffness = 0.5f,
					.springProps.anchorA = {3600, -490},
					.springProps.anchorB = {3550 -3708, 0}
			};

			level->joints[level->jointCount++] = (LevelJoint_t){
					.type = LV_JOINT_SPRING,
					.polyA = platformBasePoly,
					.polyB = (level->polygonCount-1),
					.springProps.restLength = 1.0f,
					.springProps.damping = 0.65f,
					.springProps.stiffness = 0.5f,
					.springProps.anchorA = {3815, -490},
					.springProps.anchorB = {3865 -3708, 0}
			};


			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {2940, -60}, {3340, -60}, {3340, -70}, {2940, -70} },
					.vertCount = 4,
					.borderColor = treeTrunksColor,
					.flags = LV_BLOCK_DYNAMIC,
					.tform = NewTransform((Vector2){3140, -65}, 0),
					.mass = 70.0f
			};

			level->joints[level->jointCount++] = (LevelJoint_t){
					.type = LV_JOINT_SPRING,
					.polyA = platformBasePoly,
					.polyB = (level->polygonCount-1),
					.springProps.restLength = 0.95f,
					.springProps.damping = 0.05f,
					.springProps.stiffness = 0.675f,
					.springProps.anchorA = {2990, -490},
					.springProps.anchorB = {2940 -3140, 0}
			};

			level->joints[level->jointCount++] = (LevelJoint_t){
					.type = LV_JOINT_SPRING,
					.polyA = platformBasePoly,
					.polyB = (level->polygonCount-1),
					.springProps.restLength = 0.95f,
					.springProps.damping = 0.05f,
					.springProps.stiffness = 0.675f,
					.springProps.anchorA = {3290, -490},
					.springProps.anchorB = {3340 -3140, 0}
			};


			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{3395, 50+30}, {3478, 50+30}, {3455, 70+30}, {3364, 70+30}
				},
				.vertCount = 4,
				.borderColor = terrainColor,
				.friction = 0.9f
			};


			//door section

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = {
							{7, 145}, {103, 145}, {54, 523}, {3, 523}
					},
					.vertCount = 4,
					.borderColor = terrainColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = {
							{13, 7}, {65, -500}, {106, 7}
					},
					.vertCount = 3,
					.borderColor = terrainColor
			};

			// level->polygons[level->polygonCount++] = (LevelPoly_t){
			// 		.verts = {
			// 				{44, 3}, {81, 3}, {81, 148}, {44, 148}
			// 		},
			// 		.vertCount = 4,
			// 		.borderColor = (Color){206, 162, 149, 255},
			// 		.flags = LV_BLOCK_DOOR
			// };

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {-15 +60, -10 -60}, {-8 +60, -20 -60}, {12 +60, -20 -60}, {12 +60, 20 -60}, {-15 +60, 20 -60} },
					.vertCount = 5,
					.borderColor = (Color){215, 187, 187, 255},
					.flags = LV_BLOCK_BACKGROUND
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {-14 +50, 9 -47}, {13 +50, 9 -47}, {13 +50, 30 -47}, {-14 +50, 30 -47} },
					.vertCount = 4,
					.borderColor = (Color){181, 181, 181, 255},
					.flags = LV_BLOCK_BACKGROUND
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
					.verts = { {-9 +50, 9 -47}, {-9 +50, -5 -47}, {-5 +50, -7 -47}, {5 +50, -7 -47}, {9 +50, -5 -47}, {9 +50, 9 -47} },
					.vertCount = 6,
					.borderColor = (Color){181, 181, 181, 255},
					//.flags = LV_BLOCK_BACKGROUND
			};

			//left hall section

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
						{-13, 765}, {-963, 765}, {-963, 438}, {-1155, 440},
						{-1155, 364}, {-13, 369}
				},
				.vertCount = 6,
				.borderColor = terrainColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-18, 65}, {-1854, 54}, {-1854, 586}, {-1964, 584},
					{-1964, -170}, {-18, -170}
				},
				.vertCount = 6,
				.borderColor = terrainColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-993, 788}, {-942, 789}, {-939, 826}, {-2259, 831},
					{-2270, 713}, {-993, 713}
				},
				.vertCount = 6,
				.borderColor = terrainColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-2109, 700}, {-2303, 704}, {-2302, 859}, {-2404, 868},
					{-2419, 726}, {-2613, 724}, {-2417, 505}, {-2352, 505}
				},
				.vertCount = 8,
				.borderColor = wallColor,
				.friction = 0.9f,
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-2434, 549}, {-2477, 543}, {-2463, 477}, {-2446, 474}
				},
				.vertCount = 4,
				.borderColor = terrainColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-2354, 376}, {-2454, 380}, {-2447, 59}, {-2930, 72},
					{-2941, 454}, {-3033, 499}, {-3033, -13}, {-2361, -13}
				},
				.vertCount = 8,
				.borderColor = terrainColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){ //final
				.verts = {
					{-3052, 760}, {-3734, 759}, {-4015, 438}, {-4500, 444},
					{-4491, 366}, {-3993, 366}, {-3689, 696}, {-3057, 696}
				},
				.vertCount = 8,
				.borderColor = terrainColor,
				.friction = 0.9f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					//{-1949, 455}, {-2123, 455+70}, {-2123, 419+70}, {-1949, 419}
					{-1942, 363}, {-2027, 368}, {-2130, 532}, {-2160, 532},
					{-2190, 500}, {-2141, 502}, {-2046, 339}, {-1944, 236}
				},
				.vertCount = 8,
				.borderColor = wallColor,
				.friction = 0.9f,
				.elasticity = 0.7f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					//{-2185, 233+125}, {-2330, 233}, {-2330, 204}, {-2185, 204+125}
					{-2157, 308}, {-2255, 220}, {-2346, 209}, {-2346, 178},
					{-2235, 189}, {-2127, 280}
				},
				.vertCount = 6,
				.borderColor = wallColor,
				.friction = 0.9f,
				.elasticity = 0.7f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					//{-2001, -28}, {-2086, 77+105}, {-2116, 56+105}, {-2031, -46}
					{-1938, 98}, {-2192, 91}, {-2190, 58}, {-1947, 62}
				},
				.vertCount = 4,
				.borderColor = wallColor,
				.friction = 0.9f,
				.elasticity = 0.7f
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-3113, 136}, {-3258, 119}, {-3239, 48}, {-3124, 55}
				},
				.vertCount = 4,
				.borderColor = (Color){162, 194, 219, 255}
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-3347, 218}, {-3402, 282}, {-3450, 211}, {-3404, 154}
				},
				.vertCount = 4,
				.borderColor = (Color){175, 154, 190, 255}
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-3180, 380}, {-3305, 426}, {-3321, 369}, {-3202, 325}
				},
				.vertCount = 4,
				.borderColor = (Color){234, 145, 102, 255}
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-3478, 566}, {-3553, 566}, {-3553, 505}, {-3653, 501},
					{-3653, 475}, {-3574, 478}, {-3553, -200}, {-3478, -200}
				},
				.vertCount = 8,
				.borderColor = terrainColor
			};

			//finish line

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-4181, 367}, {-4199, 367}, {-4199, 63}, {-4181, 63}
				},
				.vertCount = 4,
				.borderColor = LIGHTGRAY,
				.flags = LV_BLOCK_BACKGROUND
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-4198, 180}, {-4335, 190}, {-4344, 77}, {-4200, 72}
				},
				.vertCount = 4,
				.borderColor = (Color){162, 240, 215, 255},
				.flags = LV_BLOCK_BACKGROUND
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-4271, 163}, {-4294, 102}, {-4247, 97}
				},
				.vertCount = 3,
				.borderColor = (Color){255, 0, 128, 255}
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-4293, 156}, {-4297, 115}, {-4230, 132}
				},
				.vertCount = 3,
				.borderColor = (Color){255, 0, 128, 255}
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-4460, 458}, {-5166, 478}, {-5148, 424}, {-4460, 399}
				},
				.vertCount = 4,
				.borderColor = wallColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-3999, 377}, {-4033, 377}, {-4016, 325}, {-4004, 329}
				},
				.vertCount = 4,
				.borderColor = wallColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-4890, 416}, {-4873, 289}, {-4865, 416}
				},
				.vertCount = 3,
				.borderColor = wallColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-3063, 732}, {-3115, 726}, {-3075, 643}
				},
				.vertCount = 3,
				.borderColor = wallColor
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t){
				.verts = {
					{-2460, -10}, {-2943, -10}, {-2943, -270}, {-2460, -270}
				},
				.vertCount = 4,
				.borderColor = (Color){240, 217, 239, 70},
				.flags = LV_BLOCK_BACKGROUND
			};


//			level->polygons[level->polygonCount++] = (LevelPoly_t){
//				.verts = { {675, 50}, {743, 27}, {769, 95}, {698, 118} },
//				.vertCount = 4,
//				.borderColor = wallColor,
//				.flags = LV_BLOCK_DYNAMIC,
//				.pos = {725, 66},
//				.mass = 3.0f
//			};

			if(app.role != ROLE_REMOTE){
				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_DEATH,
						.bounds = (Rectangle){-600, 610, 6000, 100}
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_DEATH,
						.bounds = (Rectangle){-6000, 900, 6000, 100}
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_HINT,
						.bounds = (Rectangle){380, 300, 300, 100},
						.hintProps.text = "Use [A] / [D] to move"
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_HINT,
						.bounds = (Rectangle){870, 300, 300, 100},
						.hintProps.text = "[W] key to jump"
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_HINT,
						.bounds = (Rectangle){1900, 300, 300, 100},
						.hintProps.text = "[Q] / [E] to aim"
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_HINT,
						.bounds = (Rectangle){50, 10, 115, 100},
						.hintProps.text = "This door requires a SIM card."
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_HINT,
						.bounds = (Rectangle){3542, 500, 300, 200},
						.hintProps.text = "Use number keys to pick up something"
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_HINT,
						.bounds = (Rectangle){-240, 180, 105, 100},
						.hintProps.text = "[T] and [G] drop primary or secondary item"
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_HINT,
						.bounds = (Rectangle){-3456, 278, 385, 200},
						.hintProps.text = "[S] fires secondary item, [F] to swap"
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_CHAT,
						.bounds = (Rectangle){-2192, 253, 200, 200},
						.chatProps.text = "[KAZIMIR]: BIKAM ONE OF US",
						.chatProps.type = CHATMSG_GLOBAL
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_CHAT,
						.bounds = (Rectangle){-2329, 124, 200, 200},
						.chatProps.text = "[KAZIMIR]: REZISTOR IS FRUTILE!",
						.chatProps.type = CHATMSG_GLOBAL
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_CHAT,
						.bounds = (Rectangle){-3040, -280, 200, 250},
						.chatProps.text = "[KAZIMIR]: YOU SHALL NOT ESKEEEIP",
						.chatProps.type = CHATMSG_GLOBAL
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_TRANSFORM,
						.bounds = (Rectangle){-2968, -280, 500, 250},
						.transformProps.charType = CHAR_CUBE
				};


				level->checkpoints[level->checkpointCount++] = (Vector2){2566, 120};
				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_CHECKPOINT,
						.bounds = (Rectangle){2553, -100, 10, 290},
						.checkpointProps.id = 0
				};

				level->checkpoints[level->checkpointCount++] = (Vector2){-851, 100};
				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_CHECKPOINT,
						.bounds = (Rectangle){-900, 50, 400, 400},
						.checkpointProps.id = 1
				};

				level->checkpoints[level->checkpointCount++] = (Vector2){-2670, -250};
				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_CHECKPOINT,
						.bounds = (Rectangle){-2780, -250, 200, 200},
						.checkpointProps.id = 2
				};

				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_FINISH,
						.bounds = (Rectangle){-5000, 106, 400, 500}
				};


				int monsterPlayerId = CreatePlayer(1);
				Player_t* monsterPlayer = &game.players[monsterPlayerId];
				monsterPlayer->characterId = SpawnCharacter(monsterPlayerId, CHAR_CUBE, (Vector2){-1071, 640});
				GiveItem(monsterPlayer->characterId, ITEM_BLASTER);

				monsterPlayerId = CreatePlayer(1);
				monsterPlayer = &game.players[monsterPlayerId];
				monsterPlayer->characterId = SpawnCharacter(monsterPlayerId, CHAR_CUBE, (Vector2){-2395, 441});
				GiveItem(monsterPlayer->characterId, ITEM_BLASTER);

				monsterPlayerId = CreatePlayer(1);
				monsterPlayer = &game.players[monsterPlayerId];
				monsterPlayer->characterId = SpawnCharacter(monsterPlayerId, CHAR_CUBE, (Vector2){-3615, 412});
				GiveItem(monsterPlayer->characterId, ITEM_BLASTER);

				monsterPlayerId = CreatePlayer(1);
				monsterPlayer = &game.players[monsterPlayerId];
				monsterPlayer->characterId = SpawnCharacter(monsterPlayerId, CHAR_CUBE, (Vector2){-4082, 269});
				GiveItem(monsterPlayer->characterId, ITEM_BLASTER);

				monsterPlayerId = CreatePlayer(1);
				monsterPlayer = &game.players[monsterPlayerId];
				monsterPlayer->characterId = SpawnCharacter(monsterPlayerId, CHAR_CUBE, (Vector2){-4212, 269});
				GiveItem(monsterPlayer->characterId, ITEM_JUMPY);

				monsterPlayerId = CreatePlayer(1);
				monsterPlayer = &game.players[monsterPlayerId];
				monsterPlayer->characterId = SpawnCharacter(monsterPlayerId, CHAR_CUBE, (Vector2){-4334, 269});
				GiveItem(monsterPlayer->characterId, ITEM_BLASTER);

				monsterPlayerId = CreatePlayer(1);
				monsterPlayer = &game.players[monsterPlayerId];
				monsterPlayer->characterId = SpawnCharacter(monsterPlayerId, CHAR_CUBE, (Vector2){-4364, 149});
				GiveItem(monsterPlayer->characterId, ITEM_BLASTER);

				monsterPlayerId = CreatePlayer(1);
				monsterPlayer = &game.players[monsterPlayerId];
				monsterPlayer->characterId = SpawnCharacter(monsterPlayerId, CHAR_CUBE, (Vector2){-4394, 289});
				GiveItem(monsterPlayer->characterId, ITEM_BLASTER);



				int keycardId = SpawnItem(ITEM_KEYCARD, (Vector2){3654, 414});
				Item_t* keycard = &game.items[keycardId];
				keycard->flags = ITEMFLAG_RESPAWNABLE;
				keycard->respawnPos = GetPos(&keycard->tform);

				int blasterId = SpawnItem(ITEM_BLASTER, (Vector2){-2684, -230});
				Item_t* blaster = &game.items[blasterId];
				blaster->flags = ITEMFLAG_RESPAWNABLE;
				blaster->respawnPos = GetPos(&blaster->tform);
			}

			level->layerCount = 1;
			level->layers[0] = (LevelLayer_t){
				.polyStartId = 0,
				.polyTexturedCount = 0,
				.polyUntexturedCount = level->polygonCount,
				.isVisible = 1
			};

			level->entityCount = 1;
			level->entities[0] = (LevelEntity_t){
				.pos = (Vector2){530, 180}
			};

			level->bounds = (Rectangle){-7000, -1000, 15000, 4000};
		} break;


		case 1: {
			level->background = (Color){247, 244, 242, 255};
			//level->spawnPos = (Vector2){-260, 380};
			level->spawnPos = (Vector2){-500, 530};

			//right half

			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {126, 678}, {502, 676}, {544, 661}, {861, 661},
						   {861, 742}, {110, 742} },
					.vertCount = 6,
					.borderColor = DARKGRAY
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {868, 286}, {905, 286}, {905, 570}, {868, 570} },
				.vertCount = 4,
				.borderColor = DARKBLUE
			};

			int lower1 = 60;
			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {348, 383+lower1}, {654, 383+lower1}, {654, 406+lower1}, {348, 406+lower1} },
				.vertCount = 4,
				.borderColor = DARKGRAY
			};

			int lower2 = 60;
			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {148, 256+lower2}, {250, 256+lower2}, {250, 277+lower2}, {148, 277+lower2} },
				.vertCount = 4,
				.borderColor = DARKGRAY
			};

			//left half

			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {-126, 742}, {-861, 742}, {-861, 661}, {-544, 661},
						   {-502, 676}, {-110, 678} },
				.vertCount = 6,
				.borderColor = DARKGRAY
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {-868, 570}, {-905, 570}, {-905, 286}, {-868, 286} },
				.vertCount = 4,
				.borderColor = DARKPURPLE
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {-348, 406+lower1}, {-654, 406+lower1}, {-654, 383+lower1}, {-348, 383+lower1} },
				.vertCount = 4,
				.borderColor = DARKGRAY
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {-148, 277+lower2}, {-250, 277+lower2}, {-250, 256+lower2}, {-148, 256+lower2} },
				.vertCount = 4,
				.borderColor = DARKGRAY
			};

			/*
			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {-500, 600}, {100, 600}, {100, 800}, {-500, 800} },
				.vertCount = 4,
				.borderColor = DARKGRAY
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {200, 600}, {700, 600}, {700, 800}, {200, 800} },
				.vertCount = 4,
				.borderColor = DARKGRAY
			};
			*/

			if(app.role != ROLE_REMOTE){
				
				level->triggers[level->triggerCount++] = (LevelTrigger_t){
					.type = LV_TRIGGER_DEATH,
					.bounds = (Rectangle){-4000, 1000, 8000, 300}
				};
				
				int blasterId = SpawnItem(ITEM_BLASTER, (Vector2){504, 280});
				Item_t* blaster = &game.items[blasterId];
				blaster->flags = ITEMFLAG_RESPAWNABLE;
				blaster->respawnPos = GetPos(&blaster->tform);
				SynchronizeItem(blasterId);

				int rockId = SpawnItem(ITEM_ROCK, (Vector2){-560, 280});
				Item_t* rock = &game.items[rockId];
				rock->flags = ITEMFLAG_RESPAWNABLE;
				rock->respawnPos = GetPos(&rock->tform);
				SynchronizeItem(rockId);

				rockId = SpawnItem(ITEM_ROCK, (Vector2){-540, 290});
				rock = &game.items[rockId];
				rock->flags = ITEMFLAG_RESPAWNABLE;
				rock->respawnPos = GetPos(&rock->tform);
				SynchronizeItem(rockId);

				rockId = SpawnItem(ITEM_ROCK, (Vector2){-520, 300});
				rock = &game.items[rockId];
				rock->flags = ITEMFLAG_RESPAWNABLE;
				rock->respawnPos = GetPos(&rock->tform);
				SynchronizeItem(rockId);
				

				level->bounds = (Rectangle){-5000, -5000, 10000, 10000};

			}

		} break;

		case 2: {
			level->background = (Color){247, 244, 242, 255};
			level->spawnPos = (Vector2){500, 380};
			
			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {-500, 500}, {1000, 500}, {1000, 700}, {-500, 700} },
				.vertCount = 4,
				.borderColor = DARKGRAY
			};

			level->polygons[level->polygonCount++] = (LevelPoly_t) {
				.verts = { {-520, 100}, {-490, 100}, {-490, 490}, {-520, 490} },
				.vertCount = 4,
				.borderColor = DARKGRAY
			};

			if(app.role != ROLE_REMOTE){
				


				level->bounds = (Rectangle){-5000, -5000, 10000, 10000};
			}


		} break;


	}



	//generate polygons
	for(int polyId=0; polyId < level->polygonCount; polyId++){
		LV_InitPoly(level, polyId);
	}

	if(!noPhysics){
		InitializeLevelPhysics();
	}

}

void InitializeLevelPhysics(){
	Level_t* level = &game.level;
	cpSpace* space = &game.space;

	cpBody* staticBody = cpSpaceGetStaticBody(space);


	//generate polygons
	for(int polyId=0; polyId < level->polygonCount; polyId++){
		LevelPoly_t* poly = &level->polygons[polyId];
		Vector2 pos = GetPos(&poly->tform);
		float angle = GetAngle(&poly->tform);
		if(poly->flags & LV_BLOCK_BACKGROUND){
			//...
		}else{
			//add physics
			bool convex = rl_IsConvex(poly->vertCount, (Vector2*)&poly->verts);
			bool dynamic = (poly->flags & LV_BLOCK_DYNAMIC);
			cpVect offset = cpv(pos.x, pos.y);
			//cpTransform offsetTransform = cpTransformTranslate(cpvneg(offset));
			cpTransform offsetTransform = cpTransformIdentity;
			//body
			if(dynamic){
				float moment = (float)cpMomentForPoly((cpFloat)poly->mass, poly->vertCount, (const cpVect*)&poly->verts, offset, (cpFloat)1.0);
				poly->body = cpSpaceAddBody(space, cpBodyNew(poly->mass, moment));
				cpBodySetPosition(poly->body, offset);
				cpBodySetAngle(poly->body, angle);
			}
			//shapes
			if(convex){
				poly->shapes[0] = cpSpaceAddShape(space, cpPolyShapeNew( (dynamic ? poly->body : staticBody), poly->vertCount, (const cpVect*)&poly->verts, offsetTransform, (cpFloat)1.0));
			}else{
				SplitOutput hulls;
				//todo: split into convex polygons, not triangles
				cp_Triangulate(poly->vertCount, (const cpVect*)&poly->verts, &hulls);
				for(int hullId=0; hullId < hulls.polyCount; hullId++){
					SplitPoly* hull = &hulls.polygons[hullId];
					poly->shapes[hullId] = cpSpaceAddShape(space, cpPolyShapeNew( (dynamic ? poly->body : staticBody), hull->vertCount, (const cpVect*)&hull->verts, offsetTransform, (cpFloat)1.0));
				}
			};
			poly->shapeUserData.type = SHAPE_LEVEL_POLY;
			poly->shapeUserData.id = polyId;
			for(int shapeId=0; shapeId < LV_POLY_MAX_SHAPES; shapeId++){
				if(poly->shapes[shapeId]){
					if(poly->friction == 0.0f) poly->friction = 0.5f;
					if(poly->elasticity == 0.0f) poly->elasticity = 0.25f;
					cpShapeSetFriction(poly->shapes[shapeId], (cpFloat)poly->friction);
					cpShapeSetElasticity(poly->shapes[shapeId], (cpFloat)poly->elasticity);
					cpShapeSetCollisionType(poly->shapes[shapeId], COLLISION_TYPE_LEVEL);
					cpShapeSetUserData(poly->shapes[shapeId], &poly->shapeUserData);
				}
			}
		}
	}

	//add joints
	for(int jointId = 0; jointId < level->jointCount; jointId++){
		LevelJoint_t* joint = &level->joints[jointId];
		LevelPoly_t* polyA = &level->polygons[joint->polyA];
		LevelPoly_t* polyB = &level->polygons[joint->polyB];
		switch(joint->type){
		case LV_JOINT_SPRING: {
			cpVect anchorA = cpv(joint->springProps.anchorA.x, joint->springProps.anchorA.y);
			cpVect anchorB = cpv(joint->springProps.anchorB.x, joint->springProps.anchorB.y);
			cpBody* bodyA = (polyA->body) ? polyA->body : staticBody;
			cpBody* bodyB = (polyB->body) ? polyB->body : staticBody;
			if ((bodyA == staticBody) && (bodyB == staticBody)) continue;
			joint->constraint = cpSpaceAddConstraint(&game.space, cpDampedSpringNew(bodyA, bodyB, anchorA, anchorB, joint->springProps.restLength, joint->springProps.stiffness, joint->springProps.damping));
		} break;
		}
	}
}

void CleanUpLevel(){
	for(int polyId = 0; polyId < game.level.polygonCount; polyId++){
		LevelPoly_t* poly = &game.level.polygons[polyId];
		for(int i=0; i < LV_POLY_MAX_SHAPES; i++){
			if(poly->shapes[i]) cpSpaceRemoveShape(&game.space, poly->shapes[i]);
		}
		if(poly->body) cpSpaceRemoveBody(&game.space, poly->body);
	}

	for(int jointId = 0; jointId < game.level.jointCount; jointId++){
		LevelJoint_t* joint = &game.level.joints[jointId];
		if(joint->constraint) cpSpaceRemoveConstraint(&game.space, joint->constraint);
	}

	game.level.triggerCount = 0;
	game.level.checkpointCount = 0;
	game.level.layerCount = 0;
	game.level.polygonCount = 0;
	game.level.jointCount = 0;
}

void UpdateLevel(){
	Level_t* level = &game.level;

	for(int polyId=0; polyId < level->polygonCount; polyId++) {
		LevelPoly_t *poly = &level->polygons[polyId];
		if(poly->body){
			UpdateTransform(&poly->tform, poly->body);
		}
		if(poly->flags & LV_BLOCK_DOOR){
			for(int itemId = 0; itemId < MAX_ITEMS; itemId++){
				Item_t* item = &game.items[itemId];
				if( (!item->active) || (item->type != ITEM_KEYCARD) ) continue;
				//todo (this logic is a placeholder)
				float dist = Vector2Distance(poly->verts[0], GetPos(&item->tform));
				if(dist < 200){
					for(int i=0; i < LV_POLY_MAX_SHAPES; i++){
						if(poly->shapes[i]){
							cpSpaceRemoveShape(&game.space, poly->shapes[i]);
							poly->shapes[i] = NULL;
						}
					}
					poly->borderColor.a = 0;
					poly->flags ^= LV_BLOCK_DOOR;
				}
			}
		}

	}

	for(int triggerId=0; triggerId < level->triggerCount; triggerId++){
		LevelTrigger_t* trigger = &level->triggers[triggerId];
		if(trigger->triggered == -1) continue;
		for(int characterId=0; characterId < MAX_CHARACTERS; characterId++){
			Character_t* character = &game.characters[characterId];
			if(!character->active) continue;
			if(DoesIntersectRect(&character->tform, trigger->bounds)){
				trigger->triggered++;
				switch(trigger->type){
					case LV_TRIGGER_DEATH:
						if(character->health > 0){
							character->health = 0;
							character->healthAnimatedValue = 0;
							//character->healthLastDamageFrame = game.frame;
							character->healthLastDamageSource = DEATH_BY_TRIGGER;
						}
						break;
					case LV_TRIGGER_CHECKPOINT: {
						if(character->playerId == -1) continue;
						Player_t* player = &game.players[character->playerId];
						if(trigger->checkpointProps.id > player->stats.checkpointId){
							player->stats.checkpointId = trigger->checkpointProps.id;
							char text[4096];
							sprintf(text, "Checkpoint! (%i/%i)", player->stats.checkpointId+1, level->checkpointCount+1);
							SendChatMessage(CHATMSG_GLOBAL, 0, text, 0);
						}
					} break;
					case LV_TRIGGER_FINISH:
						if(character->playerId != app.localPlayerId) continue;
						if(character->livingFrames < 2) continue;
						//printf("game finished for %i\n", characterId);
						//todo
						game.state = STATE_ENDED;
						break;
					case LV_TRIGGER_HINT:
						if(character->playerId != app.localPlayerId) continue;
						strncpy(app.gameInterfaceState.hintText, trigger->hintProps.text, sizeof(app.gameInterfaceState.hintText));
						app.gameInterfaceState.hintTimer = 240;
						app.gameInterfaceState.hintFrames = 0;
						trigger->triggered = -1;
						break;
					case LV_TRIGGER_CHAT:
						if(character->playerId != app.localPlayerId) continue;
						SendChatMessage(trigger->chatProps.type, 0, (const char*)&trigger->chatProps.text, trigger->chatProps.variation);
						trigger->triggered = -1;
						break;
					case LV_TRIGGER_TRANSFORM: {
						if( (character->playerId == -1) || (character->type == trigger->transformProps.charType) ) continue;
						Player_t* player = &game.players[character->playerId];
						int newCharacterId = SpawnCharacter(character->playerId, trigger->transformProps.charType, GetPos(&character->tform));
						Character_t* newCharacter = &game.characters[newCharacterId];
						for(int i = 0; i < CHAR_MAX_INVENTORY_SLOTS; i++){
							if(character->inventoryItemIds[i] != -1){
								SetItemOwner(character->inventoryItemIds[i], newCharacterId);
								newCharacter->inventoryItemIds[i] = character->inventoryItemIds[i];
								character->inventoryItemIds[i] = -1;
							}
						}
						RemoveCharacter(characterId);
						player->characterId = newCharacterId;
						//printf("Replaced %i with %i\n", characterId, newCharacterId);
					} break;
				}
		}
		}
	}



}

void DrawLevelBackground(){
	ClearBackground(game.level.background);
	//game.level.background.a = 200;
	//DrawRectangle(0, 0, app.width, app.height, game.level.background);
	int gradientMidPoint = app.height - app.height / 5 - app.cam.target.y * 0.2f;
	int gradientHeight = app.height / 6;
//	Color gradientColor1 = game.level.background;
//	Color gradientColor2 = (Color){238, 240, 240, 255};
//	DrawRectangleGradientV(0, gradientMidPoint-gradientHeight, app.width, gradientHeight, gradientColor1, gradientColor2);
//	DrawRectangleGradientV(0, gradientMidPoint, app.width, gradientHeight, gradientColor2, gradientColor1);
}

void DebugDrawEntities(){
	for(int entityId = 0; entityId < game.level.entityCount; entityId++){
		LevelEntity_t* entity = &game.level.entities[entityId];
		DrawRectangle(entity->pos.x, entity->pos.y, 10, 10, RED);
	}
}

void DebugDrawTriggers(){
	for(int triggerId=0; triggerId < game.level.triggerCount; triggerId++){
		DrawRectangleLinesEx(game.level.triggers[triggerId].bounds, 2, BLUE);
	}
}

void DrawLevel(){
	Level_t* level = &game.level;
	app.debugInfo.levelPolysDrawn = 0;

	DebugDrawEntities();

	for(int layerId = 0; layerId < game.level.layerCount; layerId++){
		LevelLayer_t* layer = &game.level.layers[layerId];
		if(!layer->isVisible) continue;
		//first textured polys
		//...
		//then untextured
		for(int i=0; i < layer->polyUntexturedCount; i++){
			int polyId = layer->polyStartId + layer->polyTexturedCount + i;
			LevelPoly_t* poly = &level->polygons[polyId];

			if(!IsInView(&poly->tform)) continue;
			app.debugInfo.levelPolysDrawn++;
			//bool dynamic = poly->flags & LV_BLOCK_DYNAMIC;
			bool dynamic = (poly->body);
			if(dynamic) BeginTransform(&poly->tform);
			if(poly->flags & LV_BLOCK_BACKGROUND){
				//fill
				//DrawTriangleStrip((Vector2*)&poly->verts, poly->vertCount, poly->borderColor);
				DrawPolyObjectFill(poly->triangleCount, (int*)&poly->triangleIndices, (Vector2*)&poly->verts, poly->borderColor);
			}else{
				DrawPolyObjectFill(poly->triangleCount, (int*)&poly->triangleIndices, (Vector2*)&poly->verts, (Color){240, 240, 240, 255});
				//stroke
				for(int vertId=0; vertId < poly->vertCount; vertId++){
					DrawLineEx(poly->verts[vertId], poly->verts[(vertId + 1) % poly->vertCount], 4.0, poly->borderColor);
					DrawPoly(poly->verts[(vertId + 1) % poly->vertCount], 3, 2.0f, PI*2+Vector2Angle(poly->verts[vertId], poly->verts[(vertId + 2) % poly->vertCount]), poly->borderColor);
					DrawPoly(poly->verts[(vertId + 1) % poly->vertCount], 3, 2.0f, PI*2+Vector2Angle(poly->verts[vertId], poly->verts[(vertId + 2) % poly->vertCount]), poly->borderColor);
				}
			}
			if(dynamic) EndTransform();
		}
	}

	for(int jointId = 0; jointId < level->jointCount; jointId++){
		LevelJoint_t* joint = &level->joints[jointId];
		switch(joint->type){
			case LV_JOINT_SPRING: {
				LevelPoly_t* polyA = &level->polygons[joint->polyA];
				LevelPoly_t* polyB = &level->polygons[joint->polyB];
				Vector2 posA = Vector2Add(GetInterpolatedPos(&polyA->tform), joint->springProps.anchorA);
				Vector2 posB = Vector2Add(GetInterpolatedPos(&polyB->tform), joint->springProps.anchorB);
				DrawLineEx(posA, posB, 4, (Color) { 160, 149, 141, 255 });
			} break;
		}
	}

	if(app.debugMode){
		DebugDrawTriggers();
		DebugDrawEntities();
	}

}

// void DrawLevel(){
// 	Level_t* level = &game.level;

// 	app.debugInfo.levelPolysDrawn = 0;
// 	for(int polyId = 0; polyId < level->polygonCount; polyId++){
// 		LevelPoly_t* poly = &level->polygons[polyId];
// 		//todo: check if in view
// 		if(!IsInView(&poly->tform)) continue;
// 		app.debugInfo.levelPolysDrawn++;
// 		//bool dynamic = poly->flags & LV_BLOCK_DYNAMIC;
// 		bool dynamic = (poly->body);
// 		if(dynamic) BeginTransform(&poly->tform);
// 		if(poly->flags & LV_BLOCK_BACKGROUND){
// 			//fill
// 			//DrawTriangleStrip((Vector2*)&poly->verts, poly->vertCount, poly->borderColor);
// 			DrawPolyObjectFill(poly->triangleCount, (int*)&poly->triangleIndices, (Vector2*)&poly->verts, poly->borderColor);
// 		}else{
// 			DrawPolyObjectFill(poly->triangleCount, (int*)&poly->triangleIndices, (Vector2*)&poly->verts, (Color){240, 240, 240, 255});
// 			//stroke
// 			for(int vertId=0; vertId < poly->vertCount; vertId++){
// 				DrawLineEx(poly->verts[vertId], poly->verts[(vertId + 1) % poly->vertCount], 4.0, poly->borderColor);
// 				DrawPoly(poly->verts[(vertId + 1) % poly->vertCount], 3, 2.0f, PI*2+Vector2Angle(poly->verts[vertId], poly->verts[(vertId + 2) % poly->vertCount]), poly->borderColor);
// 				DrawPoly(poly->verts[(vertId + 1) % poly->vertCount], 3, 2.0f, PI*2+Vector2Angle(poly->verts[vertId], poly->verts[(vertId + 2) % poly->vertCount]), poly->borderColor);
// 			}
// 		}
// 		if(dynamic) EndTransform();
// 	}

// 	for(int jointId = 0; jointId < level->jointCount; jointId++){
// 		LevelJoint_t* joint = &level->joints[jointId];
// 		switch(joint->type){
// 			case LV_JOINT_SPRING: {
// 				LevelPoly_t* polyA = &level->polygons[joint->polyA];
// 				LevelPoly_t* polyB = &level->polygons[joint->polyB];
// 				Vector2 posA = Vector2Add(GetInterpolatedPos(&polyA->tform), joint->springProps.anchorA);
// 				Vector2 posB = Vector2Add(GetInterpolatedPos(&polyB->tform), joint->springProps.anchorB);
// 				DrawLineEx(posA, posB, 4, (Color) { 160, 149, 141, 255 });
// 			} break;
// 		}
// 	}


// 	if(app.debugMode){
// 		for(int triggerId=0; triggerId < level->triggerCount; triggerId++){
// 			DrawRectangleLinesEx(level->triggers[triggerId].bounds, 2, BLUE);
// 		}
// 	}
// }
